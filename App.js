import React from 'react';
import { SafeAreaView, StatusBar } from 'react-native';
import * as eva from '@eva-design/eva';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { AppNavigator } from './app.navigator';
import { createStore } from 'redux';
import reducers from './redux/reducers';
import { Provider } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { changeSession } from './redux/actions/userActions';
import { default as theme } from './theme.json';

//import SplashScreen from 'react-native-splash-screen';
//SplashScreen.hide()
const Store = createStore(reducers);
AsyncStorage.getItem('sessionId').then((sessionId) => {
	Store.dispatch(changeSession(sessionId));
});

export default function App() {
	return (
		<Provider store={Store}>
			<IconRegistry icons={EvaIconsPack} />
			<ApplicationProvider {...eva} theme={{ ...eva.light, ...theme }}>
				<SafeAreaView>
					<StatusBar />
				</SafeAreaView>
				<AppNavigator />
			</ApplicationProvider>
		</Provider>
	);
}
