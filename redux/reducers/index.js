import {
  SESSION_CHANGE,
  TYPE_CHANGE,
  USER_CHANGE,
  SET_SCHOOLS,
  SET_TEACHERS,
  SET_STUDENTS,
  SET_SERVICES,
  SET_LESSONS_PLUS,
  SET_LESSONS_MINUS,
  SET_BAR_CODE,
} from '../constants';
import { combineReducers } from 'redux';

const initialStateUser = {
  sessionId: -1,
  type: '',
  avatar: require('../../assets/no-avatar.png'),
  firstname: '',
  lastname: '',
  patronymic: '',
};

const userReducer = (state = initialStateUser, action) => {
  switch (action.type) {
    case SESSION_CHANGE:
      return {
        ...state,
        sessionId: action.payload,
      };
    case TYPE_CHANGE:
      return {
        ...state,
        type: action.payload,
      };
    case USER_CHANGE:
      return {
        ...state,
        ...action.payload,
        avatar: action.payload.avatar
          ? { uri: action.payload.avatar }
          : state.avatar,
      };
    default:
      return state;
  }
};

const initialStateSchools = {
  list: [],
};
const schoolsReducer = (state = initialStateSchools, action) => {
  switch (action.type) {
    case SET_SCHOOLS:
      return {
        ...state,
        list: action.payload,
      };
    default:
      return state;
  }
};

const initialStateTeachers = {
  list: [],
};
const teachersReducer = (state = initialStateTeachers, action) => {
  switch (action.type) {
    case SET_TEACHERS:
      return {
        ...state,
        list: action.payload,
      };
    default:
      return state;
  }
};

const initialStateStudents = {
  list: [],
};
const studentsReducer = (state = initialStateStudents, action) => {
  switch (action.type) {
    case SET_STUDENTS:
      return {
        ...state,
        list: action.payload,
      };
    default:
      return state;
  }
};

const initialStateServices = {
  list: [],
};
const servicesReducer = (state = initialStateServices, action) => {
  switch (action.type) {
    case SET_SERVICES:
      return {
        ...state,
        list: action.payload,
      };
    default:
      return state;
  }
};

const initialStateLessonsPlus = {
  list: [],
};
const lessonsPlusReducer = (state = initialStateLessonsPlus, action) => {
  switch (action.type) {
    case SET_LESSONS_PLUS:
      return {
        ...state,
        list: action.payload,
      };
    default:
      return state;
  }
};

const initialStateLessonsMinus = {
  list: [],
};
const lessonsMinusReducer = (state = initialStateLessonsMinus, action) => {
  switch (action.type) {
    case SET_LESSONS_MINUS:
      return {
        ...state,
        list: action.payload,
      };
    default:
      return state;
  }
};

const initialStateBarCode = {
  value: null,
};
const barCodeReducer = (state = initialStateBarCode, action) => {
  switch (action.type) {
    case SET_BAR_CODE:
      return {
        ...state,
        value: action.payload,
      };
    default:
      return state;
  }
};
export default combineReducers({
  user: userReducer,
  schools: schoolsReducer,
  teachers: teachersReducer,
  students: studentsReducer,
  services: servicesReducer,
  lessonsPlus: lessonsPlusReducer,
  lessonsMinus: lessonsMinusReducer,
  barCode: barCodeReducer,
});
