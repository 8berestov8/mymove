import { SET_SERVICES } from '../constants';
export const setServices = (id_school, services) => {
    const payload = {};
    payload[id_school] = services
    return {
        type: SET_SERVICES,
        payload: payload
    }
}