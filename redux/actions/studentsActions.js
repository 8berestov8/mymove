import { SET_STUDENTS } from '../constants';
export const setStudents = (id_school, students) => {
    const payload = {};
    payload[id_school] = students
    return {
        type: SET_STUDENTS,
        payload: payload
    }
}