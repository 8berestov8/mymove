import {SET_TEACHERS} from '../constants'

export const setTeachers = (id_school, teachers) => {
  const payload = {}
  payload[id_school] = teachers
  return {
    type: SET_TEACHERS,
    payload: payload
  }
}