import { SET_BAR_CODE } from '../constants';
export const setBarCode = (barCode) => {
	return {
		type: SET_BAR_CODE,
		payload: barCode,
	};
};
