import {SESSION_CHANGE, TYPE_CHANGE, USER_CHANGE} from '../constants'

export const changeSession = sessionId => {
  return {
    type: SESSION_CHANGE,
    payload: sessionId
  }
}
export const changeType = type => {
  return {
    type: TYPE_CHANGE,
    payload: type
  }
}
export const changeUser = (user) => {
  console.log('ChangeUser', user);
  return {
    type: USER_CHANGE,
    payload: user
  }
}