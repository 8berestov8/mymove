import { SET_LESSONS_PLUS } from '../constants';
export const setLessonsPlus = (idStudent, lessons) => {
    const payload = {};
    payload[idStudent] = lessons;
    return {
        type: SET_LESSONS_PLUS,
        payload: payload
    }
}