import { SET_LESSONS_MINUS } from '../constants';
export const setLessonsMinus = (idLessonPlus, lessons) => {
    const payload = {};
    payload[idLessonPlus] = lessons;
    return {
        type: SET_LESSONS_MINUS,
        payload: payload
    }
}