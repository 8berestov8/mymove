import { SET_SCHOOLS } from '../constants';
export const setSchools = schools => {
    return {
        type: SET_SCHOOLS,
        payload: schools
    }
}