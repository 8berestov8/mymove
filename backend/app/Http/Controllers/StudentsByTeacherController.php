<?php

namespace App\Http\Controllers;

use App\Models\Students;
use App\Models\StudentsTeachersXref;
use Illuminate\Http\Request;
use App\Http\Requests\StudentRequest;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\TeachersController;
use Illuminate\Support\Facades\DB;

class StudentsByTeacherController extends Controller
{
    public function save_teachers_xref($student_id, $teachers) {
        StudentsTeachersXref::where('student_id', '=', $student_id)->delete();
        foreach($teachers as $teacher) {
            StudentsTeachersXref::create([
                'student_id'    => $student_id,
                'teacher_id'    => $teacher['teacher_id']
            ]);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StudentRequest $request
     * @return \Illuminate\Http\Response
     */
    public function add_student(StudentRequest $request) {
        $fields = $request->validated();
        $school_id = $request->route('school_id');
        $fields['school_id'] = $school_id;
        $teacher = TeachersController::get_teacher_by_session($request, $school_id);
        $student = Students::create($fields);
        $this->save_teachers_xref($student->id, [[
            'teacher_id'    => $teacher->id
        ]]);
        return $this->students()->find($student->id);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function get_students(Request $request) {
        $user = UsersController::get_user_by_session($request);
        $students = $this->students()->where('school_id', '=', $request->route('school_id'))->whereHas('teachers_xref', function ($query) use($user) {
            $query->whereHas('teachers', function($query) use($user) {
                $query->where('phone', '=', $user->phone);
            });
        })->orderBy('id', 'DESC')->get();
        return $students;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  SchoolRequest $request
     * @return \Illuminate\Http\Response
     */
    public function edit_student(StudentRequest $request) {
        $student = Students::findOrFail($request->student_id);
        $student->fill($request->validated())->save();
        return $this->students()->find($student->id);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function delete_student(Request $request) {
        $teacher = TeachersController::get_teacher_by_session($request, $request->school_id);
        return StudentsTeachersXref::where('teacher_id', '=', $teacher->id)->where('student_id', '=', $request->student_id)->limit(1)->delete();
    }
    private function students() {
        return Students::select(
            '*',
            DB::raw('IFNULL((
                SELECT
                    SUM(lm.count_lessons)
                FROM
                    lessons_minus as lm
                WHERE
                    lm.student_id = students.id AND
                    lm.lesson_plus_id IN(SELECT id FROM lessons_plus WHERE CURDATE() >= date_start AND CURDATE() <= date_end)
            ), 0) as used_lessons'),
            DB::raw('IFNULL((
                SELECT
                    SUM(lp.count_lessons)
                FROM
                    lessons_plus as lp
                WHERE
                    student_id = students.id AND
                    CURDATE() >= date_start AND
                    CURDATE() <= date_end AND
                    infinity_lessons = 0
            ), 0) as count_lessons'),
            DB::raw('IFNULL((
                SELECT
                    IF(count(*)>0, 1, 0)
                FROM
                    lessons_plus as lp
                WHERE
                    student_id = students.id AND
                    CURDATE() >= date_start AND
                    CURDATE() <= date_end AND
                    infinity_lessons = 1
            ), 0) as infinity_lessons'),
        );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Students  $Students
     * @return \Illuminate\Http\Response
     */
    public function show(Students $Students)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Students  $Students
     * @return \Illuminate\Http\Response
     */
    public function edit(Students $Students)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Students  $Students
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Students $Students)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Students  $Students
     * @return \Illuminate\Http\Response
     */
    public function destroy(Students $Students)
    {
        //
    }
}
