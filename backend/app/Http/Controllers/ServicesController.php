<?php

namespace App\Http\Controllers;

use App\Models\Services;
use App\Http\Controllers\UsersController;
use Illuminate\Http\Request;
use App\Http\Requests\ServiceRequest;

class ServicesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  ServiceRequest $request
     * @return \Illuminate\Http\Response
     */
    public function add_service(ServiceRequest $request) {
        $service = Services::create($request->validated());
        return $service->find($service->id);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  ServiceRequest $request
     * @return \Illuminate\Http\Response
     */
    public function edit_service(ServiceRequest $request) {
        $service = Services::findOrFail($request->service_id);
        $service->fill($request->validated())->save();
        return $service;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function get_services(ServiceRequest $request) {
        $services = Services::where('school_id', '=', $request->validated()['school_id'])->orderBy('id', 'DESC')->get();
        return $services;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function delete_service(ServiceRequest $request) {
        $service = Services::findOrFail($request->validated()['service_id']);
        return $service->delete();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function show(Shools $shools)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function edit(Shools $shools)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shools $shools)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shools $shools)
    {
        //
    }
}
