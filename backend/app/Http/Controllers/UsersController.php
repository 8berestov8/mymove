<?php

namespace App\Http\Controllers;

use App\Models\Users;
use App\Models\Students;
use App\Models\Sessions;
use App\Http\Controllers\RatesController;
use Illuminate\Http\Request;
use App\Http\Requests\ProfileRequest;
use Intervention\Image\Facades\Image as InterventionImage;
use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{
    static public function get_user_by_session($session_id) {
        $session = UsersController::get_session(is_string($session_id) ? $session_id : $session_id->header('session'));
        if($session) {
            $user = Users::find($session->user_id);
            return $user;
        } else {
            return false;
        }
    }
    static public function get_session($session_id) {
        return Sessions::find(is_string($session_id) ? $session_id : $session_id->header('session'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  ProfileRequest $request
     * @return \Illuminate\Http\Response
     */
    public function save_profile(ProfileRequest $request) {
        $user = $this->get_user_by_session($request);
        $fields = $request->validated();
        $user->firstname = $fields['firstname'];
        $user->lastname = $fields['lastname'];
        $user->patronymic = $fields['patronymic'];
        if($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $image = InterventionImage::make($avatar)->orientate()->resize(null, 500, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->encode('jpg', 80);
            $year = date('Y');
            $month = date('m');
            $day = date('d');
            $avatar_path = "$year/$month/$day/$user->id.jpg";
            Storage::disk('avatars')->put($avatar_path, $image->getEncoded());
            $user->avatar = $avatar_path;
        }
        $user->save();
        return $user;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function get_profile(Request $request) {
        $session = UsersController::get_session($request);
        $user = $this->get_user_by_session($request);
        if(empty($user->public_key)) {
            $user->fill([
                'public_key'            => uniqid(),
                'public_updated_at'    => \DB::raw('now()')
            ])->save();
        }
        $data = array_merge($user->toArray(), [
            'type'          => $session->type,
            'public_key'    => $user->public_key,
        ]);
        if($session->type == 'owner') {
            $data['rate'] = RatesController::get_by_user($user->id);
        }
        return $data;
    }
    public function get_student_by_user(Request $request) {
        $user_id = $request->user_id;
        $school_id = $request->school_id;
        $user = Users::find($user_id);
        $student = Students::where('school_id', '=', $school_id)->whereAnd('phone', '=', $user->phone)->first();
        if(!$student) {
            $student = Students::create([
                'school_id' => $school_id,
                'phone'     => $user->phone,
                'firstname' => $user->firstname,
                'lastname'  => $user->lastname,
                'patronymic'  => $user->patronymic
            ]);
            $student = Students::find($student->id);
        }
        return $student;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function show(Users $users)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function edit(Users $users)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Users $users)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function destroy(Users $users)
    {
        //
    }
}
