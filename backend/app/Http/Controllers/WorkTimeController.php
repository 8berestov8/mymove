<?php

namespace App\Http\Controllers;

use App\Models\WorkTime;
use Illuminate\Http\Request;
use App\Http\Requests\WorkTimeRequest;
use App\Http\Controllers\TeachersController;

class WorkTimeController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  WorkTimeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function add_work_time(WorkTimeRequest $request) {
        $work_time = WorkTime::create(array_merge($request->validated(), [
            'teacher_id'   => $request->teacher_id
        ]));
        return $work_time->find($work_time->id);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  WorkTimeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function edit_work_time(WorkTimeRequest $request) {
        $work_time = WorkTime::findOrFail($request->work_time_id);
        $work_time->fill($request->validated())->save();
        return $work_time->find($work_time->id);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  WorkTimeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function get_work_times(WorkTimeRequest $request) {
        $fields = $request->validated();
        $work_times = WorkTime::where('school_id', '=', $fields['school_id'])->where('teacher_id', '=', $fields['teacher_id'])->orderBy('id', 'DESC')->get();
        return $work_times;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  WorkTimeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function delete_work_time(WorkTimeRequest $request) {
        $work_time = WorkTime::findOrFail($request->validated()['work_time_id']);
        return $work_time->delete();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\finance  $finance
     * @return \Illuminate\Http\Response
     */
    public function show(finance $finance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\finance  $finance
     * @return \Illuminate\Http\Response
     */
    public function edit(finance $finance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\finance  $finance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, finance $finance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\finance  $finance
     * @return \Illuminate\Http\Response
     */
    public function destroy(finance $finance)
    {
        //
    }
}
