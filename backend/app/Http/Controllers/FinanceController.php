<?php

namespace App\Http\Controllers;

use App\Models\Finance;
use Illuminate\Http\Request;
use App\Http\Requests\FinanceRequest;

class FinanceController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  FinanceRequest $request
     * @return \Illuminate\Http\Response
     */
    public function add_finance(FinanceRequest $request) {
        $user = UsersController::get_user_by_session($request);
        $finance = Finance::create(array_merge($request->validated(), [
            'user_id'   => $user->id
        ]));
        return $finance->find($finance->id);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  FinanceRequest $request
     * @return \Illuminate\Http\Response
     */
    public function edit_finance(FinanceRequest $request) {
        $finance = Finance::findOrFail($request->finance_id);
        $finance->fill($request->validated())->save();
        return $finance->find($finance->id);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function get_finances(FinanceRequest $request) {
        $finance = Finance::where('school_id', '=', $request->validated()['school_id'])->orderBy('id', 'DESC')->get();
        return $finance;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function delete_finance(FinanceRequest $request) {
        $finance = Finance::findOrFail($request->validated()['finance_id']);
        return $finance->delete();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\finance  $finance
     * @return \Illuminate\Http\Response
     */
    public function show(finance $finance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\finance  $finance
     * @return \Illuminate\Http\Response
     */
    public function edit(finance $finance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\finance  $finance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, finance $finance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\finance  $finance
     * @return \Illuminate\Http\Response
     */
    public function destroy(finance $finance)
    {
        //
    }
}
