<?php

namespace App\Http\Controllers;

use App\Models\LessonsMinus;
use App\Http\Controllers\UsersController;
use Illuminate\Http\Request;
use App\Http\Requests\LessonMinusRequest;
use Illuminate\Support\Facades\DB;

class LessonsMinusController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  LessonMinusRequest $request
     * @return \Illuminate\Http\Response
     */
    public function add_lesson_minus(LessonMinusRequest $request) {
        $lesson_minus = LessonsMinus::create($request->validated());
        return $this->lessons_minus()->find($lesson_minus->id);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  LessonMinusRequest $request
     * @return \Illuminate\Http\Response
     */
    public function edit_lesson_minus(LessonMinusRequest $request) {
        $lesson_minus = LessonsMinus::findOrFail($request->lesson_minus_id);
        $lesson_minus->fill($request->validated())->save();
        return $this->lessons_minus()->find($lesson_minus->id);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function get_lessons_minus(LessonMinusRequest $request) {
        $lessons_minus = $this->lessons_minus()->where('lesson_plus_id', '=', $request->validated()['lesson_plus_id'])->orderBy('id', 'DESC')->get();
        return $lessons_minus;
    }
    private function lessons_minus() {
        return LessonsMinus::select('*');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function delete_lesson_minus(LessonMinusRequest $request) {
        $lesson_minus = LessonsMinus::findOrFail($request->validated()['lesson_minus_id']);
        return $lesson_minus->delete();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function show(Shools $shools)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function edit(Shools $shools)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shools $shools)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shools $shools)
    {
        //
    }
}
