<?php

namespace App\Http\Controllers;

use App\Models\Teachers;
use App\Models\Schools;
use Illuminate\Http\Request;
use App\Http\Requests\TeacherRequest;
use App\Http\Controllers\UsersController;

class TeachersController extends Controller
{
    static public function get_teacher_by_session($id_session, $school_id) {
        $user = UsersController::get_user_by_session($id_session);
        if($user) {
            $teacher = Teachers::where('phone', '=', $user->phone)->where('school_id', '=', $school_id)->first();
            return $teacher;
        } else {
            return false;
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  TeacherRequest $request
     * @return \Illuminate\Http\Response
     */
    public function add_teacher(TeacherRequest $request) {
        $fields = $request->validated();
        $fields['school_id'] = $request->route('school_id');
        $teacher = Teachers::create($fields);
        return $teacher;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function get_teachers(TeacherRequest $request) {
        $user = UsersController::get_user_by_session($request);
        $teachers = Teachers::where('school_id', '=', $request->validated()['school_id'])->orderBy('id', 'DESC')->get();
        return $teachers;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  SchoolRequest $request
     * @return \Illuminate\Http\Response
     */
    public function edit_teacher(TeacherRequest $request) {
        $teacher = Teachers::findOrFail($request->teacher_id);
        $teacher->fill($request->validated())->save();
        return $teacher;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function delete_teacher(TeacherRequest $request) {
        $teacher = Teachers::findOrFail($request->validated()['teacher_id']);
        return $teacher->delete();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Teachers  $teachers
     * @return \Illuminate\Http\Response
     */
    public function show(Teachers $teachers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Teachers  $teachers
     * @return \Illuminate\Http\Response
     */
    public function edit(Teachers $teachers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Teachers  $teachers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Teachers $teachers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Teachers  $teachers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teachers $teachers)
    {
        //
    }
}
