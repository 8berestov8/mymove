<?php

namespace App\Http\Controllers;

use App\Models\LessonsPlus;
use App\Http\Controllers\UsersController;
use Illuminate\Http\Request;
use App\Http\Requests\LessonPlusRequest;
use Illuminate\Support\Facades\DB;

class LessonsPlusController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  LessonPlusRequest $request
     * @return \Illuminate\Http\Response
     */
    public function add_lesson_plus(LessonPlusRequest $request) {
        $fields = $request->validated();
        $lesson_plus = LessonsPlus::create([
            'date_start'      => $fields['date_start'],
            'date_end'        => $fields['date_end'],
            'count_lessons'   => $fields['count_lessons'],
            'infinity_lessons'  => $fields['infinity_lessons'],
            'school_id'     => $request->school_id,
            'service_id'    => $fields['service_id'],
            'student_id'    => $fields['student_id'],
        ]);
        return $this->lessons_plus()->find($lesson_plus->id);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  LessonPlusRequest $request
     * @return \Illuminate\Http\Response
     */
    public function edit_lesson_plus(LessonPlusRequest $request) {
        $lesson_plus = LessonsPlus::findOrFail($request->lesson_plus_id);
        $lesson_plus->fill($request->validated())->save();
        return $this->lessons_plus()->find($lesson_plus->id);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function get_lessons_plus(LessonPlusRequest $request) {
        $lessons_plus =
        $this->lessons_plus()->where('student_id', '=', $request->validated()['student_id'])->orderBy('id', 'DESC')->get();
        return $lessons_plus;
    }
    private function lessons_plus() {
        return LessonsPlus::select(
            '*',
            DB::raw('IFNULL((SELECT SUM(lm.count_lessons) FROM lessons_minus as lm WHERE lm.lesson_plus_id = lessons_plus.id), 0) as used_lessons'),
            DB::raw('(CURDATE() >= date_start AND CURDATE() <= date_end) as is_validity'),
        );
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function delete_lesson_plus(LessonPlusRequest $request) {
        $lesson_plus = LessonsPlus::findOrFail($request->validated()['lesson_plus_id']);
        return $lesson_plus->delete();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function show(Shools $shools)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function edit(Shools $shools)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shools $shools)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shools $shools)
    {
        //
    }
}
