<?php

namespace App\Http\Controllers;

use App\Models\CheckPhones;
use App\Models\Users;
use App\Models\Sessions;
use Illuminate\Http\Request;
use App\Http\Requests\CheckPhoneRequest;
use App\Http\Requests\CheckSmsCodeRequest;
use Khodja\Smsc\Smsc;
use App\Libraries\SmscLibrary;

use App\Libraries\OtherLibrary;

class CheckPhonesController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\CheckPhoneRequest $request
     * @return \Illuminate\Http\Response
     */
    public function check_phone(CheckPhoneRequest $request)
    {
        $fields = $request->validated();
        if($fields['phone'] == '77778888888') {
            $id = OtherLibrary::gen_uuid();
            $checkPhone = CheckPhones::create([
                'id'    => $id,
                'phone' => $fields['phone'],
                'code'  => 7388
            ]);
            return array(
                'id'    => $id
            );
        } else {
            // $code = OtherLibrary::random_number(4);
            // $result = SmsC::send($fields['phone'], 'ProMove Ваш код подтвержения: ' . $code . '. Наберите его в поле ввода');
            $date = new \DateTime;
            $date->modify('-1 minutes');
            $formatted_date = $date->format('Y-m-d H:i:s');
            $checkPhonesLastMin = CheckPhones::where('phone', '=', $fields['phone'])->where('created_at','>=',$formatted_date)->orderBy('created_at', 'DESC')->get()->first();
            if($checkPhonesLastMin) {
                abort(503, 'Повторный запрос можно осуществить через минуту.');
            }
            $result = SmscLibrary::callCode($fields['phone']);
            if(intval($result[0]) > 0) {
                $code = mb_substr($result[2], 2, 6);
                $id = OtherLibrary::gen_uuid();
                $checkPhone = CheckPhones::create([
                    'id'    => $id,
                    'phone' => $fields['phone'],
                    'code'  => $code
                ]);
                return array(
                    'id'    => $id
                );
            } else {
                abort(503, 'Ошибка проверки телефона. Пожалуйста, проверьте введенный номер телефона.');
            }
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\CheckSmsCodeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function check_sms_code(CheckSmsCodeRequest $request)
    {
        $fields = $request->validated();
        $itemCheckPhone = CheckPhones::findOrFail($fields['id']);
        if($itemCheckPhone->count_try < 5) {
            if(!$itemCheckPhone->is_activated) {
                $itemCheckPhone->count_try++;
                $itemCheckPhone->save();
                if($itemCheckPhone->code == $request->code) {
                    $itemCheckPhone->is_activated = 1;
                    $itemCheckPhone->activated_at = date('Y-m-d H:i:s');
                    //$itemCheckPhone->save();
                    $itemUser = Users::where('phone', $itemCheckPhone->phone)->first();
                    $is_new = false;
                    if(!$itemUser) {
                        $itemUser = Users::create([
                            'phone' => $itemCheckPhone->phone
                        ]);
                        $is_new = true;
                    }
                    $itemCheckPhone->is_activated = 1;
                    $itemCheckPhone->save();
                    $itemSession = Sessions::create([
                        'id'        => OtherLibrary::gen_uuid(),
                        'user_id'   => $itemUser->id,
                        'type'      => $request->type
                    ]);
                    return [
                        'id_session'    => $itemSession->id,
                        'is_new'        => $is_new
                    ];
                } else {
                    abort(504, 'Код введен не верно');
                }
            } else {
                abort(503, 'Код активирован');
            }
        } else {
            abort(505, 'Использовано максимальное количество попыток');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CheckPhones  $checkPhones
     * @return \Illuminate\Http\Response
     */
    public function show(CheckPhones $checkPhones)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CheckPhones  $checkPhones
     * @return \Illuminate\Http\Response
     */
    public function edit(CheckPhones $checkPhones)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CheckPhones  $checkPhones
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CheckPhones $checkPhones)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CheckPhones  $checkPhones
     * @return \Illuminate\Http\Response
     */
    public function destroy(CheckPhones $checkPhones)
    {
        //
    }
}
