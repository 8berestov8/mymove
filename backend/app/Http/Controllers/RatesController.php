<?php

namespace App\Http\Controllers;

use Log;
use App\Models\Rates;
use Illuminate\Http\Request;

use Imdhemy\GooglePlay\Subscriptions\SubscriptionPurchase;
use Imdhemy\Purchases\Facades\Subscription;
use App\Http\Controllers\UsersController;
use Imdhemy\GooglePlay\DeveloperNotifications\DeveloperNotification;
Use \Carbon\Carbon;

class RatesController extends Controller
{
    public function check(Request $request)
    {
        $user = UsersController::get_user_by_session($request);
        $data = $request->validate([
            'productId'             => '', // ios & android
            'transactionReceipt'    => '', // ios & android
            'transactionId'         => '', // ios & android
            'transactionDate'       => '', // ios & android
            'purchaseToken'         => '', // android
            'autoRenewingAndroid'   => '', // android
            'signatureAndroid'      => '', // android
            'isAcknowledgedAndroid' => '', // android
            'purchaseStateAndroid'  => '', // android
            'packageNameAndroid'    => '', // android


        ]);
        Log::info(json_encode($data));
        if(!empty($data['packageNameAndroid'])) {
            /**
             * Android
             */
            $res = $this->save_android_rate($data['productId'], $data['purchaseToken'], $data['packageNameAndroid'], $user->id);
            if($res) {
                return 1;
            }
        } else {
            /**
             * iOS
             */
            $res = $this->save_apple_rate($data['transactionReceipt'], $user->id);
            if($res) {
                return 1;
            }
        }
        return 0;
    }
    public static function get_by_user($user_id)
    {
        $time_now = Carbon::now();
        //print $time_now->toDateTimeString();
        $rate = Rates
            ::where('user_id', '=', $user_id)
            ->where('date_start', '<=', $time_now->toDateTimeString())
            ->where('date_end', '>=', $time_now->toDateTimeString())
            ->limit(1)->first();
        return [
            'date_start'    => !is_null($rate) ? date('d.m.Y', strtotime($rate['date_start'])) : null,
            'date_end'      => !is_null($rate) ? date('d.m.Y', strtotime($rate['date_end'])) : null,
            'type'          => !is_null($rate) ? $rate['type'] : null,
        ];
    }
    public function save_apple_rate($transactionReceipt, $id_user=NULL)
    {
        $receiptResponse = Subscription::appStore()->receiptData($transactionReceipt)->verifyReceipt();
        $status = $receiptResponse->getStatus();
        $is_valid = $status->isValid();
        $value_status = $status->getValue();
        $environment = $receiptResponse->getEnvironment();
        $receipt = $receiptResponse->getReceipt();
        $latest_receipt = $receiptResponse->getLatestReceipt();
        $latest_receipt_info = $receiptResponse->getLatestReceiptInfo();
        // $receipt->getInApp()

        
        $data = [
            'is_valid'      => $is_valid,
            'value_status'  => $value_status,
            'latest_receipt'    => $latest_receipt,
        ];
        foreach($latest_receipt_info as $row_receipt_info) {
            $item_id = $row_receipt_info->getProductId();
            $start_time = $row_receipt_info->getPurchaseDate()->toDateTime()->format('Y-m-d H:i:s');
            $expiry_time = $row_receipt_info->getExpiresDate()->toDateTime()->format('Y-m-d H:i:s');
            $is_valid = !$row_receipt_info->isCancelled();

            $order_id = $row_receipt_info->getTransactionId();
            $data_rate = [
                'type'          => $item_id,
                'date_start'    => $start_time,
                'date_end'      => $expiry_time,
                'platform'      => 'apple',
                'uid'           => $order_id,
                'data'          => json_encode($data),
                'enabled'       => $is_valid
            ];
            $rate = Rates::where('uid', '=', $order_id)->where('platform', '=', 'apple')->first();
            if($rate) {
                $rate->fill($data_rate)->save();
            } elseif(!is_null($id_user)) {
                $data_rate['user_id'] = $id_user;
                Rates::create($data_rate);
            }

        }

        return count($latest_receipt_info) > 0;
    }
    public function android_notification(Request $request)
    {
        $post = $request->toArray();
        $developerNotification = DeveloperNotification::parse($post['message']['data']);
        $subscriptionNotification = $developerNotification->getSubscriptionNotification();

        $this->save_android_rate(
            $subscriptionNotification->getSubscriptionId(),
            $subscriptionNotification->getPurchaseToken(),
            $developerNotification->getPackageName()
        );
    }
    public function apple_notification(Request $request)
    {
    }
    public function save_android_rate($item_id, $token, $package_name, $id_user=NULL) {
        $subscriptionReceipt  = Subscription::googlePlay()->packageName($package_name)->id($item_id)->token($token)->get();
        $start_time = $subscriptionReceipt->getStartTime()->toDateTime()->format('Y-m-d H:i:s');
        $expiry_time = $subscriptionReceipt->getExpiryTime()->toDateTime()->format('Y-m-d H:i:s');
        $order_id = $subscriptionReceipt->getOrderId();

        $auto_renewing = $subscriptionReceipt->isAutoRenewing();  // Будет ли подписка автоматически продлеваться по истечении текущего срока действия.
        $price_amount_micros = $subscriptionReceipt->getPriceAmountMicros(); // Цена выражается в микроединицах
        $price_currency_code = $subscriptionReceipt->getPriceCurrencyCode();
        $country_code = $subscriptionReceipt->getCountryCode();
        $cancellation = $subscriptionReceipt->getCancellation();

        $is_cancelled = $cancellation->isCancelled();
        $cancel_reason = NULL;
        $user_cancellation_time = NULL;
        if($is_cancelled) {

            $cancel_reason = $cancellation->getCancelReason()->__toString();
            $user_cancellation_time = $cancellation->getUserCancellationTime();
        }
        $data = [
            'package_name'  => $package_name,
            'item_id'       => $item_id,
            'token'         => $token,
            'start_time'    => $start_time,
            'expiry_time'   => $expiry_time,
            'order_id'      => $order_id,
            'auto_renewing' => $auto_renewing,
            'price_amount_micros' => $price_amount_micros,
            'price_currency_code'   => $price_currency_code,
            'country_code'  => $country_code,
            'is_cancelled'  => $is_cancelled,
            'cancel_reason' => $cancel_reason,
            'user_cancellation_time'    => $user_cancellation_time
        ];
        $data_rate = [
            'type'          => $item_id,
            'date_start'    => $start_time,
            'date_end'      => $expiry_time,
            'platform'      => 'google',
            'uid'           => $order_id,
            'data'          => json_encode($data),
            'enabled'       => !$is_cancelled
        ];
        $rate = Rates::where('uid', '=', $order_id)->where('platform', '=', 'google')->first();
        if($rate) {
            $rate->fill($data_rate)->save();
            return true;
        } elseif(!is_null($id_user)) {
            $data_rate['user_id'] = $id_user;
            Rates::create($data_rate);
            return true;
        }
        return false;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Rates  $rates
     * @return \Illuminate\Http\Response
     */
    public function show(Rates $rates)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Rates  $rates
     * @return \Illuminate\Http\Response
     */
    public function edit(Rates $rates)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Rates  $rates
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rates $rates)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Rates  $rates
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rates $rates)
    {
        //
    }
}
