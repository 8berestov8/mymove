<?php

namespace App\Http\Controllers;

use App\Models\SchoolsByClient;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\StudentsController;
use Illuminate\Http\Request;
use App\Http\Requests\SchoolRequest;
use Illuminate\Support\Facades\DB;

class SchoolsByClientController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public static function get_schools(Request $request) {
        $user = UsersController::get_user_by_session($request);
        $schools = SchoolsByClient::whereHas('students', function($query) use($user) {
                $query->where('phone', '=', $user->phone);
            })
            ->orderBy('id', 'DESC')
            ->get();
        foreach($schools as &$school) {
            $school->student = StudentsController::students()
                ->where('school_id', '=', $school->id)
                ->where('phone', '=', $user->phone)
                ->first();
        }
        return $schools;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function show(Shools $shools)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function edit(Shools $shools)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shools $shools)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shools $shools)
    {
        //
    }
}
