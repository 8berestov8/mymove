<?php

namespace App\Http\Controllers;

use App\Models\Schools;
use App\Http\Controllers\UsersController;
use Illuminate\Http\Request;
use App\Http\Requests\SchoolRequest;

class SchoolsController extends Controller
{
    private function save_avatar($logo) {
        $image = InterventionImage::make($logo)->orientate()->resize(null, 500, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->encode('jpg', 80);
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $logo_path = "$year/$month/$day/$user->id.jpg";
        Storage::disk('logos')->put($logo_path, $image->getEncoded());
        return $logo_path;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  SchoolRequest $request
     * @return \Illuminate\Http\Response
     */
    public function add_school(SchoolRequest $request) {
        $user = UsersController::get_user_by_session($request);
        $logo = null;
        if($request->hasFile('logo')) {
            $logo = $this->save_avatar($request->file('logo'));
        }
        $school = Schools::create(array_merge($request->validated(), [
            'user_id'       => $user->id,
            'logo'          => $logo,
        ]));
        return $school->find($school->id);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  SchoolRequest $request
     * @return \Illuminate\Http\Response
     */
    public function edit_school(SchoolRequest $request) {
        $school = Schools::findOrFail($request->school_id);
        $fields = $request->validated();
        if($request->hasFile('logo')) {
            $fields['logo'] = $this->save_avatar($request->file('logo'));
        }
        $school->fill($fields)->save();
        return $school;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function get_schools(SchoolRequest $request) {
        $user = UsersController::get_user_by_session($request);
        $schools = Schools::where('user_id', '=', $user->id)->orderBy('id', 'DESC')->get();
        return $schools;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function delete_school(SchoolRequest $request) {
        $school = Schools::findOrFail($request->validated()['school_id']);
        return $school->delete();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function show(Shools $shools)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function edit(Shools $shools)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shools $shools)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shools  $shools
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shools $shools)
    {
        //
    }
}
