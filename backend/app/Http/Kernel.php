<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        // \App\Http\Middleware\TrustHosts::class,
        \App\Http\Middleware\TrustProxies::class,
        \Fruitcake\Cors\HandleCors::class,
        \App\Http\Middleware\PreventRequestsDuringMaintenance::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:api',
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'password.confirm' => \Illuminate\Auth\Middleware\RequirePassword::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
        'verifySession' => \App\Http\Middleware\VerifySession::class,
        'verifyPublicKey' => \App\Http\Middleware\VerifyPublicKey::class,
        'onlyTypeOwnerOrTeacher'    => \App\Http\Middleware\OnlyTypeOwnerOrTeacher::class,
        'onlyTypeOwner' => \App\Http\Middleware\OnlyTypeOwner::class,
        'onlyTypeTeacher' => \App\Http\Middleware\OnlyTypeTeacher::class,
        'onlyTypeStudent' => \App\Http\Middleware\OnlyTypeStudent::class,
        'onlyTypeClient' => \App\Http\Middleware\OnlyTypeClient::class,
        'onlyOwnerSchool' => \App\Http\Middleware\OnlyOwnerSchool::class,


        'onlyOwnerOrTeacherSchool'  => \App\Http\Middleware\OnlyOwnerOrTeacherSchool::class,

        'AccessTeacher.CheckIdSchool' => \App\Http\Middleware\AccessTeacher\CheckIdSchool::class,
        'AccessTeacher.CheckIdLessonPlus' => \App\Http\Middleware\AccessTeacher\CheckIdLessonPlus::class,
        'AccessTeacher.CheckIdLessonMinus' => \App\Http\Middleware\AccessTeacher\CheckIdLessonMinus::class,
        'AccessTeacher.CheckStudentsXref' => \App\Http\Middleware\AccessTeacher\CheckStudentsXref::class,

        'AccessClient.CheckIdStudent' => \App\Http\Middleware\AccessClient\CheckIdStudent::class,

        'AccessOwner.CheckIdSchool' => \App\Http\Middleware\AccessOwner\CheckIdSchool::class,
        'AccessOwner.CheckIdTeacher' => \App\Http\Middleware\AccessOwner\CheckIdTeacher::class,
        'AccessOwner.CheckTeachersXref' => \App\Http\Middleware\AccessOwner\CheckTeachersXref::class,
        'AccessOwner.CheckIdStudent' => \App\Http\Middleware\AccessOwner\CheckIdStudent::class,
        'AccessOwner.CheckIdService' => \App\Http\Middleware\AccessOwner\CheckIdService::class,
        'AccessOwner.CheckIdLessonPlus'   => \App\Http\Middleware\AccessOwner\CheckIdLessonPlus::class,
        'AccessOwner.CheckIdLessonMinus'   => \App\Http\Middleware\AccessOwner\CheckIdLessonMinus::class,
        'AccessOwner.CheckIdFinance'   => \App\Http\Middleware\AccessOwner\CheckIdFinance::class,


        'AccessToSchool.CheckIdStudent' => \App\Http\Middleware\AccessToSchool\CheckIdStudent::class,
        'AccessToSchool.CheckIdLessonPlus' => \App\Http\Middleware\AccessToSchool\CheckIdLessonPlus::class,
        'AccessToSchool.CheckIdTeacher' => \App\Http\Middleware\AccessToSchool\CheckIdTeacher::class,


        'checkLessonPlus'   => \App\Http\Middleware\CheckLessonPlus::class,
    ];
}
