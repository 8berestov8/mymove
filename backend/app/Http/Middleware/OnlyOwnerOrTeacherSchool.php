<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use App\Http\Controllers\UsersController;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Schools;
use App\Http\Controllers\SchoolsByTeacherController;

class OnlyOwnerOrTeacherSchool
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $school_id = intval($request->route('school_id'));
        $user = UsersController::get_user_by_session($request);
        $school = Schools::where('id', '=', $school_id)->where('user_id', '=', $user->id)->first();
        $schools = SchoolsByTeacherController::get_schools($request);
        $key_school = array_search($school_id, array_column($schools->toArray(), 'id'));
        if($school || $key_school !== false) {
            return $next($request);
        } else {
            abort(403, 'Не достаточно прав');
        }
    }
}
