<?php

namespace App\Http\Middleware\AccessTeacher;

use App\Providers\RouteServiceProvider;
use App\Http\Controllers\TeachersController;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Schools;
use App\Models\Students;

class CheckStudentsXref
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $teacher = TeachersController::get_teacher_by_session($request, $request->route('school_id'));

        $teacher = Schools::where('id', '=', $request->route('school_id'))
            ->first()
            ->whereHas('teachers.students_xref', function($query) use($teacher, $request) {
                $query->where('teacher_id', '=', $teacher->id);
                $query->where('student_id', '=', $request->route('student_id'));
            })
            ->first();
        if($teacher) {
            return $next($request);
        } else {
            abort(403, 'Не достаточно прав');
        }
    }
}
