<?php

namespace App\Http\Middleware\AccessTeacher;

use App\Providers\RouteServiceProvider;
use App\Http\Controllers\UsersController;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Schools;
use App\Models\Teachers;

class CheckIdSchool
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $user = UsersController::get_user_by_session($request);
        $teacher = Teachers::where('school_id', '=', $request->route('school_id'))->where('phone', '=', $user->phone)->first();
        if($teacher) {
            return $next($request);
        } else {
            abort(403, 'Не достаточно прав');
        }
    }
}
