<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use App\Http\Controllers\UsersController;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Users;

class verifyPublicKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $public_key = strval($request->public_key);
        $user_id = $request->route('user_id');
        if(!empty($public_key) && $user_id > 0) {
            $user = Users::find($user_id)->where('public_key', '=', $public_key)->first();
        }
        if(isset($user) && $user) {
            return $next($request);
        } else {
            abort(403, 'Доступ закрыт по публичному ключу');
        }
    }
}
