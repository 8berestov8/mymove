<?php

namespace App\Http\Middleware\AccessToSchool;

use App\Providers\RouteServiceProvider;
use App\Http\Controllers\UsersController;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Schools;
use App\Models\Students;

class CheckIdStudent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $user = UsersController::get_user_by_session($request);

        $school = Schools::where('id', '=', $request->route('school_id'))->first();
        if(!$school) {
            abort(403, 'Не достаточно прав');
        }
        $student = Students::where('id', '=', $request->student_id)->where('school_id', '=', $school->id)->first();
        if(!$student) {
            abort(403, 'Не достаточно прав');
        }

        return $next($request);
    }
}
