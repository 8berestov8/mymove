<?php

namespace App\Http\Middleware\AccessClient;

use App\Providers\RouteServiceProvider;
use App\Http\Controllers\UsersController;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Schools;
use App\Models\Students;

class CheckIdStudent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $user = UsersController::get_user_by_session($request);
        $student = Students::where('school_id', '=', $request->route('school_id'))
            ->where('id', '=', $request->student_id)
            ->where('phone', '=', $user->phone)
            ->first();
        if($student) {
            return $next($request);
        } else {
            abort(403, 'Не достаточно прав');
        }
    }
}
