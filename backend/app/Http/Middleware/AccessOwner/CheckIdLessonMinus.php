<?php

namespace App\Http\Middleware\AccessOwner;

use App\Providers\RouteServiceProvider;
use App\Http\Controllers\UsersController;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Schools;
use App\Models\LessonsMinus;

class CheckIdLessonMinus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $user = UsersController::get_user_by_session($request);

        $school = Schools::where('id', '=', $request->route('school_id'))->where('user_id', '=', $user->id)->first();
        $lesson_plus = LessonsMinus::where('id', '=', $request->route('lesson_minus_id'))->where('school_id', '=', $school->id)->first();
        if($lesson_plus) {
            return $next($request);
        } else {
            abort(403, 'Не достаточно прав');
        }
    }
}
