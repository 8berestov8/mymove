<?php

namespace App\Http\Middleware\AccessOwner;

use App\Providers\RouteServiceProvider;
use App\Http\Controllers\UsersController;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Schools;
use App\Models\WorkTime;

class CheckIdWorkTime
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $work_time = WorkTime::where('id', '=', $request->route('work_time_id'))->where('school_id', '=', $request->route('school_id'))->first();
        if($work_time) {
            return $next($request);
        } else {
            abort(403, 'Не достаточно прав');
        }
    }
}
