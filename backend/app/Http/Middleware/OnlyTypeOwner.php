<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use App\Http\Controllers\UsersController;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OnlyTypeOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $session = UsersController::get_session($request);
        if($session->type == 'owner') {
            return $next($request);
        } else {
            abort(403, 'Доступ только для руководителей');
        }
    }
}
