<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class SchoolRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod())
        {
            case 'PUT':
            case 'POST':
                return [
                    'name'          => 'required|string',
                    'contact_phone' => 'string',
                    'lat'           => '',
                    'lng'           => '',
                    'logo'          => 'image|mimes:jpeg,png,jpg,gif,svg|max:8192',
                    'address'       => '',
                ];
            break;
            case 'GET':
                return [];
            break;
            case 'DELETE':
                return [
                    'school_id' => 'required:exists:schools,id'
                ];
            break;
        }
    }

    public function all($keys = null)
    {
        $data = parent::all($keys);
        $data['school_id'] = $this->route('school_id');

        return $data;
    }

}
