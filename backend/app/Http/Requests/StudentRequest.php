<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;

class StudentRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod())
        {
            case 'PUT':
            case 'POST':
                return [
                    'school_id'    => 'required:exists:schools,id',
                    'firstname'     => 'required|string',
                    'lastname'      => 'required|string',
                    'patronymic'    => 'required|string',
                    'birth_date'    => 'date|nullable|date_format:"Y-m-d"',
                    'phone'         => [
                        'required',
                        'regex:/^7[0-9]{10}$/',
                        Rule::unique('students')->whereNotIn('id', [intval($this->student_id)])->where('phone', $this->phone)->where('school_id', $this->school_id)
                    ],
                    'teachers_xref.*.teacher_id'  => 'required:exists:teachers,id'
                ];
            break;
            case 'GET':
                return [
                    'school_id' => 'required:exists:schools,id'
                ];
            break;
            case 'DELETE':
                return [
                    'student_id' => 'required:exists:students,id'
                ];
            break;
        }
    }
    public function messages()
    {
        return [
            'phone.unique' => 'Ученик с телефоном ' . $this->phone . ' уже добавлен',
        ];
    }
    public function all($keys = null)
    {
        $data = parent::all($keys);
        $data['school_id'] = $this->route('school_id');
        $data['student_id'] = $this->route('student_id');

        return $data;
    }

}
