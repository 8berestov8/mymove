<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class LessonMinusRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod())
        {
            case 'PUT':
            case 'POST':
                return [

                    'school_id'    => 'required:exists:schools,id',
                    'lesson_plus_id'    => 'required|exists:lessons_plus,id',
                    'student_id'    => 'required|exists:students,id',
                    'date_visit'    => 'date|date_format:"Y-m-d"',
                    'count_lessons' => 'integer|nullable',
                ];
            break;
            case 'GET':
                return [
                    'lesson_plus_id' => 'required:exists:lessons_plus,id'
                ];
            break;
            case 'DELETE':
                return [
                    'lesson_minus_id' => 'required:exists:lessons_minus,id'
                ];
            break;
        }
    }
    public function all($keys = null)
    {
        $data = parent::all($keys);
        $data['school_id'] = $this->route('school_id');
        $data['lesson_minus_id'] = $this->route('lesson_minus_id');

        return $data;
    }

}
