<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class ServiceRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod())
        {
            case 'PUT':
            case 'POST':
                return [
                    'name'          => 'required|string',
                    'period'        => 'required|string',
                    'price'         => 'required|numeric',
                    'count_lessons' => 'integer|nullable',
                    'count_days'    => 'integer|nullable',
                    'infinity_lessons'  => 'required|boolean',
                    'school_id'    => 'required:exists:schools,id'
                ];
            break;
            case 'GET':
                return [
                    'school_id'    => 'required:exists:schools,id'
                ];
            break;
            case 'DELETE':
                return [
                    'service_id' => 'required:exists:services,id'
                ];
            break;
        }
    }

    public function all($keys = null)
    {
        $data = parent::all($keys);
        $data['school_id'] = $this->route('school_id');
        $data['service_id'] = $this->route('service_id');

        return $data;
    }
}
