<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class WorkTimeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod())
        {
            case 'PUT':
            case 'POST':
                return [
                    'school_id'     => 'required|exists:schools,id',
                    'teacher_id'    => 'required|exists:teachers,id',
                    'count_hours'   => 'required|numeric',
                    'count_lessons' => 'required|numeric',
                    'date'          => 'required|date_format:"Y-m-d"',
                    'time'          => 'string',
                ];
            break;
            case 'GET':
                return [
                    'school_id' => 'required:exists:schools,id',
                    'teacher_id' => 'required:exists:teachers,id'
                ];
            break;
            case 'DELETE':
                return [
                    'work_time_id' => 'required:exists:work_times,id'
                ];
            break;
        }
    }
    public function all($keys = null)
    {
        $data = parent::all($keys);
        $data['school_id'] = $this->route('school_id');
        $data['work_time_id'] = $this->route('work_time_id');

        return $data;
    }

}
