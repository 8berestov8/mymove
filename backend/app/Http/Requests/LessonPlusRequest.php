<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class LessonPlusRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod())
        {
            case 'PUT':
            case 'POST':
                return [
                    'school_id'    => 'required:exists:schools,id',
                    'service_id'    => 'required|exists:services,id',
                    'student_id'    => 'required|exists:students,id',
                    'date_start'    => 'date|date_format:"Y-m-d"',
                    'date_end'      => 'date|date_format:"Y-m-d"',
                    'count_lessons' => 'integer|nullable',
                    'infinity_lessons'  => 'required|boolean',
                ];
            break;
            case 'GET':
                return [
                    'student_id'    => 'required:exists:students,id'
                ];
            break;
            case 'DELETE':
                return [
                    'lesson_plus_id' => 'required:exists:lessons_plus,id'
                ];
            break;
        }
    }
    public function all($keys = null)
    {
        $data = parent::all($keys);
        $data['lesson_plus_id'] = $this->route('lesson_plus_id');
        $data['school_id'] = $this->route('school_id');

        return $data;
    }

}
