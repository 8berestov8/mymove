<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class FinanceRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod())
        {
            case 'PUT':
            case 'POST':
                return [
                    'school_id'     => 'required|exists:schools,id',
                    'type'          => 'required|in:income,expenditure',
                    'status'        => 'required|in:new,confirmed,canceled',
                    'sum'           => 'required|numeric',
                    'date'          => 'required|date_format:"Y-m-d"',
                    'description'   => 'string|nullable',
                ];
            break;
            case 'GET':
                return [
                    'school_id' => 'required:exists:schools,id'
                ];
            break;
            case 'DELETE':
                return [
                    'finance_id' => 'required:exists:finances,id'
                ];
            break;
        }
    }
    public function all($keys = null)
    {
        $data = parent::all($keys);
        $data['school_id'] = $this->route('school_id');
        $data['finance_id'] = $this->route('finance_id');

        return $data;
    }

}
