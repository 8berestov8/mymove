<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class CheckSmsCodeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id'    => 'required',
            'code'  => 'required|regex:/^[0-9]{4}$/',
            'type'  => 'required|in:client,teacher,owner'
        ];
        return $rules;
    }

}
