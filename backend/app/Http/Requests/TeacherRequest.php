<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;

class TeacherRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod())
        {
            case 'PUT':
            case 'POST':
                return [
                    'school_id'    => 'required:exists:schools,id',
                    'firstname'     => 'required|string',
                    'lastname'      => 'required|string',
                    'patronymic'    => 'required|string',
                    'birth_date'    => 'date|nullable|date_format:"Y-m-d"',
                    'phone'         => [
                        'required',
                        'regex:/^7[0-9]{10}$/',
                        Rule::unique('teachers')->whereNotIn('id', [intval($this->teacher_id)])->where('phone', $this->phone)->where('school_id', $this->school_id)
                    ]
                ];
            break;
            case 'GET':
                return [
                    'school_id' => 'required:exists:schools,id'
                ];
            break;
            case 'DELETE':
                return [
                    'teacher_id' => 'required:exists:teachers,id'
                ];
            break;
        }
    }
    public function messages()
    {
        return [
            'phone.unique' => 'Тренер с телефоном ' . $this->phone . ' уже добавлен',
        ];
    }
    public function all($keys = null)
    {
        $data = parent::all($keys);
        $data['school_id'] = $this->route('school_id');
        $data['teacher_id'] = $this->route('teacher_id');
        return $data;
    }

}
