<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class ProfileRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'firstname'     => 'required|string',
            'lastname'      => 'required|string',
            'patronymic'    => 'required|string',
            'avatar'        => 'image|mimes:jpeg,png,jpg,gif,svg|max:8192'
        ];
        return $rules;
    }

}
