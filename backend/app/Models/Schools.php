<?php

namespace App\Models;

use App\Models\Teachers;
use App\Models\Students;
use App\Models\Services;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schools extends Model
{
    protected $withCount = ['teachers', 'students', 'services'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'contact_phone',
        'address',
        'lat',
        'lng',
        'logo',
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function teachers()
    {
        return $this->hasMany(Teachers::class, 'school_id', 'id');
    }
    public function students()
    {
        return $this->hasMany(Students::class, 'school_id', 'id');
    }
    public function services()
    {
        return $this->hasMany(Services::class, 'school_id', 'id');
    }


    use HasFactory;
}
