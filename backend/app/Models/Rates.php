<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rates extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'type',
        'date_start',
        'date_end',
        'enabled',
        'platform',
        'sum',
        'data',
	'uid',
    ];
}
