<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\StudentsTeachersXref;


class Teachers extends Model
{

    public function students_xref()
    {
        return $this->hasMany(StudentsTeachersXref::class, 'teacher_id', 'id');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'surname',
        'phone',
        'patronymic',
        'birth_date',
        'school_id',
    ];

    protected $hidden = ['created_at', 'updated_at'];
    use HasFactory;
}
