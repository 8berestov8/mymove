<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Users extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'surname',
        'phone',
        'patronymic',
        'public_key',
        'public_updated_at',
        'avatar',
    ];

    protected $hidden = ['created_at', 'updated_at', 'public_key', 'public_updated_at'];
    use HasFactory;

    public function getAvatarAttribute()
    {
        return !empty($this->attributes['avatar']) ? Storage::disk('avatars')->url($this->attributes['avatar']) . '?' . time() : NULL;
    }
}
