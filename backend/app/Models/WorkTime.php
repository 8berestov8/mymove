<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkTime extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'school_id',
        'teacher_id',
        'date',
        'count_lessons',
        'count_hours',
        'time',
    ];

    protected $hidden = ['created_at', 'updated_at'];
    use HasFactory;
}
