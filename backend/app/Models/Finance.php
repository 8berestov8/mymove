<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Finance extends Model
{
    protected $fillable = [
        'id',
        'type',
        'sum',
        'date',
        'description',
        'user_id',
        'auto',
        'school_id',
        'status',
    ];

    protected $hidden = ['created_at', 'updated_at', 'code'];
    use HasFactory;
}
