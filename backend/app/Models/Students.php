<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\StudentsTeachersXref;

class Students extends Model
{

    protected $with = ['teachers_xref'];
    public function teachers_xref()
    {
        return $this->hasMany(StudentsTeachersXref::class, 'student_id', 'id');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'surname',
        'phone',
        'patronymic',
        'birth_date',
        'school_id',
    ];

    protected $hidden = ['created_at', 'updated_at'];
    use HasFactory;
}
