<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Teachers;
use App\Models\Students;

class StudentsTeachersXref extends Model
{

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'teacher_id',
        'student_id',
    ];
    use HasFactory;

    public function teachers()
    {
        return $this->hasMany(Teachers::class, 'id', 'teacher_id');
    }

    public function students()
    {
        return $this->hasMany(Teachers::class, 'id', 'student_id');
    }
}
