<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Students;

class LessonsMinus extends Model
{
    public $table = 'lessons_minus';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'school_id',
        'lesson_plus_id',
        'student_id',
        'date_visit',
        'count_lessons',
    ];

    public function student()
    {
        return $this->hasOne(Students::class, 'id', 'student_id');
    }

    protected $hidden = ['created_at', 'updated_at'];
    use HasFactory;
}
