<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Students;

class LessonsPlus extends Model
{
    public $table = 'lessons_plus';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'school_id',
        'service_id',
        'student_id',
        'date_start',
        'date_end',
        'count_lessons',
        'infinity_lessons',
    ];

    public function student()
    {
        return $this->hasOne(Students::class, 'id', 'student_id');
    }

    protected $hidden = ['created_at', 'updated_at'];
    use HasFactory;
}
