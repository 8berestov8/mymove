<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Services extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'school_id',
        'name',
        'price',
        'period',
        'count_days',
        'count_lessons',
        'infinity_lessons',

    ];

    protected $hidden = ['created_at', 'updated_at'];
    use HasFactory;
}
