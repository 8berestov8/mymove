<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/**
 * Auth
 */
Route::get('/check_phone', 'CheckPhonesController@check_phone');
Route::get('/check_sms_code', 'CheckPhonesController@check_sms_code');


Route::middleware(['verifySession'])->group(function () {
    /**
     * Profile
     */
    Route::post('/profile', 'UsersController@save_profile');
    Route::get('/profile', 'UsersController@get_profile');
});


Route::middleware(['verifySession', 'onlyTypeClient'])->group(function () {
    /**
     * School by client
     */
    Route::get('/client/schools', 'SchoolsByClientController@get_schools');
});

Route::middleware(['verifySession', 'onlyTypeTeacher'])->group(function () {
    /**
     * School by Teacher
     */
    Route::get('/teacher/schools', 'SchoolsByTeacherController@get_schools');
});
Route::middleware(['verifySession', 'onlyTypeTeacher', 'AccessTeacher.CheckIdSchool'])->group(function () {
    /**
     * Lesson plus by Teacher
     */
    Route::get('/teacher/school/{school_id}/lessons_plus', 'LessonsPlusController@get_lessons_plus')->middleware(['AccessTeacher.CheckIdStudent']);
    Route::post('/teacher/school/{school_id}/lesson_plus', 'LessonsPlusController@add_lesson_plus')->middleware(['AccessTeacher.CheckIdStudent']);
    Route::put('/teacher/school/{school_id}/lesson_plus/{lesson_plus_id}', 'LessonsPlusController@edit_lesson_plus')->middleware(['AccessTeacher.CheckIdLessonPlus']);
    Route::delete('/teacher/school/{school_id}/lesson_plus/{lesson_plus_id}', 'LessonsPlusController@delete_lesson_plus')->middleware(['AccessTeacher.CheckIdLessonPlus']);

    /**
     * Lesson minus by Teacher
     */
    Route::get('/teacher/school/{school_id}/lessons_minus', 'LessonsMinusController@get_lessons_minus')->middleware(['AccessToSchool.CheckIdLessonPlus']);
    Route::post('/teacher/school/{school_id}/lesson_minus', 'LessonsMinusController@add_lesson_minus')->middleware(['AccessToSchool.CheckIdLessonPlus']);
    Route::delete('/teacher/school/{school_id}/lesson_minus/{lesson_minus_id}', 'LessonsMinusController@delete_lesson_minus')->middleware(['AccessTeacher.CheckIdLessonMinus']);
    /**
     * Student by Teacher
     */
    Route::post('/teacher/school/{school_id}/student', 'StudentsByTeacherController@add_student');
    Route::get('/teacher/school/{school_id}/students', 'StudentsByTeacherController@get_students');
    Route::put('/teacher/school/{school_id}/student/{student_id}', 'StudentsByTeacherController@edit_student')->middleware(['AccessTeacher.CheckStudentsXref']);
    Route::delete('/teacher/school/{school_id}/student/{student_id}', 'StudentsByTeacherController@delete_student')->middleware(['AccessTeacher.CheckStudentsXref']);

    /**
     * Service by Teacher
     */
    Route::get('/teacher/school/{school_id}/services', 'ServicesController@get_services');
});

Route::middleware(['verifySession', 'verifyPublicKey', 'onlyOwnerOrTeacherSchool'])->group(function () {
    /**
     * User info
     */
    Route::get('/school/{school_id}/get_student_by_user/{user_id}', 'UsersController@get_student_by_user');
});

Route::middleware(['verifySession', 'onlyTypeOwner'])->group(function () {
    /**
     * School by Owner
     */
    Route::get('/owner/schools', 'SchoolsController@get_schools');
    Route::post('/owner/school', 'SchoolsController@add_school');
    Route::put('/owner/school/{school_id}', 'SchoolsController@edit_school')->middleware(['AccessOwner.CheckIdSchool']);
    Route::delete('/owner/school/{school_id}', 'SchoolsController@delete_school')->middleware(['AccessOwner.CheckIdSchool']);
});

Route::middleware(['verifySession', 'onlyTypeOwner', 'AccessOwner.CheckIdSchool'])->group(function () {
    /**
     * Teacher by Owner
     */
    Route::get('/owner/school/{school_id}/teachers', 'TeachersController@get_teachers');
    Route::post('/owner/school/{school_id}/teacher', 'TeachersController@add_teacher');
    Route::put('/owner/school/{school_id}/teacher/{teacher_id}', 'TeachersController@edit_teacher')->middleware(['AccessOwner.CheckIdTeacher']);
    Route::delete('/owner/school/{school_id}/teacher/{teacher_id}', 'TeachersController@delete_teacher')->middleware(['AccessOwner.CheckIdTeacher']);

    /**
     * Student by Owner
     */
    Route::post('/owner/school/{school_id}/student', 'StudentsController@add_student')->middleware(['AccessOwner.CheckTeachersXref']);
    Route::put('/owner/school/{school_id}/student/{student_id}', 'StudentsController@edit_student')->middleware(['AccessOwner.CheckIdStudent', 'AccessOwner.CheckTeachersXref']);
    Route::get('/owner/school/{school_id}/students', 'StudentsController@get_students');
    Route::delete('/owner/school/{school_id}/student/{student_id}', 'StudentsController@delete_student')->middleware(['AccessOwner.CheckIdStudent']);

    /**
     * Service by Owner
     */
    Route::get('/owner/school/{school_id}/services', 'ServicesController@get_services');
    Route::post('/owner/school/{school_id}/service', 'ServicesController@add_service');
    Route::put('/owner/school/{school_id}/service/{service_id}', 'ServicesController@edit_service')->middleware(['AccessOwner.CheckIdService']);
    Route::delete('/owner/school/{school_id}/service/{service_id}', 'ServicesController@delete_service')->middleware(['AccessOwner.CheckIdService']);

    /**
     * Lesson plus by Owner
     */
    Route::get('/owner/school/{school_id}/lessons_plus', 'LessonsPlusController@get_lessons_plus')->middleware(['AccessToSchool.CheckIdStudent']);
    Route::post('/owner/school/{school_id}/lesson_plus', 'LessonsPlusController@add_lesson_plus')->middleware(['AccessToSchool.CheckIdStudent']);
    Route::put('/owner/school/{school_id}/lesson_plus/{lesson_plus_id}', 'LessonsPlusController@edit_lesson_plus')->middleware(['AccessOwner.CheckIdLessonPlus']);
    Route::delete('/owner/school/{school_id}/lesson_plus/{lesson_plus_id}', 'LessonsPlusController@delete_lesson_plus')->middleware(['AccessOwner.CheckIdLessonPlus']);

    /**
     * Lesson minus by Owner
     */
    Route::get('/owner/school/{school_id}/lessons_minus', 'LessonsMinusController@get_lessons_minus')->middleware(['AccessToSchool.CheckIdLessonPlus']);
    Route::post('/owner/school/{school_id}/lesson_minus', 'LessonsMinusController@add_lesson_minus')->middleware(['AccessToSchool.CheckIdLessonPlus']);
    Route::delete('/owner/school/{school_id}/lesson_minus/{lesson_minus_id}', 'LessonsMinusController@delete_lesson_minus')->middleware(['AccessOwner.CheckIdLessonMinus']);

    /**
     * Finance by Owner
     */
    Route::get('/owner/school/{school_id}/finances', 'FinanceController@get_finances');
    Route::post('/owner/school/{school_id}/finance', 'FinanceController@add_finance');
    Route::put('/owner/school/{school_id}/finance/{finance_id}', 'FinanceController@edit_finance')->middleware(['AccessOwner.CheckIdFinance']);
    Route::delete('/owner/school/{school_id}/finance/{finance_id}', 'FinanceController@delete_finance')->middleware(['AccessOwner.CheckIdFinance']);


    /**
     * Work time by Owner
     */
    Route::get('/owner/school/{school_id}/work_times', 'WorkTimeController@get_work_times')->middleware(['AccessToSchool.CheckIdTeacher']);
    Route::post('/owner/school/{school_id}/work_time', 'WorkTimeController@add_work_time')->middleware(['AccessToSchool.CheckIdTeacher']);
    Route::put('/owner/school/{school_id}/work_time/{work_time_id}', 'WorkTimeController@edit_work_time')->middleware(['AccessOwner.CheckIdWorkTime']);
    Route::delete('/owner/school/{school_id}/work_time/{work_time_id}', 'WorkTimeController@delete_work_time')->middleware(['AccessOwner.CheckIdWorkTime']);
});

Route::middleware(['verifySession', 'onlyTypeOwner'])->group(function () {
    /**
     * Rates
     */
    Route::post('/owner/rates/check', 'RatesController@check');
    //Route::post('/owner/rates/get', 'RatesController@get');
});
Route::post('/androidplay/notification', 'RatesController@android_notification');
Route::post('/applestore/notification', 'RatesController@apple_notification');

Route::middleware(['verifySession', 'onlyTypeClient', 'AccessClient.CheckIdStudent'])->group(function () {
    /**
     * Lesson plus by Client
     */
    Route::get('/client/school/{school_id}/lessons_plus', 'LessonsPlusController@get_lessons_plus');
});

Route::middleware(['verifySession', 'onlyTypeClient'])->group(function () {
    /**
     * Lesson minus by Client
     */
    Route::get('/client/school/{school_id}/lessons_minus', 'LessonsMinusController@get_lessons_minus')->middleware(['AccessToSchool.CheckIdLessonPlus', 'AccessClient.CheckIdLessonPlus']);
});
