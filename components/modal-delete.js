import React from 'react';
import { StyleSheet } from 'react-native';
import {
    Button,
    Card,
    Text,
    Modal,
    Layout,
    Input,
  
  } from '@ui-kitten/components';

const ModalForm = ({confirmDelete, setVisible}) => {
  const [inputDelete, setInputDelete] = React.useState('');
  const [statusInputDelete, setStatusInputDelete] = React.useState('basic');
  return (
    <Card >
      <Text style={styles.textConfirm}>Вы действительно хотите удалить?</Text>
      <Text>Все данные будут удалены безвозвратно.</Text>
      <Input
        style={styles.inputDeleted}
        label='Для подтверждения введите "удалить"'
        value={inputDelete}
        onChangeText={setInputDelete}
        status={statusInputDelete}
      />
      <Layout style={styles.buttons}>
        <Button appearance='ghost' onPress={() => setVisible(false)}>
          Отмена
        </Button>
        <Button onPress={() => {
          if(inputDelete === 'удалить') {
            setStatusInputDelete('basic');
            setInputDelete('');
            confirmDelete();
          } else {
            setStatusInputDelete('danger');
          }
        }}>
          Удалить
        </Button>
      </Layout>
    </Card>
  )

}
export default ({visible, setVisible, confirmDelete}) => {


    return (
        <Modal
          visible={visible}
          backdropStyle={styles.backdrop}
          onBackdropPress={() => setVisible(false)}
        >
          <ModalForm
            confirmDelete={confirmDelete}
            setVisible={setVisible}
          />
        </Modal>
    );
  }

  const styles = StyleSheet.create({

    textConfirm: {
      fontSize: 18,
      fontWeight: 'bold',
    },
    inputDeleted: {
      marginVertical: 8,
    },
    buttons: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      marginVertical: 8,
    }, 
    backdrop: {
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
  });
  