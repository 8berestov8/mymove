import React from 'react';
import { StyleSheet } from 'react-native';
import {
    Button,
    Card,
    Text,
    Modal,
    Layout,
    Input,
  
  } from '@ui-kitten/components';

const ModalForm = ({confirm, setVisible, title, subtitle, buttonOk='ОК'}) => {
  const [inputDelete, setInputDelete] = React.useState('');
  const [statusInputDelete, setStatusInputDelete] = React.useState('basic');
  return (
    <Card >
      <Text style={styles.textConfirm}>{title}</Text>
      <Text>{subtitle}</Text>
      <Layout style={styles.buttons}>
        <Button appearance='ghost' onPress={() => setVisible(false)}>
          Отмена
        </Button>
        <Button onPress={() => {
          confirm();
        }}>
          {buttonOk}
        </Button>
      </Layout>
    </Card>
  )

}
export default ({visible, setVisible, confirm, title, subtitle}) => {


    return (
        <Modal
          visible={visible}
          backdropStyle={styles.backdrop}
          onBackdropPress={() => setVisible(false)}
        >
          <ModalForm
            confirm={confirm}
            setVisible={setVisible}
            title={title}
            subtitle={subtitle}
          />
        </Modal>
    );
  }

  const styles = StyleSheet.create({

    textConfirm: {
      fontSize: 18,
      fontWeight: 'bold',
    },
    inputDeleted: {
      marginVertical: 8,
    },
    buttons: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      marginVertical: 8,
    }, 
    backdrop: {
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
  });
  