import React from 'react';
import { ImageStyle } from 'react-native';
import { Icon, IconElement } from '@ui-kitten/components';

export const ArrowHeadUpIcon = (style: ImageStyle): IconElement => (
  <Icon {...style} name='arrowhead-up'/>
);

export const ArrowHeadDownIcon = (style: ImageStyle): IconElement => (
  <Icon {...style} name='arrowhead-down'/>
);

export const HeartIcon = (style: ImageStyle): IconElement => (
  <Icon {...style} name='heart'/>
);

export const MessageCircleIcon = (style: ImageStyle): IconElement => (
  <Icon {...style} name='message-circle-outline'/>
);

export const MoreHorizontalIcon = (style: ImageStyle): IconElement => (
  <Icon {...style} name='more-horizontal'/>
);

export const ArrowBackIconOutline = (style: ImageStyle): IconElement => (
  <Icon {...style} name='arrow-back-outline'/>
);