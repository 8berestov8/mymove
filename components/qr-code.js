import React from 'react'
import QRCode from 'react-native-qrcode-svg'

export default (props) => {
  console.log(props)
  return <QRCode value={props.link} />
}
