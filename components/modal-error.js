import React from 'react';
import { StyleSheet } from 'react-native';
import {
    Button,
    Card,
    Text,
    Modal,
  
  } from '@ui-kitten/components';


export default ({error, setError}) => {
    return (
      <Modal
        visible={error.length}
        backdropStyle={styles.backdrop}
        onBackdropPress={() => setError('')}>
        <Card disabled={true}>
          <Text style={styles.title}>{error}</Text>
          <Button onPress={() => setError('')}>
            Понятно
          </Button>
        </Card>
      </Modal>
    );
  }

  const styles = StyleSheet.create({
    title: {
      fontSize: 18,
      marginVertical: 20,
    },  
    backdrop: {
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
  });
  