import React from 'react';
import { ActivityIndicator, StyleSheet } from 'react-native';

const Loading = () => (
	<ActivityIndicator
		size="large"
		color="rgb(156, 0, 193)"
		style={styles.loading}
	/>
);

const styles = StyleSheet.create({
	loading: {
		flex: 1,
	},
});

export default Loading;
