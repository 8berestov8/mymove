import React from 'react';
import { StyleSheet, View } from 'react-native';
import {
    Text,
    Icon,
    Layout
} from '@ui-kitten/components';


export default ({text='Ничего не найдено.'}) => (
    <View style={styles.layout}>
        <Icon
            style={styles.icon}
            fill='#8F9BB3'
            name='search-outline'
        />
        <Text style={styles.text}>{text}</Text>
    </View>
)

const styles = StyleSheet.create({
    layout: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        marginVertical: 20
    },
    icon: {
      width: 48,
      height: 48,
    },
    text: {
        fontSize: 14,
        textAlign: 'center',
        lineHeight: 22,
    }
});