import React from 'react';
import {
	Icon,
	TopNavigation,
	TopNavigationAction,
	useTheme,
	Text,
} from '@ui-kitten/components';
import { goBack } from '../RootNavigation';
const BackIcon = (props) => <Icon {...props} fill="#FFF" name="arrow-back" />;
const BackButton = (props) => (
	<TopNavigationAction
		icon={BackIcon}
		onPress={() => {
			goBack();
		}}
	/>
);
const PlusIcon = (props) => <Icon {...props} fill="#FFF" name="plus-outline" />;

export default (props) => {
	const PlusButton = () => (
		<TopNavigationAction icon={PlusIcon} onPress={props.onPressPlus} />
	);
	return (
		<TopNavigation
			style={{ backgroundColor: useTheme()['color-primary-default'] }}
			alignment="center"
			accessoryLeft={BackButton}
			accessoryRight={
				typeof props.onPressPlus !== 'undefined' ? PlusButton : null
			}
			{...props}
			title={
				<Text style={{ fontWeight: '600', color: '#FFF' }}>{props.title}</Text>
			}
		/>
	);
};
