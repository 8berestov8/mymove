import React from 'react';
import { View } from 'react-native';
import {
  Button,
  Divider,
  Input,
  Layout,
  StyleService,
  useStyleSheet,
  Spinner,
  NativeDateService,
  Datepicker,
  IndexPath,
  Select,
  SelectItem,
} from '@ui-kitten/components';
import { KeyboardAvoidingView } from '../../components/3rd-party';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getLessonPlus } from '../../API/LessonPlus';
import { addLessonMinus, editLessonMinus } from '../../API/LessonMinus';
import { setLessonsMinus } from '../../redux/actions/lessonsMinusActions';
import { setLessonsPlus } from '../../redux/actions/lessonsPlusActions';
import ModalErrorPopup from '../../components/modal-error';
import { calendarLang } from '../../i18n/';
import moment from 'moment';
import 'moment/locale/ru';
import TopNavigation from '../../components/top-navigation';
moment.locale('ru');

const mapStateToProps = (state) => {
  const { user, lessonsMinus, lessonsPlus } = state;
  return { user, lessonsMinus, lessonsPlus };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      setLessonsMinus,
      setLessonsPlus,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  ({
    navigation,
    route,
    lessonsMinus,
    lessonsPlus,
    user,
    setLessonsMinus,
    setLessonsPlus,
  }) => {
    const idLessonMinus = route.params.id;
    const idSchool = route.params.school_id;
    const idStudent = route.params.student_id;
    React.useEffect(() => {
      if (typeof lessonsPlus.list[idStudent] === 'undefined') {
        getLessonPlus({
          user,
          idSchool,
          idStudent,
        })
          .then((data) => {
            setLessonsPlus(idStudent, data);
          })
          .catch((e) => {
            setError(e);
          });
      }
    }, []);

    const type =
      typeof route.params !== 'undefined' &&
      typeof idLessonMinus !== 'undefined'
        ? 'edit'
        : 'add';

    let lesson = {
      service_id: '',
      count_lessons: '1',
      date_visit: new Date(),
    };
    if (type === 'edit') {
      lesson = {
        ...lessonsMinus.list[idStudent].find((v) => v.id == idLessonMinus),
      };
    }
    const lessonsPlusList =
      typeof lessonsPlus.list[idStudent] !== 'undefined'
        ? lessonsPlus.list[idStudent].filter((v) => v.is_validity)
        : [];

    let findIndexPeriod = lessonsPlusList.findIndex(
      (v) => v.id == lesson.service_id
    );
    findIndexPeriod = findIndexPeriod > 0 ? findIndexPeriod : 0;
    const [indexService, setIndexService] = React.useState(
      new IndexPath(findIndexPeriod)
    );
    const _lesson_plus = lessonsPlusList[indexService.row];

    const displayValueService =
      typeof _lesson_plus != 'undefined'
        ? `${moment(_lesson_plus.date_start).format('DD MMM')} - ${moment(
            _lesson_plus.date_end
          ).format('DD MMM')}`
        : '-';

    const styles = useStyleSheet(themedStyles);

    const [dateVisit, setDateVisit] = React.useState(lesson.date_visit);
    const [idLessonPlus, setIdLessonPlus] = React.useState(lesson.service_id);
    const [updateLessonData, setUpdateLessonData] = React.useState(true);

    const [countLessons, setCountLessons] = React.useState(
      lesson.count_lessons.toString()
    );
    const [statusCountLessons, setStatusCountLessons] = React.useState('basic');
    const [statusIdLessonPlus, setStatusIdLessonPlus] = React.useState('basic');
    const [statusDateVisit, setStatusDateVisit] = React.useState('basic');

    const [loading, setLoading] = React.useState(false);
    const [error, setError] = React.useState('');

    if (
      type == 'add' &&
      typeof _lesson_plus !== 'undefined' &&
      updateLessonData
    ) {
      setIdLessonPlus(_lesson_plus.id);
      setUpdateLessonData(false);
    }

    const onSaveButtonPress = () => {
      let hasErrors = false;
      setStatusIdLessonPlus('basic');
      setStatusCountLessons('basic');
      setStatusDateVisit('basic');

      if (idLessonPlus.length === 0) {
        setStatusIdLessonPlus('danger');
        hasErrors = true;
      }
      if (countLessons.length === 0) {
        setStatusCountLessons('danger');
        hasErrors = true;
      }
      if (dateVisit.length === 0) {
        setStatusDateVisit('danger');
        hasErrors = true;
      }
      if (!hasErrors) {
        setLoading(true);
        let formLessonMinus;
        if (type === 'edit') {
          formLessonMinus = editLessonMinus;
        } else {
          formLessonMinus = addLessonMinus;
        }
        formLessonMinus({
          user,
          idSchool,
          idLessonMinus,
          data: {
            lesson_plus_id: idLessonPlus,
            student_id: idStudent,
            count_lessons: countLessons,
            date_visit: moment(dateVisit).format('YYYY-MM-DD'),
          },
        })
          .then((data) => {
            setLoading(false);
            if (typeof lessonsMinus.list[idLessonPlus] !== 'undefined') {
              if (type === 'add') {
                lessonsMinus.list[idLessonPlus].unshift(data);
              } else {
                lessonsMinus.list[idLessonPlus] = lessonsMinus.list[
                  idStudent
                ].map((v) => {
                  return v.id == lesson.id ? data : v;
                });
              }
              setLessonsMinus(idLessonPlus, lessonsMinus.list[idLessonPlus]);
            } else {
              if (type === 'add') {
                const lessonPlus = lessonsPlusList.find(
                  (v) => v.id == idLessonPlus
                );
                lessonPlus.used_lessons =
                  parseInt(lessonPlus.used_lessons) +
                  parseInt(data.count_lessons);
              }
              setLessonsMinus(idLessonPlus, lessonsMinus.list[idLessonPlus]);
            }
            navigation.navigate('LessonsPlusList', {
              school_id: idSchool,
              student_id: idStudent,
            });
          })
          .catch((e) => {
            setLoading(false);
            setError(e);
          });
      }
    };
    const localeDateService = new NativeDateService('ru', {
      format: 'DD.MM.YYYY',
      i18n: calendarLang,
      startDayOfWeek: 1,
    });
    return (
      <KeyboardAvoidingView style={styles.container}>
        <TopNavigation
          title={
            type == 'add' ? 'Отметка занятия' : 'Редактирование отметки занятия'
          }
        />
        {loading && (
          <View style={styles.loadingContainer}>
            <Spinner size="small" />
          </View>
        )}
        <ModalErrorPopup error={error} setError={(err) => setError({ err })} />
        <Layout style={styles.form} level="1">
          <Select
            style={styles.input}
            label="Списать с занятий"
            value={displayValueService}
            selectedIndex={indexService}
            onSelect={(i) =>
              setIndexService(i) || setIdLessonPlus(lessonsPlusList[i.row].id)
            }
            status={statusIdLessonPlus}
            size="large"
          >
            {lessonsPlusList
              ?.filter((v) => v.is_validity)
              .map((p) => (
                <SelectItem
                  key={`lesson_plus_${p.id}`}
                  title={`${moment(p.date_start).format('DD MMM')} - ${moment(
                    p.date_end
                  ).format('DD MMM')}`}
                />
              ))}
          </Select>
          <Input
            style={styles.input}
            label="Количество списываемых занятий"
            value={countLessons}
            onChangeText={setCountLessons}
            status={statusCountLessons}
            keyboardType="numeric"
            size="large"
          />
          <Datepicker
            style={styles.input}
            label="Дата посещения"
            placeholder=""
            date={dateVisit}
            onSelect={setDateVisit}
            dateService={localeDateService}
            status={statusDateVisit}
            size="large"
          />
        </Layout>
        <Divider />
        <Button style={styles.addButton} onPress={onSaveButtonPress}>
          СОХРАНИТЬ
        </Button>
      </KeyboardAvoidingView>
    );
  }
);

const themedStyles = StyleService.create({
  container: {
    flex: 1,
    backgroundColor: 'background-basic-color-2',
  },
  form: {
    flex: 1,
    paddingHorizontal: 4,
    paddingVertical: 24,
  },
  input: {
    marginHorizontal: 12,
    marginVertical: 8,
  },
  middleContainer: {
    flexDirection: 'row',
  },
  middleInput: {
    width: 128,
  },
  addButton: {
    marginHorizontal: 16,
    marginVertical: 24,
  },
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 7777,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginHorizontal: 12,
    marginVertical: 8,
  },
});
