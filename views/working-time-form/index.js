import React from 'react';
import {
    Button,
    Input,
    Layout,
    Card,
    Datepicker,
    StyleService,
    useStyleSheet,
    NativeDateService,
    Divider,
} from '@ui-kitten/components';
import TopNavigation from '../../components/top-navigation';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import moment from 'moment';
import {calendarLang} from '../../i18n/';

const mapStateToProps = (state) => {
    const {user, services, teachers} = state;
    return {user, services, teachers};
};

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(({navigation, route, user}) => {
    const styles = useStyleSheet(themedStyles);
    const [date, setDate] = React.useState(moment(date).toDate());
    const localeDateService = new NativeDateService('ru', {
        format: 'DD.MM.YYYY',
        i18n: calendarLang,
        startDayOfWeek: 1,
    });

    const [quantityLessons, setQuantityLessons] = React.useState('');
    const [quantityHours, setQuantityHours] = React.useState('');
    const [time, setTime] = React.useState('');

    const addWorkingTime = () => {
        console.log(date, time, quantityLessons, quantityHours);
        navigation.navigate('WorkingTimeList');
        setDate('')
        setTime('')
        setQuantityLessons('')
        setQuantityHours('')
    };

    return (
        <Layout style={styles.container} level="2">
            <TopNavigation title="Добавить рабочее время"/>
            <Card style={styles.box}>
                <Datepicker
                    style={styles.input}
                    label="Дата"
                    placeholder=""
                    date={date}
                    onSelect={(nextDate) => setDate(nextDate)}
                    dateService={localeDateService}
                    size="large"
                    value={date}
                    onChangeText={(val) => {
                        setDate(val);
                    }}
                />
                <Input
                    style={styles.input}
                    label="Количество занятий"
                    size="large"
                    value={quantityLessons}
                    keyboardType="numeric"
                    onChangeText={(val) => {
                        setQuantityLessons(val);
                    }}
                />
                <Input
                    style={styles.input}
                    label="Количество часов"
                    size="large"
                    keyboardType="numeric"
                    value={quantityHours}
                    onChangeText={(val) => {
                        setQuantityHours(val);
                    }}
                />
                <Input
                    style={styles.input}
                    label="Время"
                    size="large"
                    keyboardType="numeric"
                    value={time}
                    onChangeText={(val) => {
                        setTime(val);
                    }}
                />
            </Card>
            <Divider/>
            <Button style={styles.button} onPress={addWorkingTime}>
                Сохранить
            </Button>
        </Layout>
    );
});

const themedStyles = StyleService.create({
    container: {
        flex: 1,
    },
    box: {
        flex: 1,
    },
    input: {
        margin: 10,
    },
    button: {
        marginHorizontal: 16,
        marginVertical: 24,
    },
});
