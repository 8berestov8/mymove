import React from 'react';
import ModalErrorPopup from '../../components/modal-error';
import { addSchool } from '../../API/Schools';
import { View } from 'react-native';
import {
  Button,
  Input,
  StyleService,
  Text,
  useStyleSheet,
  Spinner,
} from '@ui-kitten/components';
import { ImageOverlay } from './extra/image-overlay.component';
import { ArrowBackIconOutline } from '../../components/icons';
import { KeyboardAvoidingView } from '../../components/3rd-party';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setSchools } from '../../redux/actions/schoolsActions';

const mapStateToProps = (state) => {
  const { schools, user } = state;
  return { schools, user };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      setSchools,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(({ navigation, user, schools, setSchools }) => {
  const [name, setName] = React.useState('');
  const [contactPhone, setContactPhone] = React.useState('');

  const [statusName, setStatusName] = React.useState('basic');
  const [statusContactPhone, setStatusContactPhone] = React.useState('basic');

  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState('');

  const styles = useStyleSheet(themedStyles);

  const onAddButtonPress = () => {
    let hasErrors = false;
    setStatusName('basic');
    setStatusContactPhone('basic');
    if (name.length === 0) {
      setStatusName('danger');
      hasErrors = true;
    }
    if (contactPhone.length === 0) {
      setStatusContactPhone('danger');
      hasErrors = true;
    }
    if (!hasErrors) {
      setLoading(true);
      addSchool({
        user,
        dataSchool: {
          name: name,
          contact_phone: contactPhone,
        },
      })
        .then((data) => {
          setLoading(false);
          schools.list.unshift(data);
          setSchools(schools.list);
          navigation.navigate('SchoolsList');
        })
        .catch((err) => {
          setLoading(false);
          setError(err);
        });
    }
  };

  return (
    <KeyboardAvoidingView style={styles.container}>
      {loading && (
        <View style={styles.loadingContainer}>
          <Spinner size="small" />
        </View>
      )}
      <ModalErrorPopup error={error} setError={(err) => setError({ err })} />
      <ImageOverlay
        style={styles.headerContainer}
        source={require('./../../assets/image-training-5.jpg')}
      >
        <Button
          style={styles.evaButton}
          appearance="ghost"
          status="control"
          size="large"
          accessoryLeft={ArrowBackIconOutline}
          onPress={() => {
            navigation.goBack();
          }}
        ></Button>
        <View style={styles.signUpContainer}>
          <Text style={styles.signInLabel} category="h4" status="control">
            Управление в одно касание
          </Text>
        </View>
      </ImageOverlay>
      <View style={styles.socialAuthContainer}>
        <Text style={styles.socialAuthHintText}>
          Добавьте свою школу/студию/секцию в MyMove
        </Text>
      </View>
      <View style={[styles.container, styles.formContainer]}>
        <Input
          placeholder="Введите название"
          label="Название школы/студии/секции"
          autoCapitalize="words"
          status={statusName}
          value={name}
          onChangeText={setName}
          size="large"
        />
        <Input
          style={styles.formInput}
          status={statusContactPhone}
          placeholder="Введите телефон"
          label="Контактный телефон школы/студии/секции"
          autoCapitalize="words"
          value={contactPhone}
          onChangeText={setContactPhone}
          size="large"
        />
      </View>
      <Button style={styles.signUpButton} onPress={onAddButtonPress}>
        ДОБАВИТЬ
      </Button>
    </KeyboardAvoidingView>
  );
});

const themedStyles = StyleService.create({
  container: {
    backgroundColor: 'background-basic-color-1',
  },
  headerContainer: {
    minHeight: 216,
    paddingHorizontal: 16,
    paddingTop: 24,
    paddingBottom: 44,
  },
  signUpContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 32,
  },
  socialAuthContainer: {
    marginTop: 24,
  },
  socialAuthHintText: {
    alignSelf: 'center',
    marginBottom: 16,
  },
  formContainer: {
    marginTop: 48,
    paddingHorizontal: 16,
  },
  evaButton: {
    maxWidth: 72,
    paddingHorizontal: 0,
  },
  signInLabel: {
    flex: 1,
  },
  signUpButton: {
    marginVertical: 24,
    marginHorizontal: 16,
  },
  formInput: {
    marginTop: 16,
  },
  termsCheckBox: {
    marginTop: 20,
  },
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 7777,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
