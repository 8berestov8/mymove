import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setBarCode } from '../../redux/actions/barCodeActions';

const mapStateToProps = (state) => {
	const { user } = state;
	return { user };
};

const mapDispatchToProps = (dispatch) =>
	bindActionCreators({ setBarCode }, dispatch);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(({ navigation, setBarCode }) => {
	const [hasPermission, setHasPermission] = useState(null);
	const [scanned, setScanned] = useState(false);

	useEffect(() => {
		(async () => {
			const { status } = await BarCodeScanner.requestPermissionsAsync();
			setHasPermission(status === 'granted');
		})();
	}, []);

	const handleBarCodeScanned = ({ type, data }) => {
		setScanned(true);
		setBarCode(data);
		navigation.goBack();
	};

	if (hasPermission === null) {
		return <Text>Запрос разрешения камеры</Text>;
	}
	if (hasPermission === false) {
		return <Text>Нет доступа к камере</Text>;
	}

	return (
		<View style={styles.container}>
			<BarCodeScanner
				style={styles.barCode}
				onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
			>
				<View style={styles.layerTop} />
				<View style={styles.layerCenter}>
					<View style={styles.layerLeft} />
					<View style={styles.focused} />
					<View style={styles.layerRight} />
				</View>
				<View style={styles.layerBottom} />
			</BarCodeScanner>
		</View>
	);
});

const opacity = 'rgba(0, 0, 0, .6)';
const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
	},
	barCode: {
		...StyleSheet.absoluteFillObject,
	},

	layerTop: {
		flex: 2,
		backgroundColor: opacity,
	},
	layerCenter: {
		flex: 2,
		flexDirection: 'row',
	},
	layerLeft: {
		flex: 1,
		backgroundColor: opacity,
	},
	focused: {
		flex: 10,
	},
	layerRight: {
		flex: 1,
		backgroundColor: opacity,
	},
	layerBottom: {
		flex: 2,
		backgroundColor: opacity,
	},
});
