import React from 'react';
import { View } from 'react-native';
import {
  Button,
  Datepicker,
  Divider,
  Input,
  Layout,
  StyleService,
  useStyleSheet,
  Spinner,
  NativeDateService,
} from '@ui-kitten/components';
import moment from 'moment';
import { KeyboardAvoidingView } from '../../components/3rd-party';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import API from '../../API';
import { setTeachers } from '../../redux/actions/teachersActions';
import ModalErrorPopup from '../../components/modal-error';
import { calendarLang } from '../../i18n/';
import TopNavigation from '../../components/top-navigation';
import { addTeacher, editTeacher } from '../../API/Teachers';

const mapStateToProps = (state) => {
  const { user, teachers } = state;
  return { user, teachers };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      setTeachers,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(({ navigation, route, teachers, user, setTeachers }) => {
  const idSchool = route.params.school_id;
  const idTeacher = route.params.id;
  const type =
    typeof route.params !== 'undefined' &&
    typeof route.params.id !== 'undefined'
      ? 'edit'
      : 'add';
  let teacher = {
    firstname: '',
    lastname: '',
    patronymic: '',
    phone: '',
    birth_date: '',
  };
  if (type === 'edit') {
    teacher = {
      ...teachers.list[idSchool].find((v) => v.id == route.params.id),
    };
    teacher.birth_date = !teacher.birth_date
      ? teacher.birth_date
      : moment(teacher.birth_date).toDate();
  }

  const styles = useStyleSheet(themedStyles);
  let origPhone = teacher.phone;
  if (teacher.phone.length > 0) {
    const matchPhone = teacher.phone.match(
      /^7([0-9]{3})([0-9]{3})([0-9]{2})([0-9]{2})/
    );
    teacher.phone = `+7 (${matchPhone[1]}) ${matchPhone[2]}-${matchPhone[3]}-${matchPhone[4]}`;
  }
  const [visiblePhone, setVisiblePhone] = React.useState(teacher.phone);
  const [phone, setPhone] = React.useState(origPhone);
  const [preventDefault, setPreventDefault] = React.useState(false);
  const [firstname, setFirstname] = React.useState(teacher.firstname);
  const [lastname, setLastname] = React.useState(teacher.lastname);
  const [patronymic, setPatronymic] = React.useState(teacher.patronymic);
  const [birthDate, setBirthDate] = React.useState(teacher.birth_date);

  const [statusFirstname, setStatusFirstname] = React.useState('basic');
  const [statusLastname, setStatusLastname] = React.useState('basic');
  const [statusPatronymic, setStatusPatronymic] = React.useState('basic');
  const [statusVisiblePhone, setStatusVisiblePhone] = React.useState('basic');

  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState('');

  const onSaveButtonPress = () => {
    let hasErrors = false;
    setStatusFirstname('basic');
    setStatusLastname('basic');
    setStatusPatronymic('basic');
    setStatusVisiblePhone('basic');
    if (phone.length === 0) {
      setStatusVisiblePhone('danger');
      hasErrors = true;
    }
    if (firstname.length === 0) {
      setStatusFirstname('danger');
      hasErrors = true;
    }
    if (lastname.length === 0) {
      setStatusLastname('danger');
      hasErrors = true;
    }
    if (patronymic.length === 0) {
      setStatusPatronymic('danger');
      hasErrors = true;
    }
    if (!hasErrors) {
      setLoading(true);
      let formTeacher;
      if (type === 'edit') {
        formTeacher = editTeacher;
      } else {
        formTeacher = addTeacher;
      }
      formTeacher({
        user,
        idSchool,
        idTeacher,
        data: {
          phone,
          firstname,
          lastname,
          patronymic,
          birth_date: birthDate ? moment(birthDate).format('YYYY-MM-DD') : null,
        },
      })
        .then((data) => {
          setLoading(false);
          if (type === 'add') {
            teachers.list[idSchool].unshift(data);
          } else {
            teachers.list[idSchool] = teachers.list[idSchool].map((v) => {
              return v.id == teacher.id ? data : v;
            });
          }
          setTeachers(idSchool, teachers.list[idSchool]);
          navigation.navigate('TeachersList', {
            school_id: idSchool,
          });
        })
        .catch((err) => {
          setLoading(false);
          setError(err);
        });
    }
  };
  const localeDateService = new NativeDateService('ru', {
    format: 'DD.MM.YYYY',
    i18n: calendarLang,
    startDayOfWeek: 1,
  });
  const now = new Date();
  const minBirth = new Date(
    now.getFullYear(),
    now.getMonth(),
    now.getDate() - 3600 * 150
  );
  const maxBirth = new Date(
    now.getFullYear(),
    now.getMonth(),
    now.getDate() + 1
  );
  return (
    <KeyboardAvoidingView style={styles.container}>
      <TopNavigation
        title={type == 'add' ? 'Добавление тренера' : 'Редактирование тренера'}
      />
      {loading && (
        <View style={styles.loadingContainer}>
          <Spinner size="small" />
        </View>
      )}
      <ModalErrorPopup error={error} setError={(err) => setError({ err })} />
      <Layout style={styles.form} level="1">
        <Input
          style={styles.input}
          label="Телефонный номер"
          caption=""
          status={statusVisiblePhone}
          value={visiblePhone}
          keyboardType="numeric"
          onKeyPress={(e) => {
            if (e.nativeEvent.key == 'Backspace') {
              if (visiblePhone !== '+7 (') {
                setVisiblePhone(
                  visiblePhone.slice(
                    0,
                    visiblePhone.length -
                      (/(\-| |\))$/.test(visiblePhone) ? 2 : 1)
                  )
                );
                setPreventDefault(true);
              }
            }
          }}
          onChangeText={(val) => {
            setError('');
            if (preventDefault) {
              setPreventDefault(false);
              return;
            }
            const regPhone = /^\+7 \(([0-9]{3})\) ([0-9]{3})\-([0-9]{2})\-([0-9]{2})$/;
            if (regPhone.test(val)) {
              const matchPhone = val.match(regPhone);
              setVisiblePhone(val);
              setPhone(
                `7${matchPhone[1]}${matchPhone[2]}${matchPhone[3]}${matchPhone[4]}`
              );
            } else if (/^\+7 \([0-9]{3}$/.test(val)) {
              setVisiblePhone(val + ') ');
            } else if (/^\+7 \([0-9]{3}\) [0-9]{3}$/.test(val)) {
              setVisiblePhone(val + '-');
            } else if (/^\+7 \([0-9]{3}\) [0-9]{3}\-[0-9]{2}$/.test(val)) {
              setVisiblePhone(val + '-');
            } else if (
              /^\+7 \(([0-9]{1})?([0-9]{1})?([0-9]{1})?\)?( )?([0-9]{1})?([0-9]{1})?([0-9]{1})?(\-)?([0-9]{1})?([0-9]{1})?(\-)?([0-9]{1})?([0-9]{1})?$/.test(
                val
              )
            ) {
              setVisiblePhone(val);
            }
          }}
          onFocus={() => {
            if (visiblePhone == '') setVisiblePhone('+7 (');
          }}
        />
        <Input
          style={styles.input}
          label="Фамилия"
          value={lastname}
          onChangeText={setLastname}
          status={statusLastname}
        />
        <Input
          style={styles.input}
          label="Имя"
          value={firstname}
          onChangeText={setFirstname}
          status={statusFirstname}
        />
        <Input
          style={styles.input}
          label="Отчество"
          value={patronymic}
          onChangeText={setPatronymic}
          status={statusPatronymic}
        />
        <Datepicker
          style={styles.input}
          label="Дата рождения"
          placeholder=""
          date={birthDate}
          onSelect={setBirthDate}
          min={minBirth}
          max={maxBirth}
          dateService={localeDateService}
        />
      </Layout>
      <Divider />
      <Button style={styles.addButton} onPress={onSaveButtonPress}>
        СОХРАНИТЬ
      </Button>
    </KeyboardAvoidingView>
  );
});

const themedStyles = StyleService.create({
  container: {
    flex: 1,
    backgroundColor: 'background-basic-color-2',
  },
  form: {
    flex: 1,
    paddingHorizontal: 4,
    paddingVertical: 24,
  },
  input: {
    marginHorizontal: 12,
    marginVertical: 8,
  },
  middleContainer: {
    flexDirection: 'row',
  },
  middleInput: {
    width: 128,
  },
  addButton: {
    marginHorizontal: 16,
    marginVertical: 24,
  },
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 7777,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
});
