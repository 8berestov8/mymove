import React from 'react';
import { StyleSheet, View, RefreshControl } from 'react-native';
import { Layout, List } from '@ui-kitten/components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import StudentsListComponent from './extra/students-list.component';
import { setStudents } from '../../redux/actions/studentsActions';
import TopNavigation from '../../components/top-navigation';
import { getStudents } from '../../API/Students';
import Loading from '../../components/loading';

const mapStateToProps = (state) => {
	const { user, students } = state;
	return { user, students };
};

const mapDispatchToProps = (dispatch) =>
	bindActionCreators(
		{
			setStudents,
		},
		dispatch
	);

class studentsList extends React.Component {
	constructor(props) {
		super(props);
		const idSchool = props.route.params.school_id;
		this.state = {
			refreshing: false,
			error: '',
			loading: true,
		};
		const { user, setStudents } = props;
		this.loadData = () => {
			return new Promise((resolve, reject) => {
				getStudents({ user, idSchool })
					.then((data) => {
						setStudents(idSchool, data);
						resolve(data);
					})
					.catch((e) => {
						reject(e);
					});
			});
		};
		this.loadData()
			.then((res) => {
				this.setState({ loading: false });
			})
			.catch((err) => {
				this.setState({ loading: false });
				this.setState({ error: err });
			});
	}

	render() {
		const props = this.props;
		const idSchool = props.route.params.school_id;
		const { navigation, students } = props;
		const onRefresh = () => {
			this.setState({ refreshing: true });
			this.loadData()
				.then(() => {
					this.setState({ refreshing: false });
				})
				.catch((err) => {
					this.setState({ refreshing: false });
					this.setState({ error: err });
				});
		};
		return (
			<Layout style={styles.container} level="2">
				<TopNavigation
					onPressPlus={() => {
						navigation.navigate('StudentForm', {
							school_id: idSchool,
						});
					}}
					title="Ученики"
				/>
				<View style={styles.body}>
					{students.list == 0 ? (
						<Loading />
					) : (
						<StudentsListComponent
							style={styles.list}
							loading={this.state.loading}
							refreshControl={
								<RefreshControl
									refreshing={this.state.refreshing}
									onRefresh={onRefresh}
								/>
							}
							data={students.list[idSchool] ? students.list[idSchool] : []}
							school_id={idSchool}
							{...props}
						/>
					)}
				</View>
			</Layout>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(studentsList);

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	list: {
		flex: 1,
	},
	body: {
		flex: 1,
		paddingHorizontal: 16,
		paddingTop: 16,
	},
	profileContainer: {
		flexDirection: 'row',
	},
	profileAvatar: {
		marginHorizontal: 8,
	},
	profileDetailsContainer: {
		flex: 1,
		marginHorizontal: 8,
	},
	rateBar: {
		marginTop: 24,
	},
	followButton: {},
	profileSocialsContainer: {
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		marginTop: 24,
	},
	profileSectionsDivider: {
		width: 1,
		height: '100%',
		marginHorizontal: 8,
	},
	profileDescription: {
		marginTop: 24,
		marginBottom: 8,
	},
	profileParametersSection: {
		flexDirection: 'row',
		marginVertical: 16,
		marginHorizontal: 8,
	},
	profileParameter: {
		flex: 1,
		marginHorizontal: 8,
	},
});
