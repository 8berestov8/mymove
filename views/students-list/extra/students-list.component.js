import React from 'react';
import { StyleSheet, View } from 'react-native';
import {
  Button,
  Card,
  List,
  Text,
  OverflowMenu,
  MenuItem,
} from '@ui-kitten/components';
import { ProfileSocial } from './profile-social.component';
import { MoreHorizontalIcon } from '../../../components/icons';
import API from '../../../API';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setStudents } from '../../../redux/actions/studentsActions';
import ModalDeletePopup from '../../../components/modal-delete';
import ModalConfirmPopup from '../../../components/modal-confirm';
import TextNotFound from '../../../components/text-not-found';
import { deleteStudent } from '../../../API/Students';

const mapStateToProps = (state) => {
  const { user, schools, students } = state;
  return { user, schools, students };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      setStudents,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)((props) => {
  const { user, students, setStudents, navigation, loading } = props;
  const idSchool = props.school_id;
  const [idStudent, setIdstudent] = React.useState(0);
  const [visibleModalDelete, setVisibleModalDelete] = React.useState(false);
  const [visibleModalHide, setVisibleModalHide] = React.useState(false);
  const ModalDelete = () => (
    <ModalDeletePopup
      visible={visibleModalDelete}
      setVisible={(v) => setVisibleModalDelete(v)}
      confirmDelete={() => {
        deleteStudent({ user, idSchool, idStudent }).then((data) => {
          students.list[idSchool] = students.list[idSchool].filter((v) => {
            return v.id != idStudent;
          });
          setStudents(idSchool, students.list[idSchool]);
          setVisibleModalDelete(false);
        });
      }}
    />
  );
  const ModalHide = () => (
    <ModalConfirmPopup
      visible={visibleModalHide}
      setVisible={(v) => setVisibleModalHide(v)}
      title="Скрыть у себя ученика?"
      subtitle="Ученик будет убран из вашего списка."
      confirm={() => {
        deleteStudent({ user, idSchool, idStudent }).then((data) => {
          students.list[idSchool] = students.list[idSchool].filter((v) => {
            return v.id != idstudent;
          });
          setStudents(idSchool, students.list[idSchool]);
          setVisibleModalHide(false);
        });
      }}
    />
  );
  const renderstudentHeader = (student) => {
    const [visible, setVisible] = React.useState(false);

    const renderToggleButton = () => (
      <Button
        style={styles.iconButton}
        appearance="ghost"
        status="basic"
        accessoryLeft={MoreHorizontalIcon}
        onPress={() => setVisible(true)}
      />
    );

    const onItemSelect = (index) => {
      setVisible(false);
    };
    return (
      <View style={styles.studentHeader}>
        <View style={styles.studentAuthorContainer}>
          <Text category="s1">
            {student.lastname} {student.firstname} {student.patronymic}
          </Text>
          <Text appearance="hint" category="c1">
            {student.phone}
          </Text>
        </View>
        <OverflowMenu
          anchor={renderToggleButton}
          backdropStyle={styles.backdrop}
          visible={visible}
          onSelect={onItemSelect}
          onBackdropPress={() => setVisible(false)}
        >
          <MenuItem
            title="Редактировать"
            onPress={() => {
              setVisible(false);
              navigation.navigate('StudentForm', {
                school_id: idSchool,
                id: student.id,
              });
            }}
          />
          {user.type == 'owner' && (
            <MenuItem
              title="Удалить"
              onPress={() => {
                setIdstudent(student.id);
                setVisible(false);
                setVisibleModalDelete(true);
              }}
            />
          )}
          {user.type == 'teacher' && (
            <MenuItem
              title="Скрыть"
              onPress={() => {
                setIdstudent(student.id);
                setVisible(false);
                setVisibleModalHide(true);
              }}
            />
          )}
        </OverflowMenu>
      </View>
    );
  };

  const renderItem = (info) => (
    <Card
      style={styles.studentItem}
      header={() => renderstudentHeader(info.item)}
    >
      <View style={styles.profileSocialsContainer}>
        <ProfileSocial
          hint="Занятий"
          value={
            info.item.used_lessons +
            ' из ' +
            (info.item.infinity_lessons ? '~' : info.item.count_lessons)
          }
          onPress={() => {
            navigation.navigate('LessonsPlusList', {
              school_id: idSchool,
              student_id: info.item.id,
            });
          }}
        />
      </View>
    </Card>
  );
  return (
    <>
      <List
        {...props}
        renderItem={renderItem}
        ListEmptyComponent={
          !loading && <TextNotFound text={'Ученики не добавлены'} />
        }
      />
      <ModalDelete />
      <ModalHide />
    </>
  );
});

const styles = StyleSheet.create({
  studentItem: {
    marginVertical: 4,
  },
  studentHeader: {
    flexDirection: 'row',
    padding: 16,
  },
  studentAuthorContainer: {
    flex: 1,
  },
  studentReactionsContainer: {
    flexDirection: 'row',
    marginTop: 8,
    marginHorizontal: -8,
    marginVertical: -8,
  },
  iconButton: {
    paddingHorizontal: 0,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  profileSocialsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  profileParametersSection: {
    flexDirection: 'row',
    marginVertical: 16,
    marginHorizontal: 8,
  },
  profileParameter: {
    flex: 1,
    marginHorizontal: 8,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  buttons: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 8,
  },
  textConfirm: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  inputDeleted: {
    marginVertical: 8,
  },
});
