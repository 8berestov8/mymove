import React from 'react';
import { Alert, View } from 'react-native';
import {
  Datepicker,
  Input,
  Layout,
  useStyleSheet,
  Spinner,
  NativeDateService,
  CheckBox,
  StyleService,
  Text,
  Button,
} from '@ui-kitten/components';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getTeachers } from '../../API/Teachers';
import { addStudent, editStudent } from '../../API/Students';
import { setStudents } from '../../redux/actions/studentsActions';
import { setTeachers } from '../../redux/actions/teachersActions';
import { setBarCode } from '../../redux/actions/barCodeActions';
import { KeyboardAvoidingView } from '../../components/3rd-party';
import ModalErrorPopup from '../../components/modal-error';
import { calendarLang } from '../../i18n/';
import TopNavigation from '../../components/top-navigation';

const mapStateToProps = (state) => {
  const { user, students, teachers, barCode } = state;
  return { user, students, teachers, barCode };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      setStudents,
      setTeachers,
      setBarCode,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  ({
    navigation,
    route,
    students,
    teachers,
    user,
    setStudents,
    setTeachers,
    setBarCode,
    barCode,
  }) => {
    const idStudent = route.params.id;
    const idSchool = route.params.school_id;
    React.useEffect(() => {
      if (
        user.type == 'owner' &&
        typeof teachers.list[idSchool] === 'undefined'
      ) {
        getTeachers({ user, idSchool })
          .then((data) => {
            setTeachers(idSchool, data);
          })
          .catch((e) => {
            setError(e);
          });
      }
    }, []);

    const type =
      typeof route.params !== 'undefined' &&
      typeof route.params.id !== 'undefined'
        ? 'edit'
        : 'add';
    let student = {
      firstname: '',
      lastname: '',
      patronymic: '',
      phone: '',
      birth_date: '',
      teachers_xref: [],
    };
    if (type === 'edit') {
      student = {
        ...students.list[idSchool].find((v) => v.id == route.params.id),
      };
      student.birth_date = !student.birth_date
        ? student.birth_date
        : moment(student.birth_date).toDate();
    }

    const styles = useStyleSheet(themedStyles);
    let origPhone = student.phone;
    if (student.phone.length > 0) {
      const matchPhone = student.phone.match(
        /^7([0-9]{3})([0-9]{3})([0-9]{2})([0-9]{2})/
      );
      student.phone = `+7 (${matchPhone[1]}) ${matchPhone[2]}-${matchPhone[3]}-${matchPhone[4]}`;
    }
    const [visiblePhone, setVisiblePhone] = React.useState(student.phone);
    const [phone, setPhone] = React.useState(origPhone);
    const [preventDefault, setPreventDefault] = React.useState(false);
    const [firstname, setFirstname] = React.useState(student.firstname);
    const [lastname, setLastname] = React.useState(student.lastname);
    const [patronymic, setPatronymic] = React.useState(student.patronymic);
    const [birthDate, setBirthDate] = React.useState(student.birth_date);
    const [barCodeStudent, setBarCodeStudent] = React.useState(
      student.bar_code
    );

    const [statusFirstname, setStatusFirstname] = React.useState('basic');
    const [statusLastname, setStatusLastname] = React.useState('basic');
    const [statusPatronymic, setStatusPatronymic] = React.useState('basic');
    const [statusVisiblePhone, setStatusVisiblePhone] = React.useState('basic');

    const [loading, setLoading] = React.useState(false);
    const [error, setError] = React.useState('');
    const [checkedTeachers, setCheckedTeachers] = React.useState(
      student.teachers_xref
    );
    React.useEffect(() => {
      if (barCode.value != null) {
        setBarCode(null);
        setBarCodeStudent(barCode.value);
      }
    }, []);

    const onSaveButtonPress = () => {
      let hasErrors = false;
      setStatusFirstname('basic');
      setStatusLastname('basic');
      setStatusPatronymic('basic');
      setStatusVisiblePhone('basic');
      if (phone.length === 0) {
        setStatusVisiblePhone('danger');
        hasErrors = true;
      }
      if (firstname.length === 0) {
        setStatusFirstname('danger');
        hasErrors = true;
      }
      if (lastname.length === 0) {
        setStatusLastname('danger');
        hasErrors = true;
      }
      if (patronymic.length === 0) {
        setStatusPatronymic('danger');
        hasErrors = true;
      }
      if (!hasErrors) {
        setLoading(true);
        let formStudent;
        if (type === 'edit') {
          formStudent = editStudent;
        } else {
          formStudent = addStudent;
        }
        formStudent({
          user,
          idSchool,
          idStudent,
          data: {
            phone,
            firstname,
            lastname,
            patronymic,
            birth_date: birthDate
              ? moment(birthDate).format('YYYY-MM-DD')
              : null,
            teachers_xref: checkedTeachers,
          },
        })
          .then((data) => {
            setLoading(false);
            if (type === 'add') {
              students.list[idSchool].unshift(data);
            } else {
              students.list[idSchool] = students.list[idSchool].map((v) => {
                return v.id == student.id ? data : v;
              });
            }
            setStudents(idSchool, students.list[idSchool]);
            navigation.navigate('StudentsList', {
              school_id: idSchool,
            });
          })
          .catch((e) => {
            setLoading(false);
            setError(e);
          });
      }
    };
    const localeDateService = new NativeDateService('ru', {
      format: 'DD.MM.YYYY',
      i18n: calendarLang,
      startDayOfWeek: 1,
    });
    const now = new Date();
    const minBirth = new Date(
      now.getFullYear(),
      now.getMonth(),
      now.getDate() - 3600 * 150
    );
    const maxBirth = new Date(
      now.getFullYear(),
      now.getMonth(),
      now.getDate() + 1
    );

    const BarCodeButton = (props) => (
      <Button
        appearance="ghost"
        onPress={() => navigation.navigate('ScanCode')}
      >
        Сканировать
      </Button>
    );

    return (
      <Layout style={styles.container} level="2">
        <TopNavigation
          title={
            type == 'add' ? 'Добавление ученика' : 'Редактирование ученика'
          }
        />
        <KeyboardAvoidingView style={styles.container}>
          {loading && (
            <View style={styles.loadingContainer}>
              <Spinner size="small" />
            </View>
          )}
          <ModalErrorPopup
            error={error}
            setError={(err) => setError({ err })}
          />
          <Layout style={styles.form} level="1">
            <Input
              style={styles.input}
              label="Телефонный номер"
              caption=""
              status={statusVisiblePhone}
              value={visiblePhone}
              keyboardType="numeric"
              size="large"
              onKeyPress={(e) => {
                if (e.nativeEvent.key == 'Backspace') {
                  if (visiblePhone !== '+7 (') {
                    setVisiblePhone(
                      visiblePhone.slice(
                        0,
                        visiblePhone.length -
                          (/(\-| |\))$/.test(visiblePhone) ? 2 : 1)
                      )
                    );
                    setPreventDefault(true);
                  }
                }
              }}
              onChangeText={(val) => {
                setError('');
                if (preventDefault) {
                  setPreventDefault(false);
                  return;
                }
                const regPhone = /^\+7 \(([0-9]{3})\) ([0-9]{3})\-([0-9]{2})\-([0-9]{2})$/;
                if (regPhone.test(val)) {
                  const matchPhone = val.match(regPhone);
                  setVisiblePhone(val);
                  setPhone(
                    `7${matchPhone[1]}${matchPhone[2]}${matchPhone[3]}${matchPhone[4]}`
                  );
                } else if (/^\+7 \([0-9]{3}$/.test(val)) {
                  setVisiblePhone(val + ') ');
                } else if (/^\+7 \([0-9]{3}\) [0-9]{3}$/.test(val)) {
                  setVisiblePhone(val + '-');
                } else if (/^\+7 \([0-9]{3}\) [0-9]{3}\-[0-9]{2}$/.test(val)) {
                  setVisiblePhone(val + '-');
                } else if (
                  /^\+7 \(([0-9]{1})?([0-9]{1})?([0-9]{1})?\)?( )?([0-9]{1})?([0-9]{1})?([0-9]{1})?(\-)?([0-9]{1})?([0-9]{1})?(\-)?([0-9]{1})?([0-9]{1})?$/.test(
                    val
                  )
                ) {
                  setVisiblePhone(val);
                }
              }}
              onFocus={() => {
                if (visiblePhone == '') setVisiblePhone('+7 (');
              }}
            />
            <Input
              style={styles.input}
              label="Фамилия"
              value={lastname}
              onChangeText={setLastname}
              status={statusLastname}
              size="large"
            />
            <Input
              style={styles.input}
              label="Имя"
              value={firstname}
              onChangeText={setFirstname}
              status={statusFirstname}
              size="large"
            />
            <Input
              style={styles.input}
              label="Отчество"
              value={patronymic}
              onChangeText={setPatronymic}
              status={statusPatronymic}
              size="large"
            />

            <Input
              style={styles.input}
              label="Штрихкод"
              accessoryRight={BarCodeButton}
              value={barCodeStudent}
            />

            <Datepicker
              style={styles.input}
              label="Дата рождения"
              placeholder=""
              date={birthDate}
              onSelect={setBirthDate}
              min={minBirth}
              max={maxBirth}
              dateService={localeDateService}
              size="large"
            />
            {user.type == 'owner' && (
              <Text style={styles.title}>Преподаватели</Text>
            )}
            {user.type == 'owner' &&
              (typeof teachers.list[idSchool] != 'undefined'
                ? teachers.list[idSchool]
                : []
              ).map((teacher) => (
                <CheckBox
                  key={`checkbox_teacher_${teacher.id}`}
                  style={styles.input}
                  checked={checkedTeachers.some(
                    (item) => item.teacher_id == teacher.id
                  )}
                  onChange={(nextChecked) => {
                    let _checkedTeachers = [...checkedTeachers];
                    if (nextChecked) {
                      _checkedTeachers.push({ teacher_id: teacher.id });
                    } else {
                      _checkedTeachers = _checkedTeachers.filter(
                        (item) => item.teacher_id != teacher.id
                      );
                    }
                    setCheckedTeachers(_checkedTeachers);
                  }}
                >
                  {`${teacher.lastname} ${teacher.firstname} ${teacher.patronymic}`}
                </CheckBox>
              ))}
          </Layout>
          <Button style={styles.addButton} onPress={onSaveButtonPress}>
            СОХРАНИТЬ
          </Button>
        </KeyboardAvoidingView>
      </Layout>
    );
  }
);

const themedStyles = StyleService.create({
  container: {
    flex: 1,
    backgroundColor: 'background-basic-color-2',
  },
  form: {
    flex: 1,
    paddingHorizontal: 4,
    paddingVertical: 24,
  },
  input: {
    marginHorizontal: 12,
    marginVertical: 8,
  },
  middleContainer: {
    flexDirection: 'row',
  },
  middleInput: {
    width: 128,
  },
  addButton: {
    marginHorizontal: 16,
    marginBotom: 16,
  },
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 7777,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginHorizontal: 12,
    marginVertical: 8,
  },
});
