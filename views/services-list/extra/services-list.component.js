import React from 'react';
import { StyleSheet, View } from 'react-native';
import {
  Button,
  Card,
  List,
  Text,
  OverflowMenu,
  MenuItem,
} from '@ui-kitten/components';
import { MoreHorizontalIcon } from '../../../components/icons';
import API from '../../../API';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setServices } from '../../../redux/actions/servicesActions';
import ModalDeletePopup from '../../../components/modal-delete';
import TextNotFound from '../../../components/text-not-found';
import { deleteService } from '../../../API/Services';

const mapStateToProps = (state) => {
  const { user, schools, services } = state;
  return { user, schools, services };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      setServices,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)((props) => {
  const { user, services, setServices, loading } = props;
  const idSchool = props.school_id;
  const [idService, setIdservice] = React.useState(0);
  const [visibleModalDelete, setVisibleModalDelete] = React.useState(false);
  const ModalDelete = () => (
    <ModalDeletePopup
      visible={visibleModalDelete}
      setVisible={(v) => setVisibleModalDelete(v)}
      confirmDelete={() => {
        deleteService({ user, idSchool, idService }).then((data) => {
          services.list[idSchool] = services.list[idSchool].filter((v) => {
            return v.id != idService;
          });
          setServices(idSchool, services.list[idSchool]);
          setVisibleModalDelete(false);
        });
      }}
    />
  );
  const RenderserviceContent = ({ service }) => {
    const [visible, setVisible] = React.useState(false);

    const renderToggleButton = () => (
      <Button
        style={styles.iconButton}
        appearance="ghost"
        status="basic"
        accessoryLeft={MoreHorizontalIcon}
        onPress={() => setVisible(true)}
      />
    );

    const onItemSelect = (index) => {
      setVisible(false);
    };
    return (
      <View style={styles.serviceHeader}>
        <View style={styles.serviceAuthorContainer}>
          <Text category="s1">{service.name}</Text>
          <Text appearance="hint" category="c1">
            {service.infinity_lessons ? '~' : service.count_lessons} занятий
          </Text>
        </View>

        <OverflowMenu
          anchor={renderToggleButton}
          backdropStyle={styles.backdrop}
          visible={visible}
          onSelect={onItemSelect}
          onBackdropPress={() => setVisible(false)}
        >
          <MenuItem
            title="Редактировать"
            onPress={() => {
              setVisible(false);
              props.navigation.navigate('ServiceForm', {
                school_id: idSchool,
                id: service.id,
              });
            }}
          />
          <MenuItem
            title="Удалить"
            onPress={() => {
              setIdservice(service.id);
              setVisible(false);
              setVisibleModalDelete(true);
            }}
          />
        </OverflowMenu>
      </View>
    );
  };

  const renderItem = (info) => (
    <Card style={styles.serviceItem}>
      <RenderserviceContent service={info.item} />
    </Card>
  );
  return (
    <>
      <List
        {...props}
        renderItem={renderItem}
        ListEmptyComponent={
          !loading && <TextNotFound text={'Услуги не добавлены'} />
        }
      />
      <ModalDelete />
    </>
  );
});

const styles = StyleSheet.create({
  serviceItem: {
    marginVertical: 4,
  },
  serviceHeader: {
    flexDirection: 'row',
  },
  serviceAuthorContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  serviceReactionsContainer: {
    flexDirection: 'row',
    marginTop: 8,
    marginHorizontal: -8,
    marginVertical: -8,
  },
  iconButton: {
    paddingHorizontal: 0,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  profileSocialsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  profileParametersSection: {
    flexDirection: 'row',
    marginVertical: 16,
    marginHorizontal: 8,
  },
  profileParameter: {
    flex: 1,
    marginHorizontal: 8,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  buttons: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 8,
  },
  textConfirm: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  inputDeleted: {
    marginVertical: 8,
  },
});
