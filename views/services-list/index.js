import React from 'react';
import ModalErrorPopup from '../../components/modal-error';
import { getServices } from '../../API/Services';
import { StyleSheet, View, RefreshControl } from 'react-native';
import { Layout } from '@ui-kitten/components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ServicesListComponent from './extra/services-list.component';
import { setServices } from '../../redux/actions/servicesActions';
import TopNavigation from '../../components/top-navigation';
import Loading from '../../components/loading';

const mapStateToProps = (state) => {
	const { user, services } = state;
	return { user, services };
};

const mapDispatchToProps = (dispatch) =>
	bindActionCreators(
		{
			setServices,
		},
		dispatch
	);

class servicesList extends React.Component {
	constructor(props) {
		super(props);
		const idSchool = props.route.params.school_id;
		const { user, setServices } = props;
		this.state = {
			refreshing: false,
			error: '',
			loading: true,
		};
		this.loadData = () => {
			return new Promise((resolve, reject) => {
				getServices({ idSchool, user })
					.then((data) => {
						setServices(idSchool, data);
						resolve(data);
					})
					.catch((err) => {
						reject(err);
					});
			});
		};
		this.loadData()
			.then((res) => {
				this.setState({ loading: false });
			})
			.catch((err) => {
				this.setState({ loading: false });
				this.setState({ error: err });
			});
	}

	render() {
		const props = this.props;
		const idSchool = props.route.params.school_id;
		const { navigation, user, services } = props;

		const onRefresh = () => {
			this.setState({ refreshing: true });
			this.loadData()
				.then(() => {
					this.setState({ refreshing: false });
				})
				.catch((err) => {
					this.setState({ refreshing: false });
					this.setState({ error: err });
				});
		};

		return (
			<Layout style={styles.container} level="2">
				<TopNavigation
					onPressPlus={() => {
						navigation.navigate('ServiceForm', {
							school_id: idSchool,
						});
					}}
					title="Услуги"
				/>
				<ModalErrorPopup
					error={this.state.error}
					setError={(err) => this.setState({ error: err })}
				/>
				<View style={styles.body}>
					{services.list == 0 ? (
						<Loading />
					) : (
						<ServicesListComponent
							style={styles.list}
							loading={this.state.loading}
							refreshControl={
								<RefreshControl
									refreshing={this.state.refreshing}
									onRefresh={onRefresh}
								/>
							}
							data={services.list[idSchool] ? services.list[idSchool] : []}
							school_id={idSchool}
							{...props}
						/>
					)}
				</View>
			</Layout>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(servicesList);

const styles = StyleSheet.create({
	list: {
		flex: 1,
	},
	body: {
		flex: 1,
		paddingHorizontal: 16,
		paddingTop: 16,
	},
	container: {
		flex: 1,
	},
	backButton: {
		maxWidth: 72,
		paddingHorizontal: 1,
	},
});
