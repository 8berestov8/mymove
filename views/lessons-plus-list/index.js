import React from 'react';
import { StyleSheet, RefreshControl, View } from 'react-native';
import { Button, Layout } from '@ui-kitten/components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import LessonsPlusListComponent from './extra/lessons-plus-list.component';
import { setLessonsPlus } from '../../redux/actions/lessonsPlusActions';
import ModalErrorPopup from '../../components/modal-error';
import TopNavigation from '../../components/top-navigation';
import { getLessonPlus } from '../../API/LessonPlus';

const mapStateToProps = (state) => {
  const { user, lessonsPlus } = state;
  return { user, lessonsPlus };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      setLessonsPlus,
    },
    dispatch
  );

class lessonsList extends React.Component {
  constructor(props) {
    const idSchool = props.route.params.school_id;
    const idStudent = props.route.params.student_id;
    super(props);
    this.state = {
      refreshing: false,
      error: '',
      loading: true,
    };
    const { user, setLessonsPlus } = props;
    this.loadData = () => {
      return new Promise((resolve, reject) => {
        getLessonPlus({ user, idSchool, idStudent })
          .then((data) => {
            resolve(data);
            setLessonsPlus(idStudent, data);
          })
          .catch((e) => {
            reject(e);
          });
      });
    };

    this.loadData()
      .then(() => {
        this.setState({ loading: false });
      })
      .catch((err) => {
        this.setState({ loading: false });
        this.setState({ error: err });
      });
  }
  render() {
    const props = this.props;
    const idSchool = props.route.params.school_id;
    const idStudent = props.route.params.student_id;
    const { navigation, user, lessonsPlus } = props;

    const onRefresh = () => {
      this.setState({ refreshing: true });
      this.loadData()
        .then(() => {
          this.setState({ refreshing: false });
        })
        .catch((err) => {
          this.setState({ refreshing: false });
          this.setState({ error: err });
        });
    };
    return (
      <Layout style={styles.container} level="2">
        <TopNavigation title="Занятия" />
        <ModalErrorPopup
          error={this.state.error}
          setError={(err) => this.setState({ error: err })}
        />
        <View style={styles.header}>
          {user.type !== 'client' && (
            <View style={styles.headerButtons}>
              <Button
                style={styles.followButton}
                appearance="outline"
                status="primary"
                onPress={() => {
                  navigation.navigate('LessonMinusForm', {
                    student_id: idStudent,
                    school_id: idSchool,
                  });
                }}
              >
                Отметить занятия
              </Button>
              <Button
                style={styles.followButton}
                status="warning"
                appearance="outline"
                onPress={() => {
                  navigation.navigate('LessonPlusForm', {
                    student_id: idStudent,
                    school_id: idSchool,
                  });
                }}
              >
                Добавить занятия
              </Button>
            </View>
          )}
        </View>
        <View style={styles.body}>
          <LessonsPlusListComponent
            style={styles.list}
            data={
              lessonsPlus.list[idStudent] ? lessonsPlus.list[idStudent] : []
            }
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={onRefresh}
              />
            }
            school_id={idSchool}
            student_id={idStudent}
            loading={this.state.loading}
            {...props}
          />
        </View>
      </Layout>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(lessonsList);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    flex: 1,
  },
  header: {
    padding: 16,
  },
  headerButtons: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  body: {
    flex: 1,
    paddingHorizontal: 16,
  },
  followButton: {
    margin: 2,
  },
  button: {},
});
