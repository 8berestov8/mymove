import React from 'react';
import { StyleSheet, View, ViewProps, TouchableOpacity } from 'react-native';
import { Text } from '@ui-kitten/components';

export interface ProfileSocialProps extends ViewProps {
  hint: string;
  value: string;
}

export const ProfileSocial = (props): React.ReactElement => {

  const { style, hint, value, onPress, ...viewProps } = props;

  return (
    <TouchableOpacity 
      {...viewProps}
      onPress={onPress}
      style={[styles.container, style]}>
      <Text category='s2'>
        {value}
      </Text>
      <Text
        appearance='hint'
        category='c2'>
        {props.hint}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
});
