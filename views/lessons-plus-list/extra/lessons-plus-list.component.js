import React from 'react';
import { StyleSheet, View } from 'react-native';
import {
  Button,
  Card,
  List,
  Text,
  OverflowMenu,
  MenuItem,
} from '@ui-kitten/components';
import { ProfileSocial } from './profile-social.component';
import { MoreHorizontalIcon } from '../../../components/icons';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setLessonsPlus } from '../../../redux/actions/lessonsPlusActions';
import ModalDeletePopup from '../../../components/modal-delete';
import moment from 'moment';
import 'moment/locale/ru';
import TextNotFound from '../../../components/text-not-found';
import { deleteLessonPlus } from '../../../API/LessonPlus';

moment.locale('ru');

const mapStateToProps = (state) => {
  const { user, schools, lessonsPlus, lessonsMinus } = state;
  return { user, schools, lessonsPlus, lessonsMinus };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      setLessonsPlus,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)((props) => {
  const { user, lessonsPlus, setLessonsPlus, lessonsMinus, loading } = props;
  const idSchool = props.school_id;
  const idStudent = props.student_id;
  const [idLessonPlus, setidLessonPlus] = React.useState(0);
  const [visibleModalDelete, setVisibleModalDelete] = React.useState(false);
  const ModalDelete = () => (
    <ModalDeletePopup
      visible={visibleModalDelete}
      setVisible={(v) => setVisibleModalDelete(v)}
      confirmDelete={() => {
        deleteLessonPlus({ user, idSchool, idLessonPlus })
          .then((data) => {
            lessonsPlus.list[idStudent] = lessonsPlus.list[idStudent].filter(
              (v) => {
                return v.id != idLessonPlus;
              }
            );
            setLessonsPlus(idStudent, lessonsPlus.list[idStudent]);
            setVisibleModalDelete(false);
          })
          .catch((e) => {});
      }}
    />
  );
  const renderlessonPlusHeader = (lessonPlus) => {
    const [visible, setVisible] = React.useState(false);

    const renderToggleButton = () => (
      <Button
        style={styles.iconButton}
        appearance="ghost"
        status="basic"
        accessoryLeft={MoreHorizontalIcon}
        onPress={() => setVisible(true)}
      />
    );

    const onItemSelect = (index) => {
      setVisible(false);
    };
    return (
      <View style={styles.lessonPlusHeader}>
        <View style={styles.lessonPlusAuthorContainer}>
          <Text category="s1">
            С {moment(lessonPlus.date_start).format('DD MMM')} по{' '}
            {moment(lessonPlus.date_end).format('DD MMM')}
          </Text>
          <Text appearance="hint" category="c1">
            {lessonPlus.is_validity ? (
              <Text style={styles.activeValidity}>действительный</Text>
            ) : (
              <Text style={styles.noActiveValidity}>неактивный</Text>
            )}
          </Text>
        </View>
        {user.type !== 'client' && (
          <OverflowMenu
            anchor={renderToggleButton}
            backdropStyle={styles.backdrop}
            visible={visible}
            onSelect={onItemSelect}
            onBackdropPress={() => setVisible(false)}
          >
            <MenuItem
              title="Редактировать"
              onPress={() => {
                setVisible(false);
                props.navigation.navigate('LessonPlusForm', {
                  school_id: idSchool,
                  student_id: idStudent,
                  id: lessonPlus.id,
                });
              }}
            />
            <MenuItem
              title="Удалить"
              onPress={() => {
                setidLessonPlus(lessonPlus.id);
                setVisible(false);
                setVisibleModalDelete(true);
              }}
            />
          </OverflowMenu>
        )}
      </View>
    );
  };

  const renderItem = (info) => (
    <Card
      style={styles.lessonPlusItem}
      header={() => renderlessonPlusHeader(info.item)}
    >
      <View style={styles.profileSocialsContainer}>
        <ProfileSocial
          hint="Занятий проведено"
          value={
            (typeof lessonsMinus.list[info.item.id] != 'undefined'
              ? lessonsMinus.list[info.item.id].reduce(
                  (n, { count_lessons }) => n + count_lessons,
                  0
                )
              : info.item.used_lessons) +
            ' из ' +
            (info.item.infinity_lessons ? '~' : info.item.count_lessons)
          }
          onPress={() => {
            props.navigation.navigate('LessonsMinusList', {
              school_id: idSchool,
              lesson_plus_id: info.item.id,
            });
          }}
        />
      </View>
    </Card>
  );
  return (
    <>
      <List
        {...props}
        renderItem={renderItem}
        ListEmptyComponent={
          !loading && <TextNotFound text={'Занятия не добавлены'} />
        }
      />
      <ModalDelete />
    </>
  );
});

const styles = StyleSheet.create({
  activeValidity: {
    fontSize: 14,
    color: 'green',
    fontWeight: 'bold',
  },
  noActiveValidity: {
    color: 'red',
    fontWeight: 'bold',
  },
  lessonPlusItem: {
    marginVertical: 4,
  },
  lessonPlusHeader: {
    flexDirection: 'row',
    padding: 16,
  },
  lessonPlusAuthorContainer: {
    flex: 1,
  },
  lessonPlusReactionsContainer: {
    flexDirection: 'row',
    marginTop: 8,
    marginHorizontal: -8,
    marginVertical: -8,
  },
  iconButton: {
    paddingHorizontal: 0,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  profileSocialsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  profileParametersSection: {
    flexDirection: 'row',
    marginVertical: 16,
    marginHorizontal: 8,
  },
  profileParameter: {
    flex: 1,
    marginHorizontal: 8,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  buttons: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 8,
  },
  textConfirm: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  inputDeleted: {
    marginVertical: 8,
  },
});
