import React from 'react';
import { ScrollView } from 'react-native';
import {
	Button,
	StyleService,
	useStyleSheet,
	ListItem,
	Input,
	Layout,
} from '@ui-kitten/components';
import ModalErrorPopup from '../../components/modal-error';
import { changeUser } from '../../redux/actions/userActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeSession } from '../../redux/actions/userActions';
// import * as RNIap from 'react-native-iap';
import TopNavigation from '../../components/top-navigation';

const mapStateToProps = (state) => {
	const { user } = state;
	return { user };
};

const mapDispatchToProps = (dispatch) =>
	bindActionCreators(
		{
			changeUser,
			changeSession,
		},
		dispatch
	);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)((props) => {
	const { navigation, user, changeUser } = props;
	const styles = useStyleSheet(themedStyle);

	const [search, setSearch] = React.useState('');
	const [error, setError] = React.useState('');

	const renderItemAccessory = (props) => (
		<Button
			size="tiny"
			status="success"
			onPress={() => {
				// RNIap.requestSubscription('start');
			}}
		>
			Выбрать
		</Button>
	);

	const renderItem = ({ item, index }) => (
		<ListItem
			title={`${item.title}`}
			description={`${item.description}`}
			accessoryRight={renderItemAccessory}
		/>
	);
	return (
		<ScrollView
			style={styles.container}
			contentContainerStyle={styles.contentContainer}
			level="1"
		>
			<TopNavigation title="Поиск" />
			<ModalErrorPopup error={error} setError={(err) => setError({ err })} />
			<Layout style={styles.form}>
				<Input
					placeholder="Фамилия, шрих-код"
					value={search}
					onChangeText={(val) => {
						setSearch(val);
					}}
					style={styles.input}
					size="large"
				/>
			</Layout>
		</ScrollView>
	);
});

const themedStyle = StyleService.create({
	text: {
		paddingHorizontal: 12,
		fontSize: 12,
		marginTop: 12,
	},
	form: {
		flex: 1,
		paddingHorizontal: 16,
		paddingVertical: 16,
	},
	container: {
		flex: 1,
		backgroundColor: 'background-basic-color-1',
	},
	contentContainer: {
		paddingBottom: 24,
	},
	section: {
		marginTop: 24,
	},
	input: {
		width: '100%',
	},
	backdrop: {
		backgroundColor: 'rgba(0, 0, 0, 0.5)',
	},
	content: {
		width: 220,
	},
});
