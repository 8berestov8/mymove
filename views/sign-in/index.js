import React from 'react';
import { StyleSheet, View, Linking, TouchableOpacity } from 'react-native';
import { Input, Text, Spinner } from '@ui-kitten/components';
import { ImageOverlay } from './extra/image-overlay.component';

import { KeyboardAvoidingView } from './extra/3rd-party';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AsyncStorage from '@react-native-async-storage/async-storage';

import ModalConfirmPopup from '../../components/modal-confirm';

import API from '../../API';

import { changeSession } from '../../redux/actions/userActions';
import { checkPhone, checkSmsCode } from '../../API/Users';

const mapStateToProps = (state) => {
  const { user } = state;
  return { user };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      changeSession,
    },
    dispatch
  );
const policyURL = 'https://mymove.ru/policy.html';

export default connect(
  mapStateToProps,
  mapDispatchToProps
)((props) => {
  const { user, changeSession, navigation } = props;
  const [phone, setPhone] = React.useState('');
  const [code, setCode] = React.useState('');
  const [sended, setSended] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [statusInputPhone, setStatusInputPhone] = React.useState('control');
  const [preventDefault, setPreventDefault] = React.useState(false);
  const [checkPhoneId, setCheckPhoneId] = React.useState(null);
  const [error, setError] = React.useState('');
  const [visibleModalConfirm, setVisibleModalConfirm] = React.useState(false);

  return (
    <KeyboardAvoidingView>
      <ModalConfirmPopup
        visible={visibleModalConfirm}
        setVisible={(v) => setVisibleModalConfirm(v)}
        title={`На телефон ${phone} поступит звонок`}
        subtitle="Для подверждения введите 4 последние цифры входящего номера"
        confirm={() => {
          setLoading(true);
          const regPhone = /^\+7 \(([0-9]{3})\) ([0-9]{3})\-([0-9]{2})\-([0-9]{2})$/;
          const matchPhone = phone.match(regPhone);
          setVisibleModalConfirm(false);
          checkPhone({
            phone: `7${matchPhone[1]}${matchPhone[2]}${matchPhone[3]}${matchPhone[4]}`,
          })
            .then((data) => {
              setLoading(false);
              setSended(true);
              setCheckPhoneId(data.id);
            })
            .catch((e) => {
              setLoading(false);
              setError(e);
            });
        }}
      />
      <ImageOverlay
        style={styles.container}
        source={require('./../../assets/image-background.jpg')}
      >
        <View style={styles.headerContainer}>
          <Text style={styles.helloLabel} status="control">
            MyMove
          </Text>
          <Text style={styles.signInLabel} category="s1" status="control">
            Онлайн абонемент для занятий спортом, фитнесом и танцами
          </Text>
        </View>
        <View style={styles.formContainer}>
          {
            <Input
              placeholder="Телефонный номер"
              size="large"
              caption=""
              status={statusInputPhone}
              value={phone}
              keyboardType="numeric"
              onKeyPress={(e) => {
                if (e.nativeEvent.key == 'Backspace') {
                  if (phone !== '+7 (') {
                    setPhone(
                      phone.slice(
                        0,
                        phone.length - (/(\-| |\))$/.test(phone) ? 2 : 1)
                      )
                    );
                    setPreventDefault(true);
                  }
                }
              }}
              onChangeText={(val) => {
                setError('');
                if (preventDefault) {
                  setPreventDefault(false);
                  return;
                }
                const regPhone = /^\+7 \(([0-9]{3})\) ([0-9]{3})\-([0-9]{2})\-([0-9]{2})$/;
                if (regPhone.test(val)) {
                  setPhone(val);
                  setVisibleModalConfirm(true);
                } else if (/^\+7 \([0-9]{3}$/.test(val)) {
                  setPhone(val + ') ');
                } else if (/^\+7 \([0-9]{3}\) [0-9]{3}$/.test(val)) {
                  setPhone(val + '-');
                } else if (/^\+7 \([0-9]{3}\) [0-9]{3}\-[0-9]{2}$/.test(val)) {
                  setPhone(val + '-');
                } else if (
                  /^\+7 \(([0-9]{1})?([0-9]{1})?([0-9]{1})?\)?( )?([0-9]{1})?([0-9]{1})?([0-9]{1})?(\-)?([0-9]{1})?([0-9]{1})?(\-)?([0-9]{1})?([0-9]{1})?$/.test(
                    val
                  )
                ) {
                  setPhone(val);
                  setSended(false);
                }
              }}
              onFocus={() => {
                if (phone == '') setPhone('+7 (');
              }}
            />
          }
          <Text style={styles.smsCaptionLabel}>
            На веденный номер телефона поступит звонок. Отвечать не нужно.
          </Text>
          {true && <Text style={styles.errorCaptionLabel}>{error}</Text>}
          {loading && (
            <View style={styles.spinnerContainer} level="1">
              <Spinner size="medium" />
            </View>
          )}
          {sended && (
            <Input
              placeholder="Последние 4 цифры входящего номера"
              style={styles.codeInput}
              size="large"
              status="control"
              keyboardType="numeric"
              value={code}
              onChangeText={setCode}
              autoFocus={true}
              maxLength={4}
              onChangeText={(code) => {
                setError('');
                setCode(code);
                if (/^[0-9]{4}$/.test(code)) {
                  checkSmsCode({ user, checkPhoneId, code })
                    .then((data) => {
                      changeSession(data.id_session);
                      AsyncStorage.setItem('sessionId', data.id_session);
                      navigation.reset({
                        index: 0,
                        routes: [{ name: 'ToDo' }],
                      });
                    })
                    .catch((e) => {
                      setError(e);
                    });
                }
              }}
            />
          )}
          {sended && (
            <Text style={styles.smsCaptionLabel}>
              Звонок поступит в течение минуты.
            </Text>
          )}
        </View>
      </ImageOverlay>
      <Text style={styles.policy}>
        Вводя номер телефона вы соглашаетесь с
        <TouchableOpacity onPress={() => Linking.openURL(policyURL)}>
          <Text style={styles.policyURL}>политикой конфиденциальности</Text>
        </TouchableOpacity>
      </Text>
    </KeyboardAvoidingView>
  );
});

const styles = StyleSheet.create({
  signInContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 24,
  },
  socialAuthContainer: {
    marginTop: 48,
  },
  evaButton: {
    maxWidth: 72,
    paddingHorizontal: 0,
  },
  formContainer: {
    padding: 16,
  },
  codeInput: {
    marginTop: 16,
  },
  spinnerContainer: {
    marginTop: 16,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
  },
  headerContainer: {
    minHeight: 216,
    paddingHorizontal: 64,
    justifyContent: 'center',
    alignItems: 'center',
  },
  helloLabel: {
    fontSize: 28,
    lineHeight: 32,
  },
  signInLabel: {
    marginTop: 8,
    textAlign: 'center',
  },
  tabView: {
    flex: 1,
  },
  formInput: {
    marginTop: 16,
  },
  policy: {
    textAlign: 'center',
    color: 'white',
    position: 'absolute',
    bottom: 10,
    width: '100%',
  },
  policyURL: {
    color: 'white',
    marginBottom: 5,
    textDecorationLine: 'underline',
  },
  smsCaptionLabel: {
    textAlign: 'center',
    paddingHorizontal: 32,
    color: 'white',
  },
  signInButton: {
    marginHorizontal: 16,
  },
  signUpButton: {
    marginVertical: 12,
    marginHorizontal: 16,
  },
  errorCaptionLabel: {
    color: 'white',
  },
});
