import React, { useState } from 'react';
import { Alert, ScrollView, View } from 'react-native';
import {
  Button,
  StyleService,
  useStyleSheet,
  Input,
  Text,
  Layout,
  Spinner,
  MenuItem,
  Icon,
  Popover,
  Menu,
} from '@ui-kitten/components';
import ModalErrorPopup from '../../components/modal-error';
import { ProfileAvatar } from './extra/profile-avatar.component';
import { changeUser } from '../../redux/actions/userActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { changeSession } from '../../redux/actions/userActions';
import * as ImagePicker from 'expo-image-picker';
import { setProfile } from '../../API/Users';

const mapStateToProps = (state) => {
  const { user } = state;
  return { user };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      changeUser,
      changeSession,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)((props) => {
  const { navigation, user, changeUser, changeSession } = props;
  const styles = useStyleSheet(themedStyle);
  const [firstname, setFirstname] = React.useState(user.firstname);
  const [lastname, setLastname] = React.useState(user.lastname);
  const [patronymic, setPatronymic] = React.useState(user.patronymic);

  const [error, setError] = React.useState('');
  const [statusFirstname, setStatusFirstname] = React.useState('basic');
  const [statusLastname, setStatusLastname] = React.useState('basic');
  const [statusPatronymic, setStatusPatronymic] = React.useState('basic');

  const [loading, setLoading] = React.useState(false);

  const onDoneButtonPress = () => {
    let hasErrors = false;
    setStatusFirstname('basic');
    setStatusLastname('basic');
    setStatusPatronymic('basic');
    if (firstname.length === 0) {
      setStatusFirstname('danger');
      hasErrors = true;
    }
    if (lastname.length === 0) {
      setStatusLastname('danger');
      hasErrors = true;
    }
    if (patronymic.length === 0) {
      setStatusPatronymic('danger');
      hasErrors = true;
    }
    if (!hasErrors) {
      setLoading(true);
      const data = new FormData();
      data.append('firstname', firstname);
      data.append('lastname', lastname);
      data.append('patronymic', patronymic);
      if (avatar) {
        const localUri = avatar.uri;
        const filename = localUri.split('/').pop();
        const match = /\.(\w+)$/.exec(filename);
        const type = match ? `image/${match[1]}` : `image`;
        data.append('avatar', { uri: localUri, name: filename, type });
      }
      setProfile({ user, data })
        .then((data) => {
          setLoading(false);
          changeUser(data);
          setAvatar(null);
        })
        .catch((e) => {
          setLoading(false);
        });
    }
  };

  const CameraIcon = (props) => <Icon {...props} name="camera" />;
  const GaleryIcon = (props) => <Icon {...props} name="image-outline" />;
  const [visible, setVisible] = React.useState(false);

  const renderPhotoButton = () => (
    <Button
      style={styles.editAvatarButton}
      status="basic"
      accessoryLeft={CameraIcon}
      onPress={() => setVisible(true)}
    />
  );

  const BottomMenu = () => <View></View>;

  const [avatar, setAvatar] = useState(null);

  const openCamera = async () => {
    const { status } = await ImagePicker.requestCameraPermissionsAsync();
    if (status !== 'granted') {
      setError('У MyMove нет доступа к фото. Разрешите доступ.');
    } else {
      const result = await ImagePicker.launchCameraAsync({
        cameraType: 'front',
      }).catch((error) => console.log(error));
      setVisible(false);
      if (!result.cancelled) {
        setAvatar(result);
      }
    }
  };

  const openLibrary = async () => {
    const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
    if (status !== 'granted') {
      setError('У MyMove нет доступа к файлам. Разрешите доступ.');
    } else {
      const result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.photo,
        allowsEditing: true,
        aspect: [5, 5],
        quality: 1,
      });
      setVisible(false);
      if (!result.cancelled) {
        setAvatar(result);
      }
    }
  };

  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={styles.contentContainer}
      level="1"
    >
      <View style={styles.topButtons} level="1">
        <Button
          appearance="ghost"
          onPress={() => {
            changeSession('');
            AsyncStorage.removeItem('sessionId');
            navigation.reset({
              index: 0,
              routes: [{ name: 'Auth' }],
            });
          }}
        >
          Выйти
        </Button>
      </View>
      <ModalErrorPopup error={error} setError={(err) => setError({ err })} />
      {loading && (
        <View style={styles.loadingContainer}>
          <Spinner size="small" />
        </View>
      )}

      <ProfileAvatar
        style={styles.profileAvatar}
        source={avatar ? avatar : user.avatar}
        editButton={renderPhotoButton}
      />

      <Popover
        backdropStyle={styles.backdrop}
        visible={visible}
        anchor={BottomMenu}
        onBackdropPress={() => setVisible(false)}
      >
        <Layout>
          <Menu style={styles.content}>
            <MenuItem
              title="Выбрать из галереи"
              accessoryLeft={GaleryIcon}
              onPress={openLibrary}
            />
            <MenuItem
              accessoryLeft={CameraIcon}
              title="Открыть камеру"
              onPress={openCamera}
            />
          </Menu>
        </Layout>
      </Popover>

      <View
        level="1"
        style={[styles.containerSetting, styles.profileSetting, styles.section]}
      >
        <Text appearance="hint" category="s1">
          {'Фамилия'}
        </Text>
        <Input
          placeholder="Введите вашу фамилию"
          value={lastname}
          onChangeText={(val) => {
            setLastname(val);
          }}
          status={statusLastname}
          style={styles.input}
          size="large"
        />
      </View>
      <View level="1" style={[styles.containerSetting, styles.profileSetting]}>
        <Text appearance="hint" category="s1">
          {'Имя'}
        </Text>
        <Input
          placeholder="Введите ваше имя"
          value={firstname}
          onChangeText={(val) => {
            setFirstname(val);
          }}
          status={statusFirstname}
          style={styles.input}
          size="large"
        />
      </View>
      <View level="1" style={[styles.containerSetting, styles.profileSetting]}>
        <Text appearance="hint" category="s1">
          {'Отчество'}
        </Text>
        <Input
          placeholder="Введите ваше отчество"
          value={patronymic}
          onChangeText={(val) => {
            setPatronymic(val);
          }}
          status={statusPatronymic}
          style={styles.input}
          size="large"
        />
      </View>
      <Button style={styles.doneButton} onPress={onDoneButtonPress}>
        СОХРАНИТЬ
      </Button>
    </ScrollView>
  );
});

const themedStyle = StyleService.create({
  topButtons: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  container: {
    flex: 1,
    backgroundColor: 'background-basic-color-1',
  },
  containerSetting: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  contentContainer: {
    paddingBottom: 24,
  },
  profileAvatar: {
    aspectRatio: 1.0,
    height: 124,
    width: 124,
    alignSelf: 'center',
  },
  editAvatarButton: {
    aspectRatio: 1.0,
    height: 48,
    width: 48,
    borderRadius: 24,
  },
  profileSetting: {
    padding: 16,
  },
  section: {
    marginTop: 24,
  },
  doneButton: {
    marginHorizontal: 24,
    marginTop: 24,
  },
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 7777,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    width: '60%',
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  content: {
    width: 220,
  },
});
