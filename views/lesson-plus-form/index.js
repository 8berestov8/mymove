import React from 'react';
import { View } from 'react-native';
import {
  Button,
  Divider,
  Input,
  Layout,
  StyleService,
  useStyleSheet,
  Spinner,
  NativeDateService,
  CheckBox,
  RangeDatepicker,
  IndexPath,
  Select,
  SelectItem,
} from '@ui-kitten/components';
import moment from 'moment';
import { KeyboardAvoidingView } from '../../components/3rd-party';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setLessonsPlus } from '../../redux/actions/lessonsPlusActions';
import { setServices } from '../../redux/actions/servicesActions';
import ModalErrorPopup from '../../components/modal-error';
import { calendarLang } from '../../i18n/';
import TopNavigation from '../../components/top-navigation';
import { getServices } from '../../API/Services';
import { addLessonPlus, editLessonPlus } from '../../API/LessonPlus';

const mapStateToProps = (state) => {
  const { user, lessonsPlus, services } = state;
  return { user, lessonsPlus, services };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      setLessonsPlus,
      setServices,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  ({
    navigation,
    route,
    lessonsPlus,
    services,
    user,
    setLessonsPlus,
    setServices,
  }) => {
    const idLessonPlus = route.params.id;
    const idSchool = route.params.school_id;
    const idStudent = route.params.student_id;
    const getIndexServiceById = (id) => {
      let findIndexService =
        typeof services.list[idSchool] !== 'undefined'
          ? services.list[idSchool].findIndex((v) => v.id == id)
          : 0;
      return findIndexService > 0 ? findIndexService : 0;
    };
    const getServiceById = (id) => {
      return typeof services.list[idSchool] !== 'undefined'
        ? services.list[idSchool].find((v) => v.id == id)
        : undefined;
    };
    React.useEffect(() => {
      if (typeof services.list[idSchool] === 'undefined') {
        getServices({ user, idSchool })
          .then((services) => {
            setServices(idSchool, services);
            if (services.length > 0 && (idService == null || idService == '')) {
              changeService(services[0]);
              changeRange(services[0]);
            }
          })
          .catch((e) => {
            setError(e);
          });
      }
    }, []);
    const type =
      typeof route.params !== 'undefined' &&
      typeof route.params.id !== 'undefined'
        ? 'edit'
        : 'add';

    let lesson = {
      service_id: '',
      count_lessons: 0,
      infinity_lessons: false,
      date_start: new Date(),
      date_end: new Date(),
    };
    if (type === 'edit') {
      lesson = {
        ...lessonsPlus.list[idStudent].find((v) => v.id == route.params.id),
      };
    }

    const styles = useStyleSheet(themedStyles);

    const [range, setRange] = React.useState({
      startDate: moment(lesson.date_start).toDate(),
      endDate: moment(lesson.date_end).toDate(),
    });
    const [idService, setIdService] = React.useState(lesson.service_id);
    const [indexService, setIndexService] = React.useState(
      new IndexPath(getIndexServiceById(idService))
    );
    const _service = getServiceById(idService);
    const displayValueService =
      typeof _service != 'undefined' ? _service?.name : '-';
    const [countLessons, setCountLessons] = React.useState(
      lesson.count_lessons.toString()
    );
    const [infinityLessons, setInfinityLessons] = React.useState(
      Boolean(lesson.infinity_lessons)
    );
    const [statusCountLessons, setStatusCountLessons] = React.useState('basic');
    const [statusIdService, setStatusIdService] = React.useState('basic');
    const [statusPeriod, setStatusPeriod] = React.useState('basic');

    const [loading, setLoading] = React.useState(false);
    const [error, setError] = React.useState('');

    const getEndDateByPeriod = (startDate, period, count_days) => {
      const regMonth = /^([0-9]+)?(month)$/;
      if (regMonth.test(period)) {
        const match = period.match(regMonth);
        const countMonth = typeof match[1] !== 'undefined' ? match[1] : 1;
        return moment(startDate).add(countMonth, 'month').toDate();
      } else if (period == 'days') {
        return moment(startDate).add(count_days, 'days').toDate();
      } else {
        return null;
      }
    };
    const changeService = (service) => {
      setIdService(service.id);
      setInfinityLessons(Boolean(service.infinity_lessons));
      if (!Boolean(service.infinity_lessons)) {
        setCountLessons(service.count_lessons.toString());
      }
    };
    const changeRange = (service, _range) => {
      _range = typeof _range != 'undefined' ? _range : range;
      if (typeof service != 'undefined') {
        setRange({
          ..._range,
          endDate: getEndDateByPeriod(
            _range.startDate,
            service.period,
            service.count_days
          ),
        });
        /**
          setRange({
            ...range,
          });
          */
      }
    };
    const onSaveButtonPress = () => {
      let hasErrors = false;
      setStatusCountLessons('basic');
      setStatusPeriod('basic');
      setStatusIdService('basic');
      if (!infinityLessons && countLessons.length === 0) {
        setStatusCountLessons('danger');
        hasErrors = true;
      }
      if (idService.length === 0) {
        setStatusIdService('danger');
        hasErrors = true;
      }
      if (!range.startDate || !range.endDate) {
        setStatusPeriod('danger');
        hasErrors = true;
      }
      if (!hasErrors) {
        setLoading(true);
        let formLessonPlus;
        if (type === 'edit') {
          formLessonPlus = editLessonPlus;
        } else {
          formLessonPlus = addLessonPlus;
        }
        formLessonPlus({
          user,
          idSchool,
          idLessonPlus,
          data: {
            service_id: idService,
            student_id: idStudent,
            count_lessons: countLessons,
            infinity_lessons: infinityLessons,
            date_start: moment(range.startDate).format('YYYY-MM-DD'),
            date_end: moment(range.endDate).format('YYYY-MM-DD'),
          },
        })
          .then((data) => {
            setLoading(false);
            if (type === 'add') {
              lessonsPlus.list[idStudent].unshift(data);
            } else {
              lessonsPlus.list[idStudent] = lessonsPlus.list[idStudent].map(
                (v) => {
                  return v.id == lesson.id ? data : v;
                }
              );
            }
            setLessonsPlus(idStudent, lessonsPlus.list[idStudent]);
            navigation.navigate('LessonsPlusList', {
              school_id: idSchool,
              student_id: idStudent,
            });
          })
          .catch((e) => {
            setLoading(false);
            setError(e);
          });
      }
    };

    const localeDateService = new NativeDateService('ru', {
      format: 'DD.MM.YYYY',
      i18n: calendarLang,
      startDayOfWeek: 1,
    });
    return (
      <KeyboardAvoidingView style={styles.container}>
        <TopNavigation
          title={
            type == 'add' ? 'Добавление занятий' : 'Редактирование занятий'
          }
        />
        {loading && (
          <View style={styles.loadingContainer}>
            <Spinner size="small" />
          </View>
        )}
        <ModalErrorPopup error={error} setError={(err) => setError({ err })} />
        <Layout style={styles.form} level="1">
          {type == 'add' && (
            <Select
              style={styles.input}
              label="Услуга"
              value={displayValueService}
              selectedIndex={indexService}
              onSelect={(i) =>
                setIndexService(i) ||
                changeService(services.list[idSchool][i.row]) ||
                changeRange(services.list[idSchool][i.row])
              }
              status={statusIdService}
              size="large"
            >
              {services.list[idSchool]?.map((p) => (
                <SelectItem key={`period_${p.id}`} title={p.name} />
              ))}
            </Select>
          )}
          <CheckBox
            style={styles.input}
            checked={infinityLessons}
            disabled={type == 'add'}
            onChange={(nextChecked) => setInfinityLessons(nextChecked)}
          >
            {'Бесконечное количество занятий'}
          </CheckBox>
          {!infinityLessons && (
            <Input
              style={styles.input}
              label="Количество занятий"
              value={countLessons}
              onChangeText={setCountLessons}
              disabled={type == 'add'}
              status={statusCountLessons}
              keyboardType="numeric"
              size="large"
            />
          )}
          <RangeDatepicker
            style={styles.input}
            label="Период"
            placeholder=""
            range={range}
            onSelect={(v) =>
              setRange(v) || changeRange(getServiceById(idService), v)
            }
            dateService={localeDateService}
            status={statusPeriod}
            size="large"
          />
        </Layout>
        <Divider />
        <Button style={styles.addButton} onPress={onSaveButtonPress}>
          СОХРАНИТЬ
        </Button>
      </KeyboardAvoidingView>
    );
  }
);

const themedStyles = StyleService.create({
  container: {
    flex: 1,
    backgroundColor: 'background-basic-color-2',
  },
  form: {
    flex: 1,
    paddingHorizontal: 4,
    paddingVertical: 24,
  },
  input: {
    marginHorizontal: 12,
    marginVertical: 8,
  },
  middleContainer: {
    flexDirection: 'row',
  },
  middleInput: {
    width: 128,
  },
  addButton: {
    marginHorizontal: 16,
    marginVertical: 24,
  },
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 7777,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginHorizontal: 12,
    marginVertical: 8,
  },
});
