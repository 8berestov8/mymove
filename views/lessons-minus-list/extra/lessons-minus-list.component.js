import React from 'react';
import { ListRenderItemInfo, StyleSheet, View } from 'react-native';
import {
  Button,
  Card,
  List,
  Text,
  OverflowMenu,
  MenuItem,
} from '@ui-kitten/components';
import { MoreHorizontalIcon } from '../../../components/icons';
import API from '../../../API';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setLessonsMinus } from '../../../redux/actions/lessonsMinusActions';
import ModalDeletePopup from '../../../components/modal-delete';
import moment from 'moment';
import 'moment/locale/ru';
import TextNotFound from '../../../components/text-not-found';
import { deleteLessonMinus } from '../../../API/LessonMinus';
moment.locale('ru');

const mapStateToProps = (state) => {
  const { user, schools, lessonsMinus } = state;
  return { user, schools, lessonsMinus };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      setLessonsMinus,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)((props) => {
  const { user, lessonsMinus, setLessonsMinus, loading } = props;
  const idSchool = props.school_id;
  const idStudent = props.student_id;
  const idLessonMinus = props.lesson_plus_id;
  const [idlessonMinus, setIdlessonMinus] = React.useState(0);
  const [visibleModalDelete, setVisibleModalDelete] = React.useState(false);
  const ModalDelete = () => (
    <ModalDeletePopup
      visible={visibleModalDelete}
      setVisible={(v) => setVisibleModalDelete(v)}
      confirmDelete={() => {
        deleteLessonMinus({ user, idSchool, idlessonMinus }).then((data) => {
          lessonsMinus.list[idLessonMinus] = lessonsMinus.list[
            idLessonMinus
          ].filter((v) => {
            return v.id != idlessonMinus;
          });
          setLessonsMinus(idLessonMinus, lessonsMinus.list[idLessonMinus]);
          setVisibleModalDelete(false);
        });
      }}
    />
  );
  const RenderserviceContent = ({ lessonMinus }) => {
    const [visible, setVisible] = React.useState(false);

    const renderToggleButton = () => (
      <Button
        style={styles.iconButton}
        appearance="ghost"
        status="basic"
        accessoryLeft={MoreHorizontalIcon}
        onPress={() => setVisible(true)}
      />
    );

    const onItemSelect = (index) => {
      setVisible(false);
    };

    return (
      <View style={styles.lessonMinusHeader}>
        <View style={styles.lessonMinusAuthorContainer}>
          <Text category="s1">
            {moment(lessonMinus.date_visit).format('DD MMM')}
          </Text>
          <Text appearance="hint" category="c1">
            {lessonMinus.count_lessons} занятий
          </Text>
        </View>

        <OverflowMenu
          anchor={renderToggleButton}
          backdropStyle={styles.backdrop}
          visible={visible}
          onSelect={onItemSelect}
          onBackdropPress={() => setVisible(false)}
        >
          <MenuItem
            title="Удалить"
            onPress={() => {
              setIdlessonMinus(lessonMinus.id);
              setVisible(false);
              setVisibleModalDelete(true);
            }}
          />
        </OverflowMenu>
      </View>
    );
  };

  const renderItem = (info) => (
    <Card style={styles.lessonMinusItem}>
      <RenderserviceContent lessonMinus={info.item} />
    </Card>
  );
  return (
    <>
      <List
        {...props}
        renderItem={renderItem}
        ListEmptyComponent={
          !loading && <TextNotFound text={'Отмеченные занятие не добавлены'} />
        }
      />
      <ModalDelete />
    </>
  );
});

const styles = StyleSheet.create({
  activeValidity: {
    fontSize: 14,
    color: 'green',
    fontWeight: 'bold',
  },
  noActiveValidity: {
    color: 'red',
    fontWeight: 'bold',
  },
  lessonMinusItem: {
    marginVertical: 4,
  },
  lessonMinusHeader: {
    flexDirection: 'row',
  },
  lessonMinusAuthorContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  lessonMinusReactionsContainer: {
    flexDirection: 'row',
    marginTop: 8,
    marginHorizontal: -8,
    marginVertical: -8,
  },
  iconButton: {
    paddingHorizontal: 0,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  profileParametersSection: {
    flexDirection: 'row',
    marginVertical: 16,
    marginHorizontal: 8,
  },
  profileParameter: {
    flex: 1,
    marginHorizontal: 8,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  buttons: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 8,
  },
  textConfirm: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  inputDeleted: {
    marginVertical: 8,
  },
});
