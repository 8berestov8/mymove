import React from 'react';
import { StyleSheet, View, RefreshControl } from 'react-native';
import { Layout } from '@ui-kitten/components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import API from '../../API';
import LessonsMinusListComponent from './extra/lessons-minus-list.component';
import { setLessonsMinus } from '../../redux/actions/lessonsMinusActions';
import TopNavigation from '../../components/top-navigation';
import { getLessonMinus } from '../../API/LessonMinus';

const mapStateToProps = (state) => {
  const { user, lessonsMinus } = state;
  return { user, lessonsMinus };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      setLessonsMinus,
    },
    dispatch
  );

class lessonsList extends React.Component {
  constructor(props) {
    const idSchool = props.route.params.school_id;
    const idlessonPlus = props.route.params.lesson_plus_id;
    super(props);
    this.state = {
      refreshing: false,
      error: '',
      loading: true,
    };
    const { user, setLessonsMinus } = props;
    this.loadData = () => {
      return new Promise((resolve, reject) => {
        getLessonMinus({ user, idSchool, idlessonPlus })
          .then((data) => {
            resolve(data);
            setLessonsMinus(idlessonPlus, data);
          })
          .catch((e) => {
            reject(e);
          });
      });
    };
    this.loadData()
      .then(() => {
        this.setState({ loading: false });
      })
      .catch((err) => {
        this.setState({ loading: false });
        this.setState({ error: err });
      });
  }
  render() {
    const props = this.props;
    const idSchool = props.route.params.school_id;
    const idlessonPlus = props.route.params.lesson_plus_id;
    const { lessonsMinus } = props;

    const onRefresh = () => {
      this.setState({ refreshing: true });
      this.loadData()
        .then(() => {
          this.setState({ refreshing: false });
        })
        .catch((err) => {
          this.setState({ refreshing: false });
          this.setState({ error: err });
        });
    };

    return (
      <Layout style={styles.container} level="2">
        <TopNavigation title="Отмеченные занятия" />
        <View style={styles.body}>
          <LessonsMinusListComponent
            style={styles.list}
            data={
              lessonsMinus.list[idlessonPlus]
                ? lessonsMinus.list[idlessonPlus]
                : []
            }
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={onRefresh}
              />
            }
            school_id={idSchool}
            lesson_plus_id={idlessonPlus}
            loading={this.state.loading}
            {...props}
          />
        </View>
      </Layout>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(lessonsList);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    flex: 1,
  },
  header: {
    padding: 16,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  body: {
    flex: 1,
    paddingHorizontal: 16,
    paddingTop: 16,
  },
  followButton: {
    margin: 2,
  },
  button: {},
});
