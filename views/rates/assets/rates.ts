export default [
  { title: 'Free', description: 'до 15 пользователей', id: 'free' },
  { title: 'Start', description: 'до 50 пользователей', id: 'start' },
  { title: 'Go', description: 'до 100 пользователей', id: 'go' },
  { title: 'Pro', description: 'до 250 пользователей', id: 'pro' },
];
