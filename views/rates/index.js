import React from 'react';
import { ScrollView, Platform } from 'react-native';
import {
	Button,
	StyleService,
	useStyleSheet,
	Text,
	ListItem,
	List,
} from '@ui-kitten/components';
import ModalErrorPopup from '../../components/modal-error';
import { changeUser } from '../../redux/actions/userActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeSession } from '../../redux/actions/userActions';
// import * as RNIap from 'react-native-iap';
import TopNavigation from '../../components/top-navigation';
import data from './assets/rates';

const mapStateToProps = (state) => {
	const { user } = state;
	return { user };
};

const mapDispatchToProps = (dispatch) =>
	bindActionCreators(
		{
			changeUser,
			changeSession,
		},
		dispatch
	);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)((props) => {
	const { navigation, user, changeUser } = props;
	const styles = useStyleSheet(themedStyle);

	const [error, setError] = React.useState('');
	if (Platform.OS === 'ios') {
		// RNIap?.initConnection()?.then(() => {
		// 	const items = Platform.select({
		// 		ios: ['start'],
		// 		android: ['start', 'go'],
		// 	});
		// 	RNIap.getSubscriptions(items).then(({ responseCode, results }) => {
		// 		console.log(responseCode, results);
		// 	});
		// });
	}
	const renderItemAccessory = (props) => (
		<Button size="tiny" status="success">
			Выбрать
		</Button>
	);

	const renderItem = ({ item, index }) => (
		<ListItem
			title={`${item.title}`}
			description={`${item.description}`}
			accessoryRight={renderItemAccessory}
			onPress={() => {
				RNIap.requestSubscription(item.id);
			}}
		/>
	);
	return (
		<ScrollView
			style={styles.container}
			contentContainerStyle={styles.contentContainer}
			level="1"
		>
			<TopNavigation title="Тарифы" />

			<ModalErrorPopup error={error} setError={(err) => setError({ err })} />

			<List style={styles.container} data={data} renderItem={renderItem} />

			<Text style={styles.text}>*1 пользователь - 1 тренер или 1 ученик</Text>
		</ScrollView>
	);
});

const themedStyle = StyleService.create({
	text: {
		paddingHorizontal: 12,
		fontSize: 12,
		marginTop: 12,
	},
	container: {
		flex: 1,
		backgroundColor: 'background-basic-color-1',
	},
	contentContainer: {
		paddingBottom: 24,
	},
	section: {
		marginTop: 24,
	},
	input: {
		width: '60%',
	},
	backdrop: {
		backgroundColor: 'rgba(0, 0, 0, 0.5)',
	},
	content: {
		width: 220,
	},
});
