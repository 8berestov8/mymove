import React from 'react';
import { View } from 'react-native';
import {
  Button,
  Divider,
  Input,
  Layout,
  StyleService,
  useStyleSheet,
  Spinner,
  CheckBox,
  Select,
  SelectItem,
  IndexPath,
} from '@ui-kitten/components';
import moment from 'moment';
import { KeyboardAvoidingView } from '../../components/3rd-party';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import API from '../../API';
import { setServices } from '../../redux/actions/servicesActions';
import { setTeachers } from '../../redux/actions/teachersActions';
import ModalErrorPopup from '../../components/modal-error';
import TopNavigation from '../../components/top-navigation';
import { addService, editService } from '../../API/Services';

const mapStateToProps = (state) => {
  const { user, services, teachers } = state;
  return { user, services, teachers };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      setServices,
      setTeachers,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(({ navigation, route, services, user, setServices }) => {
  const idService = route.params.id;
  const idSchool = route.params.school_id;

  const type =
    typeof route.params !== 'undefined' &&
    typeof route.params.id !== 'undefined'
      ? 'edit'
      : 'add';
  let service = {
    name: '',
    price: '',
    period: 'month',
    count_lessons: '',
    count_days: '',
    infinity_lessons: false,
  };
  if (type === 'edit') {
    service = {
      ...services.list[idSchool].find((v) => v.id == route.params.id),
    };
    service.birth_date = !service.birth_date
      ? service.birth_date
      : moment(service.birth_date).toDate();
  }

  const styles = useStyleSheet(themedStyles);
  const [name, setName] = React.useState(service.name);
  const [price, setPrice] = React.useState(service.price);
  const [period, setPeriod] = React.useState(service.period);
  const [countDays, setCountDays] = React.useState(service.count_days);
  const [countLessons, setCountLessons] = React.useState(
    service.count_lessons?.toString()
  );
  const [infinityLessons, setInfinityLessons] = React.useState(
    Boolean(service.infinity_lessons)
  );
  const [indexPeriod, setIndexPeriod] = React.useState(new IndexPath(0));

  const [statusName, setStatusName] = React.useState('basic');
  const [statusPrice, setStatusPrice] = React.useState('basic');
  const [statusPeriod, setStatusPeriod] = React.useState('basic');
  const [statusCountDays, setStatusCountDays] = React.useState('basic');
  const [statusCountLessons, setStatusCountLessons] = React.useState('basic');

  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState('');

  const onSaveButtonPress = () => {
    let hasErrors = false;
    setStatusName('basic');
    setStatusPrice('basic');
    setStatusPeriod('basic');
    setStatusCountLessons('basic');
    setStatusCountDays('basic');
    if (name.length === 0) {
      setStatusName('danger');
      hasErrors = true;
    }
    if (price.length === 0) {
      setStatusPrice('danger');
      hasErrors = true;
    }
    if (period.length === 0) {
      setStatusPeriod('danger');
      hasErrors = true;
    }
    if (!infinityLessons && countLessons.length === 0) {
      setStatusCountLessons('danger');
      hasErrors = true;
    }
    if (period === 'days') {
      if (countDays.length === 0) {
        setStatusCountDays('danger');
        hasErrors = true;
      }
    }
    if (!hasErrors) {
      setLoading(true);
      let formService;
      if (type === 'edit') {
        formService = editService;
      } else {
        formService = addService;
      }
      formService({
        user,
        idSchool,
        idService,
        data: {
          name,
          price,
          period,
          count_days: countDays,
          count_lessons: countLessons,
          infinity_lessons: infinityLessons,
        },
      })
        .then((data) => {
          setLoading(false);
          if (type === 'add') {
            services.list[idSchool].unshift(data);
          } else {
            services.list[idSchool] = services.list[idSchool].map((v) => {
              return v.id == service.id ? data : v;
            });
          }
          setServices(idSchool, services.list[idSchool]);
          navigation.navigate('ServicesList', {
            school_id: idSchool,
          });
        })
        .catch((e) => {
          setLoading(false);
          setError(e);
        });
    }
  };

  const periods = [
    { title: '1 месяц', index: 'month' },
    { title: '2 месяца', index: '2month' },
    { title: '3 месяца', index: '3month' },
    { title: '6 месяцев', index: '6month' },
    { title: '12 месяцев', index: '12month' },
    { title: 'Установить количество дней', index: 'days' },
  ];
  const displayValue = periods[indexPeriod.row].title;

  return (
    <KeyboardAvoidingView style={styles.container}>
      <TopNavigation
        title={type == 'add' ? 'Добавление услуги' : 'Редактирование услуги'}
      />
      {loading && (
        <View style={styles.loadingContainer}>
          <Spinner size="small" />
        </View>
      )}
      <ModalErrorPopup error={error} setError={(err) => setError({ err })} />
      <Layout style={styles.form} level="1">
        <Input
          style={styles.input}
          label="Название"
          value={name}
          onChangeText={setName}
          status={statusName}
          size="large"
        />
        <Input
          style={styles.input}
          label="Стоимость"
          value={price}
          onChangeText={setPrice}
          status={statusPrice}
          keyboardType="numeric"
          size="large"
        />
        <Select
          style={styles.input}
          label="Период"
          value={displayValue}
          selectedIndex={indexPeriod}
          onSelect={(i) => setIndexPeriod(i) || setPeriod(periods[i.row].index)}
          status={statusPeriod}
          size="large"
        >
          {periods.map((p) => (
            <SelectItem key={`period_${p.index}`} title={p.title} />
          ))}
        </Select>
        {period == 'days' && (
          <Input
            style={styles.input}
            label="Количество дней"
            value={countDays}
            onChangeText={setCountDays}
            status={statusCountDays}
            keyboardType="numeric"
          />
        )}
        <CheckBox
          style={styles.input}
          checked={infinityLessons}
          onChange={(nextChecked) => setInfinityLessons(nextChecked)}
        >
          {'Бесконечное количество занятий'}
        </CheckBox>
        {!infinityLessons && (
          <Input
            style={styles.input}
            label="Количество занятий"
            value={countLessons}
            onChangeText={setCountLessons}
            status={statusCountLessons}
            size="large"
            keyboardType="numeric"
          />
        )}
      </Layout>
      <Divider />
      <Button style={styles.addButton} onPress={onSaveButtonPress}>
        СОХРАНИТЬ
      </Button>
    </KeyboardAvoidingView>
  );
});

const themedStyles = StyleService.create({
  container: {
    flex: 1,
    backgroundColor: 'background-basic-color-2',
  },
  form: {
    flex: 1,
    paddingHorizontal: 4,
    paddingVertical: 24,
  },
  input: {
    marginHorizontal: 12,
    marginVertical: 8,
  },
  middleContainer: {
    flexDirection: 'row',
  },
  middleInput: {
    width: 128,
  },
  addButton: {
    marginHorizontal: 16,
    marginVertical: 24,
  },
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 7777,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginHorizontal: 12,
    marginVertical: 8,
  },
});
