import React from 'react';

import { TypesListScreen } from './types-list.component';


export default ({navigation}) => {
  return (
    <TypesListScreen navigation={navigation}/>
  );
};
