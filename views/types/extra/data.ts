import { ImageSourcePropType } from 'react-native';

export enum TrainingLevel {
  EASY = 'Easy',
  MIDDLE = 'Middle',
  HARD = 'Hard',
}

export class Type {

  constructor(readonly title: string,
              readonly level: string,
              readonly image: ImageSourcePropType,
              readonly onPress: Function) {
  }

  get formattedLevel(): string {
    return `${this.level}`;
  }


  
  static search(onPress: Function): Type {
    return new Type(
      'Поиск',
      'Ищите школу танцев, фитнес студию, спортивную секцию в вашем городе',
      require('./../../../assets/image-training-1.jpg'),
      onPress,
    );
  }

  static client(onPress: Function): Type {
    return new Type(
      'Ученик',
      'Статистика посещений 24 часа в сутки',
      require('./../../../assets/image-training-2.jpg'),
      onPress,
    );
  }

  static teacher(onPress: Function): Type {
    return new Type(
      'Тренер/Педагог',
      'Ведение групп учеников',
      require('./../../../assets/image-training-3.jpg'),
      onPress,
    );
  }

  static owner(onPress: Function): Type {
    return new Type(
      'Руководитель',
      'Управление своей школой танцев, фитнес клубом или спортивной секцией',
      require('./../../../assets/image-training-5.jpg'),
      onPress
    );
  }
  
}