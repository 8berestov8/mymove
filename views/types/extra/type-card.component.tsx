import React from 'react';
import { StyleSheet } from 'react-native';
import { Card, CardElement, CardProps, Text } from '@ui-kitten/components';
import { ImageOverlay } from './image-overlay.component';
import { Type } from './data';

export interface TypeCardProps extends Omit<CardProps, 'children'> {
  type: Type;
  style: any,
}

export type TypeCardElement = React.ReactElement<TypeCardProps>;

export const TypeCard = (props: TypeCardProps): CardElement => {

  const { style, type, ...cardProps } = props;

  return (
    <Card
      {...cardProps}
      style={[styles.container, style]}
      >
      <ImageOverlay
        style={styles.image}
        source={type.image}>
        <Text
          style={styles.level}
          category='s1'
          status='control'>
          {type.formattedLevel}
        </Text>
        <Text
          style={styles.title}
          category='h2'
          status='control'>
          {type.title}
        </Text>

      </ImageOverlay>
    </Card>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 150,
  },
  image: {
    ...StyleSheet.absoluteFillObject,
    height: 150,
    paddingVertical: 24,
    paddingHorizontal: 16,
  },
  level: {
    zIndex: 1,
  },
  title: {
    zIndex: 1,
  },
  durationButton: {
    position: 'absolute',
    left: 16,
    bottom: 16,
    borderRadius: 16,
    paddingHorizontal: 0,
  },
});
