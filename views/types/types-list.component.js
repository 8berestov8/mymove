import React from 'react'
import {StyleSheet,} from 'react-native'
import {List, Text} from '@ui-kitten/components'
import {TypeCard} from './extra/type-card.component'
import {Type} from './extra/data'

import {changeType} from '../../redux/actions/userActions'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

const mapStateToProps = (state) => {
  const {user} = state
  return {user}
}

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    changeType,
  }, dispatch)
)

export const TypesListScreen = connect(mapStateToProps, mapDispatchToProps)(({navigation, changeType}) => {
  const types = [
    Type.client(() => {
      navigation.navigate('SignIn')
      changeType('client')
    }),
    Type.teacher(() => {
      navigation.navigate('SignIn')
      changeType('teacher')
    }),
    Type.owner(() => {
      navigation.navigate('SignIn')
      changeType('owner')
    }),
  ]


  const displayTypes = types//.filter(type => type.level === route.name);

  const renderHeader = () => (
    <React.Fragment>
      <Text
        style={styles.headerTitle}
        appearance='hint'>
        ВЫБЕРИТЕ ТИП
      </Text>
    </React.Fragment>
  )

  const renderVerticalTypeItem = (info) => (
    <TypeCard
      onPress={() => {
        info.item.onPress()
      }}
      style={styles.verticalItem}
      type={info.item}
    />
  )

  return (
    <List
      contentContainerStyle={styles.list}
      data={displayTypes}
      renderItem={renderVerticalTypeItem}
      ListHeaderComponent={renderHeader}
    />
  )
})

const styles = StyleSheet.create({
  list: {
    paddingVertical: 24,
  },
  headerTitle: {
    marginHorizontal: 16,
  },
  horizontalList: {
    marginVertical: 16,
    paddingHorizontal: 8,
  },
  verticalItem: {
    marginVertical: 8,
    marginHorizontal: 16,
  },
  horizontalItem: {
    width: 256,
    marginHorizontal: 8,
  },
})
