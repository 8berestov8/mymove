import React from 'react';
import { StyleSheet, View } from 'react-native';
import {
	Button,
	Card,
	List,
	Text,
	OverflowMenu,
	MenuItem,
} from '@ui-kitten/components';
import { ProfileSocial } from './profile-social.component';
import { MoreHorizontalIcon } from '../../../components/icons';
import API from '../../../API';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setTeachers } from '../../../redux/actions/teachersActions';
import ModalDeletePopup from '../../../components/modal-delete';
import TextNotFound from '../../../components/text-not-found';
import { deleteTeacher } from '../../../API/Teachers';

const mapStateToProps = (state) => {
	const { user, schools, teachers } = state;
	return { user, schools, teachers };
};

const mapDispatchToProps = (dispatch) =>
	bindActionCreators(
		{
			setTeachers,
		},
		dispatch
	);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)((props) => {
	const { navigation, user, teachers, setTeachers, loading } = props;
	const idSchool = props.school_id;
	const [idTeacher, setIdTeacher] = React.useState(0);
	const [visibleModalDelete, setVisibleModalDelete] = React.useState(false);
	const ModalDelete = () => (
		<ModalDeletePopup
			visible={visibleModalDelete}
			setVisible={(v) => setVisibleModalDelete(v)}
			confirmDelete={() => {
				deleteTeacher({ user, idSchool, idTeacher }).then((data) => {
					teachers.list[idSchool] = teachers.list[idSchool].filter((v) => {
						return v.id != idTeacher;
					});
					setTeachers(idSchool, teachers.list[idSchool]);
					setVisibleModalDelete(false);
				});
			}}
		/>
	);
	const renderteacherHeader = (teacher) => {
		const [visible, setVisible] = React.useState(false);

		const renderToggleButton = () => (
			<Button
				style={styles.iconButton}
				appearance="ghost"
				status="basic"
				accessoryLeft={MoreHorizontalIcon}
				onPress={() => setVisible(true)}
			/>
		);

		const onItemSelect = (index) => {
			setVisible(false);
		};
		return (
			<View style={styles.teacherHeader}>
				<View style={styles.teacherAuthorContainer}>
					<Text category="s1">
						{teacher.lastname} {teacher.firstname} {teacher.patronymic}
					</Text>
					<Text appearance="hint" category="c1">
						{teacher.phone}
					</Text>
				</View>
				<OverflowMenu
					anchor={renderToggleButton}
					backdropStyle={styles.backdrop}
					visible={visible}
					onSelect={onItemSelect}
					onBackdropPress={() => setVisible(false)}
				>
					<MenuItem
						title="Редактировать"
						onPress={() => {
							setVisible(false);
							props.navigation.navigate('TeacherForm', {
								school_id: idSchool,
								id: teacher.id,
							});
						}}
					/>
					<MenuItem
						title="Удалить"
						onPress={() => {
							setIdTeacher(teacher.id);
							setVisible(false);
							setVisibleModalDelete(true);
						}}
					/>
				</OverflowMenu>
			</View>
		);
	};

	const renderItem = (info) => (
		<Card
			style={styles.teacherItem}
			header={() => renderteacherHeader(info.item)}
		>
			<View style={styles.profileSocialsContainer}>
				<ProfileSocial
					hint="Ученики"
					value={0}
					onPress={() => {
						alert(66);
					}}
				/>
				<Button
					onPress={() => {
						navigation.navigate('WorkingTimeList');
					}}
				>
					Рабочее время
				</Button>
			</View>
		</Card>
	);
	return (
		<>
			<List
				{...props}
				renderItem={renderItem}
				ListEmptyComponent={
					!loading && <TextNotFound text={'Тренера не добавлены'} />
				}
			/>
			<ModalDelete />
		</>
	);
});

const styles = StyleSheet.create({
	teacherItem: {
		marginVertical: 4,
	},
	teacherHeader: {
		flexDirection: 'row',
		padding: 16,
	},
	teacherAuthorContainer: {
		flex: 1,
	},
	teacherReactionsContainer: {
		flexDirection: 'row',
		marginTop: 8,
		marginHorizontal: -8,
		marginVertical: -8,
	},
	iconButton: {
		paddingHorizontal: 0,
	},
	backdrop: {
		backgroundColor: 'rgba(0, 0, 0, 0.5)',
	},
	profileSocialsContainer: {
		flexDirection: 'row',
		justifyContent: 'space-evenly',
	},
	profileParametersSection: {
		flexDirection: 'row',
		marginVertical: 16,
		marginHorizontal: 8,
	},
	profileParameter: {
		flex: 1,
		marginHorizontal: 8,
	},
	backdrop: {
		backgroundColor: 'rgba(0, 0, 0, 0.5)',
	},
	buttons: {
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'center',
		marginVertical: 8,
	},
	textConfirm: {
		fontSize: 18,
		fontWeight: 'bold',
	},
	inputDeleted: {
		marginVertical: 8,
	},
});
