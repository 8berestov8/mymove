import React from 'react';
import { StyleSheet, View, RefreshControl } from 'react-native';
import { Layout } from '@ui-kitten/components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import API from '../../API';
import TeachersListComponent from './extra/teachers-list.component';
import { setTeachers } from '../../redux/actions/teachersActions';
import ModalErrorPopup from '../../components/modal-error';
import TopNavigation from '../../components/top-navigation';
import { getTeachers } from '../../API/Teachers';
import Loading from '../../components/loading';

const mapStateToProps = (state) => {
	const { user, teachers } = state;
	return { user, teachers };
};

const mapDispatchToProps = (dispatch) =>
	bindActionCreators(
		{
			setTeachers,
		},
		dispatch
	);

class TeachersList extends React.Component {
	constructor(props) {
		const idSchool = props.route.params.school_id;
		super(props);
		this.state = {
			refreshing: false,
			error: '',
			loading: true,
		};
		const { user, setTeachers } = props;
		this.loadData = () => {
			return new Promise((resolve, reject) => {
				getTeachers({ user, idSchool })
					.then((data) => {
						setTeachers(idSchool, data);
						resolve(res.data);
					})
					.catch((e) => {
						reject(e);
					});
			});
		};
		this.loadData()
			.then((res) => {
				this.setState({ loading: false });
			})
			.catch((err) => {
				this.setState({ loading: false });
				this.setState({ error: err });
			});
	}
	render() {
		const props = this.props;
		const idSchool = props.route.params.school_id;
		const { navigation, teachers } = props;
		const onRefresh = () => {
			this.setState({ refreshing: true });
			this.loadData()
				.then(() => {
					this.setState({ refreshing: false });
				})
				.catch((err) => {
					this.setState({ refreshing: false });
					this.setState({ error: err });
				});
		};
		return (
			<Layout style={styles.container} level="2">
				<ModalErrorPopup
					error={this.state.error}
					setError={(err) => this.setState({ error: err })}
				/>
				<TopNavigation
					onPressPlus={() => {
						navigation.navigate('TeacherForm', {
							school_id: idSchool,
						});
					}}
					title="Тренера"
				/>
				<View style={styles.body}>
					{teachers.list == 0 ? (
						<Loading />
					) : (
						<TeachersListComponent
							style={styles.list}
							loading={this.state.loading}
							refreshControl={
								<RefreshControl
									refreshing={this.state.refreshing}
									onRefresh={onRefresh}
								/>
							}
							data={teachers.list[idSchool] ? teachers.list[idSchool] : []}
							school_id={idSchool}
							{...props}
						/>
					)}
				</View>
			</Layout>
		);
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(TeachersList);

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	header: {
		padding: 16,
		flexDirection: 'row',
		justifyContent: 'center',
	},
	list: {
		flex: 1,
	},
	body: {
		flex: 1,
		paddingHorizontal: 16,
		paddingTop: 16,
	},
	profileContainer: {
		flexDirection: 'row',
	},
	profileAvatar: {
		marginHorizontal: 8,
	},
	profileDetailsContainer: {
		flex: 1,
		marginHorizontal: 8,
	},
	rateBar: {
		marginTop: 24,
	},
	followButton: {},
	profileSocialsContainer: {
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		marginTop: 24,
	},
	profileSectionsDivider: {
		width: 1,
		height: '100%',
		marginHorizontal: 8,
	},
	profileDescription: {
		marginTop: 24,
		marginBottom: 8,
	},
	profileParametersSection: {
		flexDirection: 'row',
		marginVertical: 16,
		marginHorizontal: 8,
	},
	profileParameter: {
		flex: 1,
		marginHorizontal: 8,
	},
});
