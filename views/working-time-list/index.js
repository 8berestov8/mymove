import React from 'react';
import { View } from 'react-native';
import {
	Button,
	Input,
	StyleService,
	useStyleSheet,
	Modal,
	Layout,
	Card,
	Datepicker,
	NativeDateService,
	List,
	Text,
	OverflowMenu,
	MenuItem,
	IndexPath,
	Select,
	SelectItem,
} from '@ui-kitten/components';
import TopNavigation from '../../components/top-navigation';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

const mapStateToProps = (state) => {
	const { user, services, teachers } = state;
	return { user, services, teachers };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(({ navigation, route, user }) => {
	return (
		<Layout level="2">
			<TopNavigation
				onPressPlus={() => {
					navigation.navigate('WorkingTimeForm');
				}}
				title="Рабочее время"
			/>
			<View></View>
		</Layout>
	);
});
