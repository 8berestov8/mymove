import React from 'react';
import { View } from 'react-native';
import {
	Button,
	Input,
	StyleService,
	useStyleSheet,
	Modal,
	Layout,
	Card,
	Datepicker,
	NativeDateService,
	List,
	Text,
	OverflowMenu,
	MenuItem,
	IndexPath,
	Select,
	SelectItem,
} from '@ui-kitten/components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setServices } from '../../redux/actions/servicesActions';
import { setTeachers } from '../../redux/actions/teachersActions';
import TopNavigation from '../../components/top-navigation';
import { calendarLang } from '../../i18n/';
import {
	addFinance,
	getFinances,
	removeFinance,
	updateFinance,
} from '../../API/Finances';
import moment from 'moment';
import { MoreHorizontalIcon } from '../../components/icons';
import Loading from '../../components/loading';

const mapStateToProps = (state) => {
	const { user, services, teachers } = state;
	return { user, services, teachers };
};

const ModalForm = (props) => {
	const { styles, id, visible, user, item, idSchool, finances, setFinances } =
		props;
	const listStatuses = [
		{ key: 'new', name: 'Новый' },
		{ key: 'confirm', name: 'Подвержден' },
		{ key: 'canceled', name: 'Отменен' },
	];

	const [date, setDate] = React.useState(moment(item.date).toDate());
	const [sum, setSum] = React.useState(item.sum ? item.sum : '');
	const [description, setDescription] = React.useState(
		item.description ? item.description : ''
	);

	const [statusSum, setStatusSum] = React.useState('basic');
	const [selectedIndex, setSelectedIndex] = React.useState(new IndexPath(0));

	const localeDateService = new NativeDateService('ru', {
		format: 'DD.MM.YYYY',
		i18n: calendarLang,
		startDayOfWeek: 1,
	});
	const setVisible = (v) => {
		if (!v) {
			props.onChangeItem({});
		}
		props.onChangeVisible(v);
	};
	const onChangeButtonPress = () => {
		setStatusSum('basic');
		let hasErrors = false;
		if (sum.length == 0) {
			hasErrors = true;
			setStatusSum('danger');
		}
		if (!hasErrors) {
			addFinance({
				user,
				idSchool,
				dataFinance: {
					type: item.type ? item.type : props.type,
					sum,
					date: moment(date).format('YYYY-MM-DD'),
					description,
					status: listStatuses[selectedIndex.row].key,
				},
			})
				.then((data) => {
					setVisible(false);
					if (!item.id) {
						finances.unshift(data);
					} else {
						const finances = finances.map((o) => {
							if (o.id === data.id) {
								return data;
							}
							return o;
						});
					}
					setFinances(finances);
				})
				.catch((e) => {});
		}
	};
	return (
		<Modal
			visible={visible}
			backdropStyle={styles.backdrop}
			onBackdropPress={() => setVisible(false)}
			style={styles.modal}
		>
			<Card style={styles.card} disabled={true}>
				<Select
					label="Статус"
					style={styles.input}
					selectedIndex={selectedIndex}
					onSelect={(index) => setSelectedIndex(index)}
					value={listStatuses[selectedIndex.row].name}
				>
					{listStatuses.map((v) => (
						<SelectItem key={`key_list_status_${v.key}`} title={v.name} />
					))}
				</Select>
				<Datepicker
					style={styles.input}
					label="Дата"
					placeholder=""
					date={date}
					onSelect={(nextDate) => setDate(nextDate)}
					dateService={localeDateService}
					size="large"
					value={date}
					onChangeText={(val) => {
						setDate(val);
					}}
				/>

				<Input
					label="Сумма"
					style={styles.input}
					size="large"
					status={statusSum}
					value={sum}
					keyboardType="numeric"
					onChangeText={(val) => {
						setSum(val);
					}}
				/>
				<Input
					multiline={true}
					textStyle={{ minHeight: 64 }}
					label="Описание"
					style={styles.input}
					size="large"
					value={description}
					onChangeText={(val) => {
						setDescription(val);
					}}
				/>

				<Button onPress={onChangeButtonPress} style={styles.button}>
					{!item.id ? 'ДОБАВИТЬ' : 'СОХРАНИТЬ'}
				</Button>
			</Card>
		</Modal>
	);
};

const mapDispatchToProps = (dispatch) =>
	bindActionCreators(
		{
			setServices,
			setTeachers,
		},
		dispatch
	);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(({ navigation, route, user }) => {
	const idSchool = route.params.school_id;
	const styles = useStyleSheet(themedStyles);
	const [finances, setFinances] = React.useState([]);
	const [visible, setVisible] = React.useState(false);
	const [updateItem, setUpdateItem] = React.useState({});
	const [type, setType] = React.useState('');

	const openIncome = () => {
		setType('income');
		setVisible(true);
	};
	const openCosts = () => {
		setType('expenditure');
		setVisible(true);
	};

	React.useEffect(() => {
		getFinances({ user, idSchool }).then((data) => {
			setFinances(data);
		});
	}, []);

	const renderHeader = (item) => {
		const [visibleToogle, setVisibleToogle] = React.useState(false);
		const renderToggleButton = () => (
			<Button
				style={styles.iconButton}
				appearance="ghost"
				status="basic"
				accessoryLeft={MoreHorizontalIcon}
				onPress={() => setVisibleToogle(true)}
			/>
		);

		const removeItem = (item) => {
			removeFinance({
				user,
				idSchool,
				idFinance: item.id,
			})
				.then((data) => {
					setVisibleToogle(false);
					const newArr = finances.filter((n) => n.id !== item.id);
					setFinances(newArr);
				})
				.catch((e) => {});
		};

		const updateItem = (item) => {
			setVisible(true);
			setUpdateItem(item);
			console.log(user, idSchool);
		};

		return (
			<View style={styles.financeItemHeader}>
				<View>
					{item.type == 'income' && (
						<Text style={styles.income}>+{item.sum}₽</Text>
					)}

					{item.type == 'expenditure' && (
						<Text style={styles.expenditure}>-{item.sum}₽</Text>
					)}

					<Text style={styles.date} appearance="hint" category="c1">
						{moment(item.date).format('DD.MM.YYYY ')}
					</Text>
				</View>
				<OverflowMenu
					anchor={renderToggleButton}
					backdropStyle={styles.backdrop}
					visible={visibleToogle}
					onBackdropPress={() => setVisibleToogle(false)}
				>
					<MenuItem title="Редактировать" onPress={() => updateItem(item)} />
					<MenuItem
						title="Удалить"
						onPress={() => {
							removeItem(item);
						}}
					/>
				</OverflowMenu>
			</View>
		);
	};

	const renderItem = (info) => {
		return (
			<View style={styles.body}>
				<Card style={styles.list} header={() => renderHeader(info.item)}>
					<Text style={styles.description}>
						Описание: {info.item.description}
					</Text>
				</Card>
			</View>
		);
	};

	return (
		<Layout style={styles.container} level="2">
			<TopNavigation title="Финансы" />
			<View style={styles.headerButtons}>
				<Button
					appearance="outline"
					status="primary"
					style={styles.followButton}
					onPress={openCosts}
				>
					Отметить расход
				</Button>
				<Button
					status="warning"
					appearance="outline"
					style={styles.followButton}
					onPress={openIncome}
				>
					Добавить доход
				</Button>
			</View>
			{finances.length == 0 ? (
				<Loading />
			) : (
				<List data={finances} renderItem={renderItem} />
			)}

			<ModalForm
				key={`modal_form_finance_${updateItem?.id}`}
				type={type}
				styles={styles}
				item={updateItem}
				visible={visible}
				finances={finances}
				setFinances={setFinances}
				user={user}
				idSchool={idSchool}
				onChangeVisible={(v) => setVisible(v)}
				onChangeItem={(v) => setUpdateItem(v)}
			></ModalForm>
		</Layout>
	);
});

const themedStyles = StyleService.create({
	container: {
		flex: 1,
	},
	body: {
		flex: 1,
		paddingHorizontal: 16,
		paddingTop: 16,
	},
	list: {
		flex: 1,
	},
	headerButtons: {
		flexDirection: 'row',
		justifyContent: 'center',
		padding: 16,
	},

	followButton: {
		margin: 2,
	},
	modal: {
		width: '90%',
	},
	backdrop: {
		backgroundColor: 'rgba(0, 0, 0, 0.5)',
	},
	card: {
		color: '#fffff',
	},
	input: {
		margin: 10,
	},
	button: {
		margin: 10,
	},
	income: {
		color: 'green',
		fontSize: 20,
		padding: 10,
		fontWeight: 'bold',
	},
	expenditure: {
		color: 'red',
		fontSize: 20,
		padding: 10,
		fontWeight: 'bold',
	},
	description: {
		fontStyle: 'italic',
	},

	iconButton: {
		paddingHorizontal: 0,
		justifyContent: 'center',
	},
	financeItemHeader: {
		flexDirection: 'row',
		padding: 16,
		justifyContent: 'space-between',
	},
});
