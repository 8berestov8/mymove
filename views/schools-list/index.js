import React from 'react';
import SchoolsList from './extra/schools-list.component';
import ModalErrorPopup from '../../components/modal-error';
import { getProfile } from '../../API/Users';
import { getSchools } from '../../API/Schools';
import { StyleSheet, RefreshControl, View, Platform } from 'react-native';
import {
  Avatar,
  Button,
  Text,
  Layout,
  List,
  Card,
} from '@ui-kitten/components';
// import rates from '../rates/assets/rates';
import { changeUser, changeSession } from '../../redux/actions/userActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setSchools } from '../../redux/actions/schoolsActions';
import { setTeachers } from '../../redux/actions/teachersActions';
import { ImageOverlay } from './extra/image-overlay.component';
import { Type } from '../types/extra/data';
import QrCode from '../../components/qr-code';
// import * as RNIap from 'react-native-iap';
import { checkRate } from '../../API/Rates';

const mapStateToProps = (state) => {
  const { user, schools } = state;
  return { user, schools };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      changeUser,
      setSchools,
      setTeachers,
      changeSession,
    },
    dispatch
  );

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      error: '',
      loading: true,
    };
    const { changeUser, setSchools, changeSession, navigation, user } = props;
    if (Platform.OS === 'ios') {
      // RNIap.initConnection().then(() => {
      // 	RNIap.flushFailedPurchasesCachedAsPendingAndroid()
      // 		.catch(() => {})
      // 		.then(() => {
      // 			RNIap.purchaseUpdatedListener((purchase) => {
      // 				const receipt = purchase.transactionReceipt;
      // 				if (receipt) {
      // 					checkRate(purchase, user.sessionId).then((data) => {
      // 						if (data == 1) {
      // 							RNIap.finishTransaction(purchase, false);
      // 						}
      // 					});
      // 				}
      // 			});
      // 			RNIap.purchaseErrorListener((error) => {
      // 				console.warn('purchaseErrorListener', error);
      // 			});
      // 		});
      // });
    }
    this.loadData = (user) => {
      return new Promise((resolve, reject) => {
        getSchools({ user })
          .then((data) => {
            setSchools(data);
            resolve(data);
          })
          .catch((err) => {
            reject(err);
          });
      });
    };
    getProfile({ user })
      .then((data) => {
        changeUser(data);
        this.setState({ loading: true });
        this.loadData({ ...user, ...data })
          .then((res) => {
            this.setState({ loading: false });
          })
          .catch((err) => {
            this.setState({ error: err });
          });
      })
      .catch((err) => {
        if (err.response && err.response.status == 403) {
          navigation.navigate('Auth');
          changeSession('');
        } else {
          this.setState({ error: err });
        }
      });
  }
  render() {
    const props = this.props;
    const { navigation, user, schools } = props;

    const onRefresh = () => {
      this.setState({ refreshing: true });
      this.loadData()
        .then(() => {
          this.setState({ refreshing: false });
        })
        .catch((err) => {
          this.setState({ refreshing: false });
          this.setState({ error: err });
        });
    };

    const QRCODE_URL =
      'https://izhevsk.hh.ru/resume/4936a2a9ff011832600039ed1f517472554f6a';
    const type = 'open_client';
    // const profileLink = `${QRCODE_URL}/${user.id}/${user.public_key}/${type}`
    const profileLink = `${QRCODE_URL}`;
    const renderHorizontalItem = (info) => (
      <Card
        onPress={info.item.onPress}
        style={[styles.container, styles.horizontalItem]}
      >
        <Text style={styles.level} status="control">
          {info.item.title}
        </Text>
      </Card>
    );
    return (
      <Layout style={styles.container} level="2">
        <ModalErrorPopup
          error={this.state.error}
          setError={(err) => this.setState({ error: err })}
        />

        <ImageOverlay
          style={styles.header}
          source={
            typeof Type[user.type] != 'undefined'
              ? Type[user.type]().image
              : null
          }
        >
          <Avatar style={styles.profileAvatar} source={props.user.avatar} />
          <Text style={styles.profileName} category="h5" status="control">
            {user.lastname} {user.firstname} {user.patronymic}
          </Text>
        </ImageOverlay>
        <View style={styles.body}>
          {user.type === 'owner' && (
            <Button
              style={styles.button}
              appearance="outline"
              status="success"
              onPress={() => {
                navigation.navigate('Rates');
              }}
            >
              <Text>Тариф: user.rate.type (0 пользователей из 15)</Text>
            </Button>
          )}
          <List
            contentContainerStyle={styles.horizontalList}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={[
              {
                title: 'Поиск по ученикам',
                onPress: () => {
                  navigation.navigate('Search');
                },
              },
              {
                title: 'Сканирование штрих-кода',
                onPress: () => {
                  navigation.navigate('ScanCode');
                },
              },
            ]}
            renderItem={renderHorizontalItem}
          />
          <View style={styles.sectionLabel}>
            <Text b category="h6">
              Школы/студии/секции
            </Text>
            {user.type == 'owner' && (
              <Button
                size="small"
                appearance="outline"
                onPress={() => {
                  navigation.navigate('SchoolAdd');
                }}
              >
                Добавить
              </Button>
            )}
          </View>
          <View>
            {user.type === 'client' && false && <QrCode link={profileLink} />}
          </View>

          <SchoolsList
            style={styles.list}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={onRefresh}
              />
            }
            loading={this.state.loading}
            data={schools.list}
            {...props}
          />
        </View>
      </Layout>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Profile);

const styles = StyleSheet.create({
  list: {},
  container: {
    flex: 1,
  },
  header: {
    paddingVertical: 24,
    alignItems: 'center',
  },
  body: {
    marginTop: 24,
    marginHorizontal: 24,
  },
  profileName: {
    zIndex: 1,
    textAlign: 'center',
  },
  profileAvatar: {
    width: 62,
    height: 62,
    borderRadius: 62,
    marginVertical: 16,
  },
  sectionLabel: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    fontSize: 18,
    marginTop: 24,
    marginBottom: 8,
  },
  horizontalList: {
    marginVertical: 16,
  },
  horizontalItem: {
    width: 155,
    height: 75,
    marginRight: 8,
    backgroundColor: '#9c00c1',
    textAlign: 'center',
  },
});
