import React from 'react';
import ModalDeletePopup from '../../../components/modal-delete';
import { deleteSchool } from '../../../API/Schools';
import { StyleSheet, View } from 'react-native';
import {
  Button,
  Card,
  List,
  Text,
  OverflowMenu,
  MenuItem,
} from '@ui-kitten/components';
import { ProfileSocial } from './profile-social.component';
import { MoreHorizontalIcon } from '../../../components/icons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setSchools } from '../../../redux/actions/schoolsActions';
import TextNotFound from '../../../components/text-not-found';
import { getDeclination } from '../../../helpers/string';

const mapStateToProps = (state) => {
  const { user, schools, teachers, students, services } = state;
  return { user, schools, teachers, students, services };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      setSchools,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)((props) => {
  const {
    user,
    schools,
    teachers,
    students,
    setSchools,
    services,
    loading,
  } = props;
  const [idSchool, setIdSchool] = React.useState(0);

  const [visibleModalDelete, setVisibleModalDelete] = React.useState(false);
  const ModalDelete = () => (
    <ModalDeletePopup
      visible={visibleModalDelete}
      setVisible={(v) => setVisibleModalDelete(v)}
      confirmDelete={() => {
        deleteSchool({
          idSchool,
          user,
        })
          .then((data) => {
            schools.list = schools.list.filter((v) => {
              return v.id != idSchool;
            });
            setSchools(schools.list);
            //setLoading(false);
            setVisibleModalDelete(false);
          })
          .catch((err) => {
            //setLoading(false);
          });
      }}
    />
  );
  const renderSchoolHeader = (school) => {
    const [visible, setVisible] = React.useState(false);
    const renderToggleButton = () => (
      <Button
        style={styles.iconButton}
        appearance="ghost"
        status="basic"
        accessoryLeft={MoreHorizontalIcon}
        onPress={() => setVisible(true)}
      />
    );
    const onItemSelect = (index) => {
      setVisible(false);
    };
    return (
      <View style={styles.schoolHeader}>
        <View style={styles.schoolAuthorContainer}>
          <Text category="s1">{school.name}</Text>
          <Text appearance="hint" category="c1">
            {school.contact_phone}
          </Text>
        </View>
        {user.type == 'owner' && (
          <OverflowMenu
            anchor={renderToggleButton}
            backdropStyle={styles.backdrop}
            visible={visible}
            onSelect={onItemSelect}
            onBackdropPress={() => setVisible(false)}
          >
            <MenuItem
              title="Редактировать"
              onPress={() => {
                setVisible(false);
                props.navigation.navigate('SchoolEdit', {
                  id: school.id,
                });
              }}
            />
            <MenuItem
              title="Удалить"
              onPress={() => {
                setIdSchool(school.id);
                setVisible(false);
                setVisibleModalDelete(true);
              }}
            />
          </OverflowMenu>
        )}
      </View>
    );
  };
  const ProfileServices = ({ info }) => (
    <ProfileSocial
      hint={getDeclination(
        typeof services.list[info.item.id] !== 'undefined'
          ? services.list[info.item.id].length
          : info.item.services_count,
        ['Услуга', 'Услуги', 'Услуг']
      )}
      value={
        typeof services.list[info.item.id] !== 'undefined'
          ? services.list[info.item.id].length
          : info.item.services_count
      }
      onPress={() => {
        props.navigation.navigate('ServicesList', {
          school_id: info.item.id,
        });
      }}
    />
  );
  const ProfileTeachers = ({ info }) => (
    <ProfileSocial
      hint={getDeclination(
        typeof teachers.list[info.item.id] !== 'undefined'
          ? teachers.list[info.item.id].length
          : info.item.teachers_count,
        ['Тренер', 'Тренера', 'Тренеров']
      )}
      value={
        typeof teachers.list[info.item.id] !== 'undefined'
          ? teachers.list[info.item.id].length
          : info.item.teachers_count
      }
      onPress={() => {
        props.navigation.navigate('TeachersList', {
          school_id: info.item.id,
        });
      }}
    />
  );
  const ProfileStudents = ({ info }) => (
    <ProfileSocial
      hint={getDeclination(
        typeof students.list[info.item.id] !== 'undefined'
          ? students.list[info.item.id].length
          : info.item.students_count,
        ['Ученик', 'Ученика', 'Учеников']
      )}
      value={
        typeof students.list[info.item.id] !== 'undefined'
          ? students.list[info.item.id].length
          : info.item.students_count
      }
      onPress={() => {
        props.navigation.navigate('StudentsList', {
          school_id: info.item.id,
        });
      }}
    />
  );

  const ProfileFinance = ({ info }) => (
    <ProfileSocial
      hint={'Финансы'}
      value={0}
      // 	typeof students.list[info.item.id] !== 'undefined'
      // 		? students.list[info.item.id].length
      // 		: info.item.students_count

      onPress={() => {
        props.navigation.navigate('Finance', { school_id: info.item.id });
      }}
    />
  );

  const ProfileLessons = ({ info }) => (
    <ProfileSocial
      hint="Занятий"
      value={
        info.item.student.used_lessons +
        ' из ' +
        (info.item.student.infinity_lessons
          ? '~'
          : info.item.student.count_lessons)
      }
      onPress={() => {
        props.navigation.navigate('LessonsPlusList', {
          school_id: info.item.id,
          student_id: info.item.student.id,
        });
      }}
    />
  );
  const renderItem = (info) => (
    <Card
      style={styles.schoolItem}
      header={() => renderSchoolHeader(info.item)}
    >
      <View style={styles.profileSocialsContainer}>
        {user.type == 'owner' && <ProfileServices info={info} />}
        {user.type == 'owner' && <ProfileTeachers info={info} />}
        {user.type == 'owner' && <ProfileStudents info={info} />}
        {user.type == 'owner' && <ProfileFinance info={info} />}
        {user.type == 'teacher' && <ProfileStudents info={info} />}
        {user.type == 'client' && <ProfileLessons info={info} />}
      </View>
    </Card>
  );

  return (
    <>
      <List
        {...props}
        renderItem={renderItem}
        ListEmptyComponent={
          !loading && (
            <TextNotFound
              text={
                user.type == 'owner'
                  ? 'Школа/студия/секция не добавлена'
                  : user.type == 'teacher'
                  ? `Вы не относитесь ни к одной школе/студии/секции как тренер
Потяните вниз, чтобы обновить информацию`
                  : user.type == 'client'
                  ? `Вы не относитесь ни к одной школе/студии/секции как ученик 
Потяните вниз, чтобы обновить информацию`
                  : ''
              }
            />
          )
        }
      />
      <ModalDelete />
    </>
  );
});

const styles = StyleSheet.create({
  schoolItem: {
    marginVertical: 4,
  },
  schoolHeader: {
    flexDirection: 'row',
    padding: 16,
  },
  schoolAuthorContainer: {
    flex: 1,
  },
  textConfirm: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  inputDeleted: {
    marginVertical: 8,
  },
  buttons: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 8,
  },
  schoolReactionsContainer: {
    flexDirection: 'row',
    marginTop: 8,
    marginHorizontal: -8,
    marginVertical: -8,
  },
  iconButton: {
    paddingHorizontal: 0,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  profileSocialsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  profileParametersSection: {
    flexDirection: 'row',
    marginVertical: 16,
    marginHorizontal: 8,
  },
  profileParameter: {
    flex: 1,
    marginHorizontal: 8,
  },
});
