import React, { useState } from 'react';
import { editSchool } from '../../API/Schools';
import ModalErrorPopup from '../../components/modal-error';
import { View, Text, Platform, TouchableWithoutFeedback } from 'react-native';
import {
  Button,
  Divider,
  Input,
  Layout,
  StyleService,
  useStyleSheet,
  Icon,
  Spinner,
  Autocomplete,
  AutocompleteItem,
} from '@ui-kitten/components';
import { KeyboardAvoidingView } from '../../components/3rd-party';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setSchools } from '../../redux/actions/schoolsActions';
import Geocoder from '@timwangdev/react-native-geocoder';
import { Avatar } from '@ui-kitten/components';
import MapView, {
  PROVIDER_GOOGLE,
  Marker,
} from '../../_node_modules/react-native-maps';
import * as ImagePicker from 'expo-image-picker';
import TopNavigation from '../../components/top-navigation';

const mapStateToProps = (state) => {
  const { user, schools } = state;
  return { user, schools };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      setSchools,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(({ navigation, route, schools, user, setSchools }) => {
  const school = schools.list.find((v) => v.id == route.params.id);
  const styles = useStyleSheet(themedStyles);

  const [name, setName] = React.useState(school.name);
  const [contactPhone, setContactPhone] = React.useState(school.contact_phone);

  const [statusName, setStatusName] = React.useState('basic');
  const [statusContactPhone, setStatusContactPhone] = React.useState('basic');

  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState('');

  const [address, setAddress] = React.useState(school.address);
  const [locations, setLocations] = React.useState([]);

  const [position, setPosition] = React.useState({
    lat: school.lat,
    lng: school.lng,
  });

  const onSaveButtonPress = () => {
    let hasErrors = false;
    setStatusName('basic');
    setStatusContactPhone('basic');
    if (name.length === 0) {
      setStatusName('danger');
      hasErrors = true;
    }
    if (contactPhone.length === 0) {
      setStatusContactPhone('danger');
      hasErrors = true;
    }
    if (!hasErrors) {
      setLoading(true);
      const data = new FormData();
      data.append('name', name);
      data.append('contact_phone', contactPhone);
      data.append('address', address);
      data.append('lat', position.lat);
      data.append('lng', position.lng);
      if (logo) {
        const localUri = logo.uri;
        const filename = localUri.split('/').pop();
        const match = /\.(\w+)$/.exec(filename);
        const type = match ? `image/${match[1]}` : `image`;
        data.append('logo', { uri: localUri, name: filename, type });
      }
      editSchool({
        user,
        schoolId: school.id,
        dataSchool: data,
      })
        .then((data) => {
          setLoading(false);
          schools.list = schools.list.map((v) => {
            return v.id == school.id ? data : v;
          });
          setSchools(schools.list);
          navigation.navigate('SchoolsList');
        })
        .catch((err) => {
          setLoading(false);
          setError(err);
        });
    }
  };

  const [logo, setLogo] = useState(null);

  const selectAvatar = async () => {
    const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
    if (status !== 'granted') {
      setError('У MyMove нет доступа к файлам. Разрешите доступ.');
    } else {
      const result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.photo,
        allowsEditing: true,
        aspect: [5, 5],
        quality: 1,
      });
      if (!result.cancelled) {
        setLogo(result);
      }
    }
  };

  const renderLocation = (item, index) => (
    <AutocompleteItem key={index} title={item.formattedAddress} />
  );
  const onSelectLocation = (index) => {
    setPosition(locations[index].position);
    setAddress(locations[index].formattedAddress);
  };
  const clearAddress = () => {
    setAddress('');
    setPosition({
      lat: null,
      lng: null,
    });
  };
  const renderCloseIcon = (props) => (
    <TouchableWithoutFeedback onPress={clearAddress}>
      <Icon {...props} name="close" />
    </TouchableWithoutFeedback>
  );
  const searchAddress = async (Address) => {
    if (Address.length > 0) {
      try {
        const result = await Geocoder.geocodeAddress(Address, {
          apiKey: 'AIzaSyAaKRz9tQY9pEmFtUbY95sakO1T4n7ABtw',
          locale: 'ru',
          fallbackToGoogle: true,
        });
        setLocations(result);
      } catch (err) {
        console.log(err);
      }
    } else {
      setLocations([]);
    }
  };

  let indexTimeout;

  return (
    <Layout style={styles.container} level="2">
      <TopNavigation title="Редактирование школы/студии/секции" />
      <KeyboardAvoidingView style={styles.container}>
        {false && loading && (
          <View style={styles.loadingContainer}>
            <Spinner size="small" />
          </View>
        )}
        <ModalErrorPopup error={error} setError={(err) => setError({ err })} />
        <Layout style={styles.form} level="1">
          <Input
            style={styles.input}
            label="Название"
            value={name}
            onChangeText={setName}
            status={statusName}
            size="large"
          />
          <Input
            style={styles.input}
            label="Контактный телефон"
            value={contactPhone}
            onChangeText={setContactPhone}
            status={statusContactPhone}
            size="large"
          />
          <Text style={styles.logoLabel}>Логотип</Text>

          <View style={styles.logoContainer}>
            <Avatar
              source={logo ? logo : require('../../assets/placeholder.png')}
              style={styles.logo}
            />
            <Button
              style={styles.selectLogo}
              onPress={selectAvatar}
              appearance="ghost"
            >
              Выбрать изображение
            </Button>
          </View>

          <Autocomplete
            style={styles.input}
            onChangeText={setAddress}
            value={address}
            label="Адрес школы"
            size="large"
            blurOnSubmit={true}
            onChangeText={(Address) => {
              if (typeof indexTimeout !== 'undefined') {
                clearTimeout(indexTimeout);
              }
              indexTimeout = setTimeout(() => {
                searchAddress(Address);
              }, 100);
            }}
            placeholder="Москва Тверская 122"
            onSelect={onSelectLocation}
            accessoryRight={renderCloseIcon}
          >
            {locations.length == 0 ? (
              <AutocompleteItem title="Введите адрес" />
            ) : (
              locations.map(renderLocation)
            )}
          </Autocomplete>

          <View style={styles.mapContainer}>
            {Platform.OS !== 'web' && (
              <MapView
                style={styles.map}
                provider={PROVIDER_GOOGLE}
                customMapStyle={[]}
                region={{
                  latitude:position.lat != null ? Number(position.lat) : 55.741296,
                  longitude: position.lng != null ? Number(position.lng) : 37.583197,
                  latitudeDelta: 0.0015,
                  longitudeDelta: 0.001,
                }}
              >
                {position.lat != null && position.lng != null && (
                  <Marker
                    coordinate={{
                      latitude: Number(position.lat),
                      longitude: Number(position.lng),
                      latitudeDelta: 0.002,
                      longitudeDelta: 0.002,
                    }}
                  />
                )}
              </MapView>
            )}
          </View>
          <Button style={styles.addButton} onPress={onSaveButtonPress}>
            СОХРАНИТЬ
          </Button>
        </Layout>
        <Divider />
      </KeyboardAvoidingView>
    </Layout>
  );
});

const themedStyles = StyleService.create({
  container: {
    flex: 1,
    backgroundColor: 'background-basic-color-2',
  },
  form: {
    flex: 1,
    paddingHorizontal: 4,
    paddingVertical: 24,
  },
  input: {
    marginHorizontal: 12,
    marginVertical: 8,
  },
  logoLabel: {
    marginHorizontal: 12,
    marginVertical: 8,
    color: 'rgb(143, 155, 179)',
    fontSize: 12,
    fontWeight: '800',
    marginBottom: 4,
  },
  logoContainer: {
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
  },
  selectLogo: {
    marginHorizontal: 16,
    marginVertical: 24,
    maxWidth: 150,
  },
  logo: {
    aspectRatio: 1.0,
    height: 124,
    width: 124,
    alignSelf: 'center',
  },

  addButton: {
    marginHorizontal: 16,
    marginVertical: 24,
  },
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 7777,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },

  mapContainer: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    width: '100%',
    height: 200,
  },
});
