import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { AuthNavigator } from './auth.navigator';
import { TodoNavigator } from './todo.navigator';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { navigationRef, navigate, dispatch, reset } from './RootNavigation';

const { Navigator, Screen } = createStackNavigator();
const onReady = (props) => {
	AsyncStorage.getItem('sessionId').then((sessionId) => {
		if (sessionId !== -1 && sessionId === null) {
			// reset({
			// 	index: 0,
			// 	routes: [{ name: 'Auth' }],
			// });
		} else if (sessionId !== -1) {
			reset({
				index: 0,
				routes: [{ name: 'ToDo' }],
			});
		}
	});
};
export const AppNavigator = () => {
	return (
		<NavigationContainer ref={navigationRef} onReady={onReady}>
			<Navigator headerMode="none">
				<Screen name="Auth" component={AuthNavigator} />
				<Screen name="ToDo" component={TodoNavigator} />
			</Navigator>
		</NavigationContainer>
	);
};
