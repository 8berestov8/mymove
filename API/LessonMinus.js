import API from './index';
export const addLessonMinus = (
  { user, idSchool, idLessonMinus, data },
  type = 'add'
) => {
  return new Promise((resolve, reject) => {
    const url =
      type == 'add'
        ? `/${user.type}/school/${idSchool}/lesson_minus`
        : `/${user.type}/school/${idSchool}/lesson_minus/${idLessonMinus}`;
    const REQ =
      type == 'add'
        ? API.new(user.sessionId).post(url, data)
        : API.new(user.sessionId).put(url, data);
    REQ.then((res) => {
      resolve(res.data);
    }).catch((err) => {
      API.checkError(err.response).catch((err) => {
        reject(err);
      });
    });
  });
};
export const editLessonMinus = (data) => addLessonMinus(data, 'edit');
export const getLessonMinus = ({ user, idSchool, idlessonPlus }) => {
  return new Promise((resolve, reject) => {
    API.new(user.sessionId)
      .get(`/${user.type}/school/${idSchool}/lessons_minus`, {
        params: {
          lesson_plus_id: idlessonPlus,
        },
      })
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};
export const deleteLessonMinus = ({ user, idSchool, idlessonMinus }) => {
  return new Promise((resolve, reject) => {
    API.new(user.sessionId)
      .delete(
        `/${user.type}/school/${idSchool}/lesson_minus/${idlessonMinus}`,
        {
          params: {},
        }
      )
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};
