import API from './index';
export const addLessonPlus = (
  { user, idSchool, idLessonPlus, data },
  type = 'add'
) => {
  return new Promise((resolve, reject) => {
    const url =
      type == 'add'
        ? `/${user.type}/school/${idSchool}/lesson_plus`
        : `/${user.type}/school/${idSchool}/lesson_plus/${idLessonPlus}`;
    const REQ =
      type == 'add'
        ? API.new(user.sessionId).post(url, data)
        : API.new(user.sessionId).put(url, data);
    REQ.then((res) => {
      resolve(res.data);
    }).catch((err) => {
      API.checkError(err.response).catch((err) => {
        reject(err);
      });
    });
  });
};
export const editLessonPlus = (data) => addLessonPlus(data, 'edit');
export const getLessonPlus = ({ user, idSchool, idStudent }) => {
  return new Promise((resolve, reject) => {
    API.new(user.sessionId)
      .get(`/${user.type}/school/${idSchool}/lessons_plus`, {
        params: {
          student_id: idStudent,
        },
      })
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};
export const deleteLessonPlus = ({ user, idSchool, idLessonPlus }) => {
  return new Promise((resolve, reject) => {
    API.new(user.sessionId)
      .delete(`/${user.type}/school/${idSchool}/lesson_plus/${idLessonPlus}`, {
        params: {},
      })
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};
