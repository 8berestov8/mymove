import API from './index';
export const setProfile = ({ user, data }) => {
  return new Promise((resolve, reject) => {
    API.new(user.sessionId)
      .post('/profile', data, {
        headers: { 'Content-Type': 'multipart/form-data' },
      })
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};
export const getProfile = ({ user }) => {
  return new Promise((resolve, reject) => {
    API.new(user.sessionId)
      .get('/profile', {
        params: {},
      })
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        reject(err);
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};
export const checkSmsCode = ({ user, checkPhoneId, code }) => {
  return new Promise((resolve, reject) => {
    API.new()
      .get('/check_sms_code', {
        params: {
          id: checkPhoneId,
          code: code,
          type: user.type,
        },
      })
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        if (typeof err.response != 'undefined' && err.response.status >= 500)
          reject(err.response.data.message);
        else
          API.checkError(err.response).catch((err) => {
            reject(err);
          });
      });
  });
};
export const checkPhone = ({ phone }) => {
  return new Promise((resolve, reject) => {
    API.new()
      .get('/check_phone', {
        params: {
          phone,
        },
      })
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};
