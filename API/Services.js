import API from './index';
export const addService = (
  { user, idSchool, idService, data },
  type = 'add'
) => {
  return new Promise((resolve, reject) => {
    const url =
      type == 'add'
        ? `/${user.type}/school/${idSchool}/service`
        : `/${user.type}/school/${idSchool}/service/${idService}`;
    const REQ =
      type == 'add'
        ? API.new(user.sessionId).post(url, data)
        : API.new(user.sessionId).put(url, data);
    REQ.then((res) => {
      resolve(res.data);
    }).catch((err) => {
      API.checkError(err.response).catch((err) => {
        reject(err);
      });
    });
  });
};
export const editService = (data) => addService(data, 'edit');
export const getServices = ({ user, idSchool }) => {
  return new Promise((resolve, reject) => {
    API.new(user.sessionId)
      .get(`/${user.type}/school/${idSchool}/services`, {
        params: {},
      })
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};
export const deleteService = ({ user, idSchool, idService }) => {
  return new Promise((resolve, reject) => {
    API.new(user.sessionId)
      .delete(`/${user.type}/school/${idSchool}/service/${idService}`, {})
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};
