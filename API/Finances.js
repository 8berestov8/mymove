import API from './index';
export const getFinances = ({ user, idSchool }) => {
	return new Promise((resolve, reject) => {
		API.new(user.sessionId)
			.get(`/${user.type}/school/${idSchool}/finances`, {
				params: {},
			})
			.then((res) => {
				resolve(res.data);
			})
			.catch((err) => {
				API.checkError(err.response).catch((err) => {
					reject(err);
				});
			});
	});
};
export const addFinance = ({ user, idSchool, dataFinance }) => {
	return new Promise((resolve, reject) => {
		API.new(user.sessionId)
			.post(`/${user.type}/school/${idSchool}/finance`, dataFinance)
			.then((res) => {
				resolve(res.data);
			})
			.catch((err) => {
				API.checkError(err.response).catch((err) => {
					reject(err);
				});
			});
	});
};

export const removeFinance = ({ user, idSchool, idFinance }) => {
	return new Promise((resolve, reject) => {
		API.new(user.sessionId)
			.delete(`/${user.type}/school/${idSchool}/finance/${idFinance}`)
			.then((res) => {
				resolve(res.data);
			})
			.catch((err) => {
				API.checkError(err.response).catch((err) => {
					reject(err);
				});
			});
	});
};

export const updateFinance = ({ user, idSchool, idFinance, dataFinance }) => {
	return new Promise((resolve, reject) => {
		API.new(user.sessionId)
			.put(`/${user.type}/school/${idSchool}/finance/${idFinance}`, dataFinance)
			.then((res) => {
				resolve(res.data);
			})
			.catch((err) => {
				API.checkError(err.response).catch((err) => {
					reject(err);
				});
			});
	});
};
