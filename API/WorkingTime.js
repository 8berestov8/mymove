import API from './index';
export const getWorkingTime = ({ user, idSchool }) => {
	return new Promise((resolve, reject) => {
		API.new(user.sessionId)
			.get(`/${user.type}/school/${idSchool}/finances`, {
				params: {},
			})
			.then((res) => {
				resolve(res.data);
			})
			.catch((err) => {
				API.checkError(err.response).catch((err) => {
					reject(err);
				});
			});
	});
};
