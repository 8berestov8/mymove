import API from './index';
export const addStudent = (
  { user, idSchool, idStudent, data },
  type = 'add'
) => {
  return new Promise((resolve, reject) => {
    const url =
      type == 'add'
        ? `/${user.type}/school/${idSchool}/student`
        : `/${user.type}/school/${idSchool}/student/${idStudent}`;
    const REQ =
      type == 'add'
        ? API.new(user.sessionId).post(url, data)
        : API.new(user.sessionId).put(url, data);
    REQ.then((res) => {
      resolve(res.data);
    }).catch((err) => {
      API.checkError(err.response).catch((err) => {
        reject(err);
      });
    });
  });
};
export const editStudent = (data) => addStudent(data, 'edit');
export const getStudents = ({ user, idSchool }) => {
  return new Promise((resolve, reject) => {
    API.new(user.sessionId)
      .get(`/${user.type}/school/${idSchool}/students`, {
        params: {},
      })
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};
export const deleteStudent = ({ user, idSchool, idStudent }) => {
  return new Promise((resolve, reject) => {
    API.new(user.sessionId)
      .delete(`/${user.type}/school/${idSchool}/student/${idStudent}`, {
        params: {
          id_session: user.sessionId,
        },
      })
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};
