import API from './index';
export const getSchools = ({ user }) => {
  return new Promise((resolve, reject) => {
    API.new(user.sessionId)
      .get(`/${user.type}/schools`, {
        params: {},
      })
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};
export const addSchool = ({ user, dataSchool }) => {
  return new Promise((resolve, reject) => {
    API.new(user.sessionId)
      .post(`/${user.type}/school`, dataSchool)
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};
export const editSchool = ({ user, schoolId, dataSchool }) => {
  return new Promise((resolve, reject) => {
    API.new(user.sessionId)
      .post(`/${user.type}/school/${schoolId}?_method=PUT`, dataSchool)
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};

export const deleteSchool = ({ idSchool, user }) => {
  return new Promise((resolve, reject) => {
    API.new(user.sessionId)
      .delete(`/${user.type}/school/${idSchool}`, {
        params: {},
      })
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};
