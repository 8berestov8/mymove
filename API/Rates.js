import API from './index';
export const checkRate = (data, sessionId) => {
  return new Promise((resolve, reject) => {
    API.new(sessionId)
      .post(`/owner/rates/check`, data)
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};
export const getRate = (data, sessionId) => {
  return new Promise((resolve, reject) => {
    API.new(sessionId)
      .post(`/owner/rates/get`, data)
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};
