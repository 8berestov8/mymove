import API from './index';
export const addTeacher = (
  { user, idSchool, idTeacher, data },
  type = 'add'
) => {
  return new Promise((resolve, reject) => {
    const url =
      type == 'add'
        ? `/${user.type}/school/${idSchool}/teacher`
        : `/${user.type}/school/${idSchool}/teacher/${idTeacher}`;
    const REQ =
      type == 'add'
        ? API.new(user.sessionId).post(url, data)
        : API.new(user.sessionId).put(url, data);
    REQ.then((res) => {
      resolve(res.data);
    }).catch((err) => {
      API.checkError(err.response).catch((err) => {
        reject(err);
      });
    });
  });
};
export const editTeacher = (data) => addTeacher(data, 'edit');
export const getTeachers = ({ user, idSchool }) => {
  return new Promise((resolve, reject) => {
    API.new(user.sessionId)
      .get(`/${user.type}/school/${idSchool}/teachers`, {
        params: {},
      })
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};
export const deleteTeacher = ({ user, idSchool, idTeacher }) => {
  return new Promise((resolve, reject) => {
    API.new(user.sessionId)
      .delete(`/${user.type}/school/${idSchool}/teacher/${idTeacher}`, {})
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        API.checkError(err.response).catch((err) => {
          reject(err);
        });
      });
  });
};
