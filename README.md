MyMove.ru APP

# React Native + Expo bare

cd /

npm install

rename config-example.js to config.js

npm run web

# Laravel 8

cd /backend

# Rename file .env-example to .env

# Laravel command

composer install

php artisan migrate

php artisan serve

php artisan export:postman

# /storage/app/postman

# Change URL API "baseURL"

vi /API/index.js
