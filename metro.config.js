module.exports = {
	transformer: {
		assetPlugins: ['expo-asset/tools/hashAssetFiles'],
		getTransformOptions: async () => ({
			transform: {
				experimentalImportSupport: false,
				inlineRequires: false,
			},
		}),
	},
};

const { getDefaultConfig } = require('expo/metro-config');

module.exports = getDefaultConfig(__dirname);
