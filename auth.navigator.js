import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import SignIn from './views/sign-in';
import Types from './views/types';

const { Navigator, Screen } = createStackNavigator();



const HomeNavigator = () => {
  return (
  <Navigator headerMode='none'>
    <Screen name='Types' component={Types}/>
    <Screen name='SignIn' component={SignIn}/>
  </Navigator>
  )
};

export const AuthNavigator = () => (
  <HomeNavigator/>
);
