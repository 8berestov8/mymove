import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import SchoolsList from './views/schools-list';
import ProfileSettings from './views/profile-settings';
import SchoolAdd from './views/school-add';
import SchoolEdit from './views/school-edit';
import TeachersList from './views/teachers-list';
import TeacherForm from './views/teacher-form';
import StudentsList from './views/students-list';
import StudentForm from './views/student-form';
import ServicesList from './views/services-list';
import ServiceForm from './views/service-form';
import LessonsPlusList from './views/lessons-plus-list';
import LessonsMinusList from './views/lessons-minus-list';
import LessonPlusForm from './views/lesson-plus-form';
import LessonMinusForm from './views/lesson-minus-form';
import ScanCode from './views/scan-code';
import Rates from './views/rates';
import Finance from './views/finance';
import Search from './views/search';
import WorkingTimeList from './views/working-time-list';
import WorkingTimeForm from './views/working-time-form';

import {
	BottomNavigation,
	BottomNavigationTab,
	Icon,
} from '@ui-kitten/components';
import { createStackNavigator } from '@react-navigation/stack';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const HomeIcon = (props) => <Icon {...props} name="home-outline" />;

const PersonIcon = (props) => <Icon {...props} name="person-outline" />;
const tabs = [
	{ icon: HomeIcon, route: 'Home' },
	{ icon: PersonIcon, route: 'Settings' },
];
const BottomTabBar = ({ navigation }) => {
	const [selectedIndex, setSelectedIndex] = React.useState(0);
	return (
		<BottomNavigation
			appearance="noIndicator"
			selectedIndex={selectedIndex}
			onSelect={(index) =>
				setSelectedIndex(index) || navigation.navigate(tabs[index].route)
			}
		>
			{tabs.map((tab) => (
				<BottomNavigationTab key={`tab_${tab.route}`} icon={tab.icon} />
			))}
		</BottomNavigation>
	);
};
const HomeNavigator = () => {
	return (
		<Stack.Navigator headerMode="none">
			<Stack.Screen name="SchoolsList" component={SchoolsList} />
			<Stack.Screen name="SchoolAdd" component={SchoolAdd} />
			<Stack.Screen name="SchoolEdit" component={SchoolEdit} />
			<Stack.Screen name="TeachersList" component={TeachersList} />
			<Stack.Screen name="TeacherForm" component={TeacherForm} />

			<Stack.Screen name="StudentsList" component={StudentsList} />
			<Stack.Screen name="StudentForm" component={StudentForm} />

			<Stack.Screen name="ServicesList" component={ServicesList} />
			<Stack.Screen name="ServiceForm" component={ServiceForm} />

			<Stack.Screen name="LessonsPlusList" component={LessonsPlusList} />
			<Stack.Screen name="LessonsMinusList" component={LessonsMinusList} />
			<Stack.Screen name="LessonPlusForm" component={LessonPlusForm} />

			<Stack.Screen name="LessonMinusForm" component={LessonMinusForm} />
			<Stack.Screen name="Rates" component={Rates} />
			<Stack.Screen name="ScanCode" component={ScanCode} />
			<Stack.Screen name="Search" component={Search} />
			<Stack.Screen name="Finance" component={Finance} />
			<Stack.Screen name="WorkingTimeList" component={WorkingTimeList} />
			<Stack.Screen name="WorkingTimeForm" component={WorkingTimeForm} />
		</Stack.Navigator>
	);
};
const SettingsNavigator = () => {
	return (
		<Stack.Navigator headerMode="none">
			<Stack.Screen name="ProfileSettings" component={ProfileSettings} />
		</Stack.Navigator>
	);
};
const MainNavigator = () => {
	return (
		<Tab.Navigator
			headerMode="none"
			tabBar={(props) => <BottomTabBar {...props} />}
		>
			<Tab.Screen name="Home" component={HomeNavigator} />
			<Tab.Screen name="Settings" component={SettingsNavigator} />
		</Tab.Navigator>
	);
};

export const TodoNavigator = () => <MainNavigator />;
